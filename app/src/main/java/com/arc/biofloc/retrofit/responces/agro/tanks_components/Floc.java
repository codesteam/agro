
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Floc {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("examine_ts")
    @Expose
    private String examineTs;
    @SerializedName("examine_duration")
    @Expose
    private String examineDuration;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("probiotics")
    @Expose
    private String probiotics;
    @SerializedName("molasses")
    @Expose
    private String molasses;
    @SerializedName("feed")
    @Expose
    private String feed;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getExamineTs() {
        return examineTs;
    }

    public void setExamineTs(String examineTs) {
        this.examineTs = examineTs;
    }

    public String getExamineDuration() {
        return examineDuration;
    }

    public void setExamineDuration(String examineDuration) {
        this.examineDuration = examineDuration;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getProbiotics() {
        return probiotics;
    }

    public void setProbiotics(String probiotics) {
        this.probiotics = probiotics;
    }

    public String getMolasses() {
        return molasses;
    }

    public void setMolasses(String molasses) {
        this.molasses = molasses;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

}
