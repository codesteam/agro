package com.arc.biofloc.retrofit.api;




import com.arc.biofloc.retrofit.responces.DummyHelper;
import com.arc.biofloc.retrofit.responces.LoginResponse;
import com.arc.biofloc.retrofit.responces.RegistrationResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetDailyChecklist;
import com.arc.biofloc.retrofit.responces.agro.get.GetFeeding;
import com.arc.biofloc.retrofit.responces.agro.get.GetFeeds;
import com.arc.biofloc.retrofit.responces.agro.get.GetFloc;
import com.arc.biofloc.retrofit.responces.agro.get.GetHarvest;
import com.arc.biofloc.retrofit.responces.agro.get.GetPreparation;
import com.arc.biofloc.retrofit.responces.agro.get.GetProjects;
import com.arc.biofloc.retrofit.responces.agro.get.GetSeeds;
import com.arc.biofloc.retrofit.responces.agro.get.GetSeedsRelease;
import com.arc.biofloc.retrofit.responces.agro.get.GetTanks;
import com.arc.biofloc.retrofit.responces.agro.get.GetWeeklyChecklist;
import com.arc.biofloc.retrofit.responces.agro.project.Project;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Harvest;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.SeedsRelease;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;
import com.arc.biofloc.retrofit.responces.board.Board;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.DistinctExamHistory;
import com.arc.biofloc.retrofit.responces.examhistory.ExamHistory;
import com.arc.biofloc.retrofit.responces.participation.Participation;
import com.arc.biofloc.retrofit.responces.parts.Part;
import com.arc.biofloc.retrofit.responces.question.Question;
import com.arc.biofloc.retrofit.responces.subject.Subject;
import com.arc.biofloc.retrofit.responces.submissions.ExamSubmissionsResponce;
import com.arc.biofloc.retrofit.responces.submitexam.SubmitExam;
import com.arc.biofloc.retrofit.responces.year.Year;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;


public interface APIs {

	//This api call for splash screen
	/*@GET("/api/participationtypes/")
	public void getPartsWithDetails(
			@Query("timestamp") String timestamp,
            @Query("package_name") String packageName,
            Callback<Part> callback);*/
	// this api call for service only
	@GET("/api/board/")
	public void getBoards(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Board> callback);
	@GET("/api/year/")
	public void getYears(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Year> callback);
	@GET("/api/subjects/")
	public void getSubjects(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Subject> callback);
	@GET("/api/participationtypes/")
	public void getParts(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Part> callback);
	@GET("/api/participations/")
	public void getParticipation(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			@Query("subject") String subject,
			@Query("part_id") String partId,
			Callback<Participation> callback);

	@GET("/api/")
	public void getDailyExam(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getModelTest(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getSuggestion(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getPBQ(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getPackage(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/exam-history/")
	public void getExamHistory(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("uid") String userId,
			Callback<ExamHistory> callback);
	@GET("/api/exam-history/{exam_id}/")
	public void getDistinctExamHistory(
			@Path("exam_id") String exam_id,
			@Header("Authorization") String authHeader,
			@Query("uid") String userId,
			Callback<DistinctExamHistory> callback);
	@GET("/api/")
	public void getNoticeBoard(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/questions")
	public void getQuestion(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			@Query("q_set") String questionSetId,
			Callback<Question> callback);

	@GET("/api/")
	public void purchase(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("product_id") String productId,
			@Query("user_id") String userId,
			Callback<DummyHelper> callback);

	@FormUrlEncoded
	@POST("/rest-auth/registration/")
	void registration(
			@Field("phone_number") String phone_number,
			@Field("first_name") String first_name,
			@Field("email") String email,
			@Field("password") String password,
			@Field("confirm_password") String confirm_password,
			Callback<RegistrationResponse> callback);

	@FormUrlEncoded
	@POST("/rest-auth/login/")
	void login(
			@Field("phone_number") String phone_number,
			@Field("password") String password,
			Callback<LoginResponse> callback);

	@PUT("/api/exam-history/{id}/")
	void submitExam(@Path("id") String id, @Body SubmitExam submitExam, Callback<ExamSubmissionsResponce> callback);
/* agro apis/*
 */

	@FormUrlEncoded
	@POST("/api/new/project/")
	void project(
			@Field("name") String name,
			@Field("address") String address,
			Callback<Project> callback);

	@FormUrlEncoded
	@POST("/api/new/tank/")
	void tank(
			@Field("project_id") Long projectId,
			@Field("name") String name,
			@Field("details") String details,
			@Field("type") String type,
			@Field("height") String height,
			@Field("water_level") String water_level,
			@Field("tank_shape") String tank_shape,
			@Field("radius") String radius,
			@Field("conical_height") String conical_height,
			@Field("max_capacity") String max_capacity,
			@Field("start_ts") String start_ts,
			Callback<Tank> callback);

	@FormUrlEncoded
	@POST("/api/new/tank/preparation/")
	void tankPreparation(
			@Field("tank_id") Long tank_id,
			@Field("prep_start_ts") String prep_start_ts,
			@Field("temperature") String temperature,
			@Field("ph_level") String ph_level,
			@Field("tds_level") String tds_level,
			@Field("do_level") String do_level,
			@Field("arsenic_level") String arsenic_level,
			@Field("clorin_level") String clorin_level,
			@Field("iron_level") String iron_level,
			@Field("areation_ts") String areation_ts,
			@Field("areation_unit") String areation_unit,
			@Field("areation_point") String areation_point,
			@Field("areation_wait_duration") String areation_wait_duration,
			@Field("ground_water_tds") String ground_water_tds,
			@Field("target_tds") String target_tds,
			@Field("salt_amount") String salt_amount,
			@Field("tds_wait_duration") String tds_wait_duration,
			@Field("molasses_amount") String molasses_amount,
			@Field("molasses_wait_duration") String molasses_wait_duration,
			@Field("probiotics_name") String probiotics_name,
			@Field("probiotics_dosage") String probiotics_dosage,
			@Field("probiotic_state") String probiotic_state,
			@Field("probiotic_strength") String probiotic_strength,
			@Field("probiotic_strain") String probiotic_strain,
			Callback<Preparation> callback);

	@PUT("/api/new/tank/preparation/{id}/")
	void updatePreparation(@Path("id") String id, @Body Preparation preparation, Callback<Preparation> callback);
	@FormUrlEncoded
	@POST("/api/tank/floc/")
	void floc(
			@Field("tank_id") Long tank_id,
			@Field("examine_ts") String examine_ts,
			@Field("examine_duration") String examine_duration,
			@Field("volume") String volume,
			@Field("temperature") String temperature,
			@Field("probiotics") String probiotics,
			@Field("molasses") String molasses,
			@Field("feed") String starter_feed,
			@Field("remark") String remark,
			Callback<Floc> callback);
	@FormUrlEncoded
	@POST("/api/tank/daily_check_list/")
	void dailyCheckList(
			@Field("tank_id") Long tank_id,
			@Field("entry_ts") String entry_ts,
			@Field("temperature") String temperature,
			@Field("ph_level") String ph_level,
			@Field("tds_level") String tds_level,
			@Field("do_level") String do_level,
			@Field("ammonia_level") String ammonia_level,
			@Field("mortality") String mortality,
			Callback<DailyChecklist> callback);
	@FormUrlEncoded
	@POST("/api/tank/seeds/")
	void seeds(
			@Field("tank_id") Long tank_id,
			@Field("tank_capacity") String tank_capacity,
			@Field("target") String target,
			@Field("arrival_date") String arrival_date,
			@Field("preparation_details") String preparation_details,
			@Field("initial_avarage_size") String initial_avarage_size,
			@Field("marketable_size") String marketable_size,
			@Field("number_Of_seeds") String number_Of_seeds,
			@Field("release_date") String release_date,
			@Field("harvesting_date") String harvesting_date,
			@Field("seeds_details") String seeds_details,
			@Field("supplier_details") String supplier_details,
			@Field("initial_biomass") String initial_biomass,
			Callback<Seeds> callback);
	@FormUrlEncoded
	@POST("/api/tank/seeds_release/")
	void seedsRelease(
			@Field("tank_id") Long tank_id,
			@Field("release_ts") String release_ts,
			@Field("quantity") String quantity,
			@Field("temperature_adjustment") String temperature_adjustment,
			@Field("pm_wash") String pm_wash,
			@Field("wash_duration") String wash_duration,
			@Field("hour") String hour,
			@Field("mortality") String mortality,
			Callback<SeedsRelease> callback);

	@FormUrlEncoded
	@POST("/api/tank/weekly_check_list/")
	void weeklyCheckList(
			@Field("tank_id") Long tank_id,
            @Field("entry_ts") String entry_ts,
			@Field("meadin_biomass") String meadin_biomass,
			@Field("legitimate_biomass") String legitimate_biomass,
			@Field("meadin_size") String meadin_size,
			@Field("legitimate_size") String legitimate_size,
			@Field("ammonia_level") String ammonia_level,
			Callback<WeeklyCheckList> callback);
    @FormUrlEncoded
    @POST("/api/tank/feed/")
    void feeds(
            @Field("tank_id") Long tank_id,
			@Field("start_date") String start_date,
            @Field("brand") String brand,
            @Field("type") String type,
            @Field("protein_level") String protein_level,
            @Field("fish_age") String fish_age,
            Callback<Feed> callback);
	@FormUrlEncoded
	@POST("/api/tank/feeding/")
	void feeding(
			@Field("tank_id") Long tank_id,
			@Field("feeding_ts") String feeding_ts,
			@Field("legitimate_biomass") String legitimate_biomass,
			@Field("feed_percentage") String feed_percentage,
			@Field("feed_amount") String feed_amount,
			@Field("protein_amount") String protein_amount,
			@Field("produced_nitrogen") String total_nitrogen,
			@Field("wasted_nitrogen") String wasted_nitrogen,
			@Field("carbon_source_type") String carbon_source_type,
			@Field("carbon_percentage") String carbon_percentage,
			@Field("catalyst_amount") String catalyst_amount,
			Callback<Feeding> callback);
    @FormUrlEncoded
    @POST("/api/tank/harvest/")
    void harvest(
            @Field("tank_id") Long tank_id,
            @Field("harvesting_ts") String harvesting_ts,
            @Field("tank_biomass") String tank_biomass,
            @Field("legitimate_biomass") String legitimate_biomass,
			@Field("remark") String remark,
            Callback<Harvest> callback);

    // Get calls for agro
	@GET("/api/projects/")
	public void getProjects(
			@Query("timestamp") Long timestamp,
			Callback<GetProjects> callback);
	@GET("/api/tanks/")
	public void getTanks(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			Callback<GetTanks> callback);
	@GET("/api/preparation/")
	public void getPrepration(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetPreparation> callback);
	@GET("/api/seeds/")
	public void getSeeds(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetSeeds> callback);
	@GET("/api/floc/")
	public void getFloc(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetFloc> callback);
	@GET("/api/seeds_release/")
	public void getSeedsRelease(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetSeedsRelease> callback);
	@GET("/api/feeding/")
	public void getFeeding(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetFeeding> callback);
	@GET("/api/weekly_check_list/")
	public void getWeeklyCheckList(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetWeeklyChecklist> callback);
	@GET("/api/feed/")
	public void getFeed(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetFeeds> callback);
	@GET("/api/harvest/")
	public void getHarvest(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetHarvest> callback);
	@GET("/api/daily_check_list/")
	public void getDailyCheckList(
			@Query("timestamp") Long timestamp,
			@Query("project_id") Long project_id,
			@Query("tank_id") Long tank_id,
			Callback<GetDailyChecklist> callback);



}
