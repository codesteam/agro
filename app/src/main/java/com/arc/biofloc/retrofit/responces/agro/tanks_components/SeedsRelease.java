
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeedsRelease {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("release_ts")
    @Expose
    private String releaseTs;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("temperature_adjustment")
    @Expose
    private String temperatureAdjustment;
    @SerializedName("pm_wash")
    @Expose
    private String pmWash;
    @SerializedName("wash_duration")
    @Expose
    private String washDuration;
    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("mortality")
    @Expose
    private String mortality;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getReleaseTs() {
        return releaseTs;
    }

    public void setReleaseTs(String releaseTs) {
        this.releaseTs = releaseTs;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTemperatureAdjustment() {
        return temperatureAdjustment;
    }

    public void setTemperatureAdjustment(String temperatureAdjustment) {
        this.temperatureAdjustment = temperatureAdjustment;
    }

    public String getPmWash() {
        return pmWash;
    }

    public void setPmWash(String pmWash) {
        this.pmWash = pmWash;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMortality() {
        return mortality;
    }

    public void setMortality(String mortality) {
        this.mortality = mortality;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public String getWashDuration() {
        return washDuration;
    }

    public void setWashDuration(String washDuration) {
        this.washDuration = washDuration;
    }
}
