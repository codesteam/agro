
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Seeds {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("tank_capacity")
    @Expose
    private String tankCapacity;
    @SerializedName("target")
    @Expose
    private String target;
    @SerializedName("arrival_date")
    @Expose
    private String arrivalDate;
    @SerializedName("preparation_details")
    @Expose
    private String preparationDetails;
    @SerializedName("initial_avarage_size")
    @Expose
    private String initialAvarageSize;
    @SerializedName("marketable_size")
    @Expose
    private String marketableSize;
    @SerializedName("number_Of_seeds")
    @Expose
    private String numberOfSeeds;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("harvesting_date")
    @Expose
    private String harvestingDate;
    @SerializedName("seeds_details")
    @Expose
    private String seedsDetails;
    @SerializedName("supplier_details")
    @Expose
    private String supplierDetails;
    @SerializedName("initial_biomass")
    @Expose
    private String initialBiomass;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;

    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(String tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getPreparationDetails() {
        return preparationDetails;
    }

    public void setPreparationDetails(String preparationDetails) {
        this.preparationDetails = preparationDetails;
    }

    public String getInitialAvarageSize() {
        return initialAvarageSize;
    }

    public void setInitialAvarageSize(String initialAvarageSize) {
        this.initialAvarageSize = initialAvarageSize;
    }

    public String getMarketableSize() {
        return marketableSize;
    }

    public void setMarketableSize(String marketableSize) {
        this.marketableSize = marketableSize;
    }

    public String getNumberOfSeeds() {
        return numberOfSeeds;
    }

    public void setNumberOfSeeds(String numberOfSeeds) {
        this.numberOfSeeds = numberOfSeeds;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getHarvestingDate() {
        return harvestingDate;
    }

    public void setHarvestingDate(String harvestingDate) {
        this.harvestingDate = harvestingDate;
    }

    public String getSeedsDetails() {
        return seedsDetails;
    }

    public void setSeedsDetails(String seedsDetails) {
        this.seedsDetails = seedsDetails;
    }

    public String getSupplierDetails() {
        return supplierDetails;
    }

    public void setSupplierDetails(String supplierDetails) {
        this.supplierDetails = supplierDetails;
    }

    public String getInitialBiomass() {
        return initialBiomass;
    }

    public void setInitialBiomass(String initialBiomass) {
        this.initialBiomass = initialBiomass;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

}
