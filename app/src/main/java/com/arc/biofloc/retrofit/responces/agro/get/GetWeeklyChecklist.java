
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;

public class GetWeeklyChecklist {

    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("weekly_check_list")
    @Expose
    private List<WeeklyCheckList> weeklyCheckList = null;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public List<WeeklyCheckList> getWeeklyCheckList() {
        return weeklyCheckList;
    }

    public void setWeeklyCheckList(List<WeeklyCheckList> weeklyCheckList) {
        this.weeklyCheckList = weeklyCheckList;
    }

}
