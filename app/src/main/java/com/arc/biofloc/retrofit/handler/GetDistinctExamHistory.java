package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.DistinctExamHistory;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetDistinctExamHistory extends APICallHandler<Long> {

	private  static GetDistinctExamHistory singleton = null;

	private GetDistinctExamHistory() {
	}

	public static GetDistinctExamHistory getInstance() {
		singleton = new GetDistinctExamHistory();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).getDistinctExamHistory(args[1], args[2],args[3],new Callback<DistinctExamHistory>() {

			@Override
			public void failure(RetrofitError arg0) {
				Print.e(this, "failed");
				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(DistinctExamHistory arg0,
								Response arg1) {
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}
}
