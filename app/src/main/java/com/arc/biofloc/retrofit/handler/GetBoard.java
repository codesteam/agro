package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.db.dao.BoardDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.board.Board;
import com.arc.biofloc.retrofit.responces.board.Result;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetBoard extends APICallHandler<Long> {

	private  static GetBoard singleton = null;

	private GetBoard() {
	}

	public static GetBoard getInstance() {
		singleton = new GetBoard();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getBoards(args[1], args[2],new Callback<Board>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Board arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_BOARDS, arg0.getRespondedAt());
					BoardDao dao = new BoardDao(CommonApplication.getAppContext());
					for (Result result: arg0.getResults()){
						dao.insert(result);
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, Board arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
