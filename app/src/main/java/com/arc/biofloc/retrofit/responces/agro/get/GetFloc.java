
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;

public class GetFloc {

    @SerializedName("floc")
    @Expose
    private List<Floc> floc = null;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public List<Floc> getFloc() {
        return floc;
    }

    public void setFloc(List<Floc> floc) {
        this.floc = floc;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
