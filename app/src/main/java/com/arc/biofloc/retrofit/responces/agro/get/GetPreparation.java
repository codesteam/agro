
package com.arc.biofloc.retrofit.responces.agro.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;

public class GetPreparation {

    @SerializedName("preparation")
    @Expose
    private Preparation preparation;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public Preparation getPreparation() {
        return preparation;
    }

    public void setPreparation(Preparation preparation) {
        this.preparation = preparation;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
