package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetTanks;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestTanks extends APICallHandler<Long> {
	public static final String tag = "Tank";
	private  static RequestTanks singleton = null;

	private RequestTanks() {
	}

	public static RequestTanks getInstance() {
		singleton = new RequestTanks();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getTanks(ref1,ref2,new Callback<GetTanks>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetTanks arg0,
								Response arg1) {
				if(arg0.getTank() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					TankDao dao = new TankDao(CommonApplication.getAppContext());
					for (Tank result: arg0.getTank()){
						dao.insert(result);
						Print.e(this, "tank inserted");
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
