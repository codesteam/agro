
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;

public class GetTanks {

    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("tank")
    @Expose
    private List<Tank> tank = null;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public List<Tank> getTank() {
        return tank;
    }

    public void setTank(List<Tank> tank) {
        this.tank = tank;
    }

}
