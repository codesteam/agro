package com.arc.biofloc.retrofit.sync;

import android.content.Context;

import com.arc.biofloc.R;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.get.RequestProjects;

public class SyncProjects {
    ApiStatusDao apiStatusDao;
    Context context;
    String baseUrl;
    APIClientResponse apiClientResponse;
    public static boolean busy;
    public SyncProjects(Context context, APIClientResponse apiClientResponse){
        this.apiStatusDao = new ApiStatusDao(context);
        this.context = context;
        baseUrl = context.getString(R.string.base_url);
        if(busy){
            return;
        }

        busy=true;
        this.apiClientResponse = apiClientResponse;
        start(0);
    }

    private void start(int pos){
        RequestProjects.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                apiClientResponse.onFailure(msg, sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                apiClientResponse.onSuccess(msg, sender);

            }
        },null, (long)apiStatusDao.getRepondedTime(RequestProjects.tag),null,null,baseUrl);
    }
}
