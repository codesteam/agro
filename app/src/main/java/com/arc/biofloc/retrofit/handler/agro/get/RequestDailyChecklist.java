package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.DailyChecklistDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetDailyChecklist;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestDailyChecklist extends APICallHandler<Long> {

	public static final String tag = "Daily Check List";
	private  static RequestDailyChecklist singleton = null;

	private RequestDailyChecklist() {
	}

	public static RequestDailyChecklist getInstance() {
		singleton = new RequestDailyChecklist();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getDailyCheckList(ref1,ref2,ref3, new Callback<GetDailyChecklist>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetDailyChecklist arg0,
								Response arg1) {
				if(arg0.getDailyChecklist() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					DailyChecklistDao dao = new DailyChecklistDao(CommonApplication.getAppContext());
					for (DailyChecklist result: arg0.getDailyChecklist()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
