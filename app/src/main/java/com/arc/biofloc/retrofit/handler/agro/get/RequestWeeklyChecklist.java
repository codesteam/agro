package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.WeeklyChecklistDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetWeeklyChecklist;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestWeeklyChecklist extends APICallHandler<Long> {
    public static final String tag = "Weekly Check List";
	private  static RequestWeeklyChecklist singleton = null;

	private RequestWeeklyChecklist() {
	}

	public static RequestWeeklyChecklist getInstance() {
		singleton = new RequestWeeklyChecklist();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getWeeklyCheckList(ref1,ref2, ref3, new Callback<GetWeeklyChecklist>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetWeeklyChecklist arg0,
								Response arg1) {
				if(arg0.getWeeklyCheckList() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					WeeklyChecklistDao dao = new WeeklyChecklistDao(CommonApplication.getAppContext());
					for (WeeklyCheckList result: arg0.getWeeklyCheckList()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
