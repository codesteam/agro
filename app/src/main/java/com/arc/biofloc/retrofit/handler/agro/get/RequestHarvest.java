package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.HarvestDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetHarvest;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestHarvest extends APICallHandler<Long> {
	public static final String tag = "Harvest";
	private  static RequestHarvest singleton = null;

	private RequestHarvest() {
	}

	public static RequestHarvest getInstance() {
		singleton = new RequestHarvest();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getHarvest(ref1,ref2,ref3, new Callback<GetHarvest>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetHarvest arg0,
								Response arg1) {
				if(arg0.getHarvest() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					HarvestDao dao = new HarvestDao(CommonApplication.getAppContext());
					dao.insert(arg0.getHarvest());
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
