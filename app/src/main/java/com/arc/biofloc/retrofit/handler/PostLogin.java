package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.LoginResponse;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PostLogin extends APICallHandler<Long> {

	private  static PostLogin singleton =null;

	private PostLogin() {
	}

	public static PostLogin getInstance() {
		singleton = new PostLogin();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).login(args[1], args[2], new Callback<LoginResponse>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (arg0 != null){
					if (arg0.getResponse()!=null){
						if(arg0.getResponse().getBody()!=null){
							try{
								if (callback != null) {
									callback.onFailure("failed", arg0.getBody());
								}
							}catch (Exception ex){
								ex.printStackTrace();
								if (callback != null) {
									callback.onFailure("failed", null);
								}
							}
						}else{
							if (callback != null) {
								callback.onFailure("failed", null);
							}
						}
					}else{
						if (callback != null) {
							callback.onFailure("failed", null);
						}
					}
				}else{
					if (callback != null) {
						callback.onFailure("failed", null);
					}
				}
			}

			@Override
			public void success(LoginResponse arg0,
								Response arg1) {
				Print.e(this, "RegistrationResponse");
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, LoginResponse arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
