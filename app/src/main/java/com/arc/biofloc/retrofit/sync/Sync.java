package com.arc.biofloc.retrofit.sync;

import android.content.Context;

import com.arc.biofloc.R;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.get.RequestProjects;
import com.arc.biofloc.retrofit.handler.agro.get.RequestTanks;
import com.arc.biofloc.retrofit.responces.agro.project.Project;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;

public class Sync {
    ProjectDao projectDao;
    TankDao tankDao;
    ApiStatusDao apiStatusDao;
    ArrayList<Project> projects;
    ArrayList<Tank> tanks;
    int projectIndex =0;
    int tankIndex = 0;
    Context context;
    ISync iSync;
    public Sync(Context context, ISync iSync){
        this.context = context;
        this.iSync=iSync;
        projectDao = new ProjectDao(context);
        apiStatusDao = new ApiStatusDao(context);
        tankDao = new TankDao(context);
    }
    public void start(){
        RequestProjects.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) { // no new projects recived
                projects = projectDao.getResult(); // fatchecd all projects from db
                if (projects!=null && projects.size() >0){
                    projectIndex = 0;
                    syncTanks(projects.get(projectIndex).getId());
                }else{
                    //No projects created till this day, one user must enter app and create a new project
                    //CommonConstants.GotoLoginActivity(context);
                    iSync.completed();
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) { // recived all new projects
                //ArrayList<Project> projects = (ArrayList<Project>) ((GetProjects) sender).getProject();
                projects = projectDao.getResult(); // fatchecd all projects from db
                if (projects!=null && projects.size() >0){
                    projectIndex = 0;
                    syncTanks(projects.get(projectIndex).getId());
                }else{
                    //No projects created till this day, one user must enter app and create a new project
                    //CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                    iSync.completed();
                }

            }
        },null,(long)apiStatusDao.getRepondedTime(RequestProjects.tag),null,null,context.getString(R.string.base_url));

    }

    private void syncTanks(final  long projectId){
        RequestTanks.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) { // no tanks recived under this project
                projectIndex++;
                if (projectIndex < projects.size()){
                    syncTanks(projects.get(projectIndex).getId());
                }else{
                   // CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                    iSync.completed();
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) { // recived all tanks under this project
                tanks = tankDao.getResults(projectId);
                Print.e(this, "Tanks size:"+tanks.size());
                tankIndex = 0;
                if (tanks!= null && tanks.size() > 0){
                    syncTanksComponents(projectId, tanks.get(0).getId());
                }else{ // since there is no tanks in this project move to new projects tanks set.
                    projectIndex++;
                    if (projectIndex < projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                       // CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                        iSync.completed();
                    }
                }
                /*if (projectIndex < projects.size()){
                    syncTanks(projects.get(projectIndex).getId());
                }*/

            }
        },null,(long)apiStatusDao.getRepondedTime(RequestProjects.tag),projectId,null,context.getString(R.string.base_url));

    }

    private void syncTanksComponents(final long projectId, long tankId){
        final SyncTankComponents syncTankComponents = new SyncTankComponents(context, projectId, tankId, new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                tankIndex++;
                if(tankIndex<tanks.size()){
                    syncTanksComponents(projectId, tanks.get(tankIndex).getId());

                }else{
                    projectIndex++;
                    if (projectIndex<projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                        //ToastMsg.Toast(SplashScreenActivity.this, "Sync completed.");
                        //CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                        iSync.completed();

                    }
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                tankIndex++;
                if(tankIndex<tanks.size()){
                    syncTanksComponents(projectId, tanks.get(tankIndex).getId());

                }else{
                    projectIndex++;
                    if (projectIndex<projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                        //ToastMsg.Toast(SplashScreenActivity.this, "Sync completed.");
                       // CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                        iSync.completed();

                    }
                }
            }
        });

    }
}
