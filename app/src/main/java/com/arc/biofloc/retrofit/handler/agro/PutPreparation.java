package com.arc.biofloc.retrofit.handler.agro;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;
import com.arc.biofloc.retrofit.responces.subject.Subject;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PutPreparation extends APICallHandler<Long> {

	private static PutPreparation singleton;
	Preparation preparation;

	private PutPreparation(Preparation preparation) {
		this.preparation = preparation;
	}

	public static PutPreparation getInstance(Preparation preparation) {
		singleton = new PutPreparation(preparation);
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

         getAPIs(args[0]).updatePreparation(args[1], preparation, new Callback<Preparation>() {
             @Override
             public void success(Preparation preparation, Response response) {
                 Print.e(this, "abc success");
                 if (callback!=null){
                 	callback.onSuccess("success", preparation);
				 }
             }

             @Override
             public void failure(RetrofitError error) {
                 Print.e(this, "abc failed");
                 callback.onFailure("failed", error);
             }
         });

	}

	private void sendSuccessFeedBack(APIClientResponse callback, Subject arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
