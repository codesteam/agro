package com.arc.biofloc.retrofit.api;


import com.arc.biofloc.db.BackgroundRenderer;

public abstract class APICallHandler<T> {

	public APIs getAPIs(String endPoint){
		return APIHandler.getApiInterface(endPoint);
	}
	abstract  public void callAPI(final APIClientResponse callback,  BackgroundRenderer backgroundRenderer, Long ref1, Long ref2,Long ref3, String... args);
	//abstract  public void callAPI(final APIClientResponse callback, T arg0, String ep, String path);

}
