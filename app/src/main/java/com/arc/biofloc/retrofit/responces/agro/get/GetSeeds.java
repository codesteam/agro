
package com.arc.biofloc.retrofit.responces.agro.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;

public class GetSeeds {

    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("seeds")
    @Expose
    private Seeds seeds;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public Seeds getSeeds() {
        return seeds;
    }

    public void setSeeds(Seeds seeds) {
        this.seeds = seeds;
    }

}
