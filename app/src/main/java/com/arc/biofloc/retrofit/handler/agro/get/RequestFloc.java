package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.FlocDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetFloc;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestFloc extends APICallHandler<Long> {
    public static final String tag = "Floc";
	private  static RequestFloc singleton = null;

	private RequestFloc() {
	}

	public static RequestFloc getInstance() {
		singleton = new RequestFloc();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getFloc(ref1,ref2,ref3, new Callback<GetFloc>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetFloc arg0,
								Response arg1) {
				if(arg0.getFloc() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					FlocDao dao = new FlocDao(CommonApplication.getAppContext());
					for (Floc result: arg0.getFloc()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
