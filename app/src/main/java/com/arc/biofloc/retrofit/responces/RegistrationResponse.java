
package com.arc.biofloc.retrofit.responces;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("user_id")
    @Expose
    private Long userId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("phone_number")
    @Expose
    private List<String> phoneNumber = null;
    @SerializedName("password")
    @Expose
    private List<String> password = null;
    @SerializedName("first_name")
    @Expose
    private List<String> firstName = null;
    @SerializedName("confirm_password")
    @Expose
    private List<String> confirmPassword = null;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(List<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(List<String> firstName) {
        this.firstName = firstName;
    }

    public List<String> getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(List<String> confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
