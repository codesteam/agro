
package com.arc.biofloc.retrofit.responces.agro.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.SeedsRelease;

public class GetSeedsRelease {

    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("seeds_release")
    @Expose
    private SeedsRelease seedsRelease;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public SeedsRelease getSeedsRelease() {
        return seedsRelease;
    }

    public void setSeedsRelease(SeedsRelease seedsRelease) {
        this.seedsRelease = seedsRelease;
    }

}
