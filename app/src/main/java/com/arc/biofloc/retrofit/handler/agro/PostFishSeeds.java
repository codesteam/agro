package com.arc.biofloc.retrofit.handler.agro;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PostFishSeeds extends APICallHandler<Long> {

	private static PostFishSeeds singleton;

	private PostFishSeeds() {
	}

	public static PostFishSeeds getInstance() {
		singleton = new PostFishSeeds();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).seeds(ref1, args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],
				args[10],args[11],args[12],
				new Callback<Seeds>() {

			@Override
			public void failure(RetrofitError arg0) {
			/*	Print.e(this, "RetrofitError response body: "+ arg0.getResponse().getBody().toString());
				Print.e(this, "RetrofitError message: "+ arg0.getMessage());*/

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Seeds arg0,
								Response arg1) {
				Print.e(this, "RegistrationResponse");
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}else{
					Print.e(this, "call back null");
				}
			}

		});


	}

}
