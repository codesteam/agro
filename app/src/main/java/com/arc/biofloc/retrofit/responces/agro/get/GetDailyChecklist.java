
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;

public class GetDailyChecklist {

    @SerializedName("daily_checklist")
    @Expose
    private List<DailyChecklist> dailyChecklist = null;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public List<DailyChecklist> getDailyChecklist() {
        return dailyChecklist;
    }

    public void setDailyChecklist(List<DailyChecklist> dailyChecklist) {
        this.dailyChecklist = dailyChecklist;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
