package com.arc.biofloc.retrofit.sync;

import com.arc.biofloc.retrofit.handler.agro.get.RequestProjects;
import com.arc.biofloc.retrofit.handler.agro.get.RequestTanks;

public class SyncObj {
    String tag;
    int priority;
    boolean synced;
    public SyncObj(String tag){
        this.tag = tag;
        this.synced = false;
        if(tag.equals(RequestProjects.tag)){
            priority = 1;
        }else if(tag.equals(RequestTanks.tag)){
            priority = 2;
        }else{
            priority = 3;
        }
    }

    public int getPriority() {
        return priority;
    }

    public String getTag() {
        return tag;
    }

    public boolean isSynced() {
        return synced;
    }
}
