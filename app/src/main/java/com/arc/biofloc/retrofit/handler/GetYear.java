package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.db.dao.YearDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;

import com.arc.biofloc.retrofit.responces.year.Result;
import com.arc.biofloc.retrofit.responces.year.Year;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetYear extends APICallHandler<Long> {

	private  static GetYear singleton = null;

	private GetYear() {
	}

	public static GetYear getInstance() {
		singleton = new GetYear();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).getYears(args[1], args[2],new Callback<Year>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Year arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_YEARS, arg0.getRespondedAt());
					YearDao dao = new YearDao(CommonApplication.getAppContext());
					for (Result result: arg0.getResults()){
						dao.insert(result);
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});
	}

	private void sendSuccessFeedBack(APIClientResponse callback, Year arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
