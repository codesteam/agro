
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.project.Project;

public class GetProjects {

    @SerializedName("project")
    @Expose
    private List<Project> project = null;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public List<Project> getProject() {
        return project;
    }

    public void setProject(List<Project> project) {
        this.project = project;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
