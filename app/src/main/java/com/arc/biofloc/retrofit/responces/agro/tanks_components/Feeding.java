
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feeding {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("feeding_ts")
    @Expose
    private String feedingTs;
    @SerializedName("legitimate_biomass")
    @Expose
    private String legitimateBiomass;
    @SerializedName("feed_percentage")
    @Expose
    private String feedPercentage;
    @SerializedName("feed_amount")
    @Expose
    private String feedAmount;
    @SerializedName("protein_amount")
    @Expose
    private String proteinAmount;
    @SerializedName("produced_nitrogen")
    @Expose
    private String producedNitrogen;
    @SerializedName("wasted_nitrogen")
    @Expose
    private String wastedNitrogen;
    @SerializedName("carbon_source_type")
    @Expose
    private String carbonSourceType;
    @SerializedName("carbon_percentage")
    @Expose
    private String carbonPercentage;
    @SerializedName("catalyst_amount")
    @Expose
    private String catalystAmount;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;

    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getFeedingTs() {
        return feedingTs;
    }

    public void setFeedingTs(String feedingTs) {
        this.feedingTs = feedingTs;
    }

    public String getLegitimateBiomass() {
        return legitimateBiomass;
    }

    public void setLegitimateBiomass(String legitimateBiomass) {
        this.legitimateBiomass = legitimateBiomass;
    }

    public String getFeedPercentage() {
        return feedPercentage;
    }

    public void setFeedPercentage(String feedPercentage) {
        this.feedPercentage = feedPercentage;
    }

    public String getFeedAmount() {
        return feedAmount;
    }

    public void setFeedAmount(String feedAmount) {
        this.feedAmount = feedAmount;
    }

    public String getProteinAmount() {
        return proteinAmount;
    }

    public void setProteinAmount(String proteinAmount) {
        this.proteinAmount = proteinAmount;
    }

    public String getProducedNitrogen() {
        return producedNitrogen;
    }

    public void setProducedNitrogen(String producedNitrogen) {
        this.producedNitrogen = producedNitrogen;
    }

    public String getWastedNitrogen() {
        return wastedNitrogen;
    }

    public void setWastedNitrogen(String wastedNitrogen) {
        this.wastedNitrogen = wastedNitrogen;
    }

    public String getCarbonSourceType() {
        return carbonSourceType;
    }

    public void setCarbonSourceType(String carbonSourceType) {
        this.carbonSourceType = carbonSourceType;
    }

    public String getCarbonPercentage() {
        return carbonPercentage;
    }

    public void setCarbonPercentage(String carbonPercentage) {
        this.carbonPercentage = carbonPercentage;
    }

    public String getCatalystAmount() {
        return catalystAmount;
    }

    public void setCatalystAmount(String catalystAmount) {
        this.catalystAmount = catalystAmount;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

}
