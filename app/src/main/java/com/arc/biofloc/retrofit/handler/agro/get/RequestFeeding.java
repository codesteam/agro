package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetFeeding;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestFeeding extends APICallHandler<Long> {
	public static final String tag = "Feeding";
	private  static RequestFeeding singleton = null;

	private RequestFeeding() {
	}

	public static RequestFeeding getInstance() {
		singleton = new RequestFeeding();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getFeeding(ref1,ref2, ref3, new Callback<GetFeeding>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetFeeding arg0,
								Response arg1) {
				if(arg0.getFeeding() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					FeedingDao dao = new FeedingDao(CommonApplication.getAppContext());
					for (Feeding result: arg0.getFeeding()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
