package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.dao.ExamHistoriyDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.examhistory.ExamHistory;
import com.arc.biofloc.retrofit.responces.examhistory.Result;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetExamHistory extends APICallHandler<Long> {

	private  static GetExamHistory singleton = null;

	private GetExamHistory() {
	}

	public static GetExamHistory getInstance() {
		singleton = new GetExamHistory();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).getExamHistory(args[1], args[2],args[3],new Callback<ExamHistory>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(ExamHistory arg0,
								Response arg1) {
				if (arg0 != null){
					if (arg0.getResults() != null){
						ExamHistoriyDao examHistoriyDao = new ExamHistoriyDao(CommonApplication.getAppContext());
						for(Result r: arg0.getResults()){
							examHistoriyDao.insert(r);
						}
					}

				}
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, ExamHistory arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
