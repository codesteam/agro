
package com.arc.biofloc.retrofit.responces.agro.tank;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tank {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("project_id")
    @Expose
    private Long projectId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("water_level")
    @Expose
    private String waterLevel;
    @SerializedName("tank_shape")
    @Expose
    private String tankShape;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("conical_height")
    @Expose
    private String conicalHeight;
    @SerializedName("max_capacity")
    @Expose
    private String maxCapacity;
    @SerializedName("start_ts")
    @Expose
    private String startTs;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(String waterLevel) {
        this.waterLevel = waterLevel;
    }

    public String getTankShape() {
        return tankShape;
    }

    public void setTankShape(String tankShape) {
        this.tankShape = tankShape;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getConicalHeight() {
        return conicalHeight;
    }

    public void setConicalHeight(String conicalHeight) {
        this.conicalHeight = conicalHeight;
    }

    public String getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(String maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getStartTs() {
        return startTs;
    }

    public void setStartTs(String startTs) {
        this.startTs = startTs;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}
