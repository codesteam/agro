
package com.arc.biofloc.retrofit.responces.agro.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Harvest;

public class GetHarvest {

    @SerializedName("harvest")
    @Expose
    private Harvest harvest;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public Harvest getHarvest() {
        return harvest;
    }

    public void setHarvest(Harvest harvest) {
        this.harvest = harvest;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
