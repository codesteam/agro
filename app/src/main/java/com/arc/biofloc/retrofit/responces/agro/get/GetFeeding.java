
package com.arc.biofloc.retrofit.responces.agro.get;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;

public class GetFeeding {

    @SerializedName("feeding")
    @Expose
    private List<Feeding> feeding = null;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;

    public List<Feeding> getFeeding() {
        return feeding;
    }

    public void setFeeding(List<Feeding> feeding) {
        this.feeding = feeding;
    }

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

}
