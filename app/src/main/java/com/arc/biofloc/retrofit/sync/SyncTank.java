package com.arc.biofloc.retrofit.sync;

import android.content.Context;

import com.arc.biofloc.R;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.get.RequestTanks;
;

public class SyncTank {
    int position=0;
    Long projectId;
    ApiStatusDao apiStatusDao;
    Context context;
    String baseUrl;
    public static boolean busy;
    APIClientResponse apiClientResponse;
    public SyncTank(Context context, Long projectId, APIClientResponse apiClientResponse){
        if(busy){
            return;
        }
        this.apiStatusDao = new ApiStatusDao(context);
        this.context = context;
        busy=true;
        position = 0;
        baseUrl = context.getString(R.string.base_url);
        this.apiClientResponse = apiClientResponse;
        start();
    }

    private void start(){
        RequestTanks.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                apiClientResponse.onFailure(msg, sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                apiClientResponse.onSuccess(msg, sender);
            }
        },null, (long)apiStatusDao.getRepondedTime(RequestTanks.tag),projectId,null,baseUrl);

    }
}
