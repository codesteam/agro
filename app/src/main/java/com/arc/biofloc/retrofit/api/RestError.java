package com.arc.biofloc.retrofit.api;

import com.google.gson.annotations.SerializedName;

public class RestError {
    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;
}
