
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Harvest {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("harvesting_ts")
    @Expose
    private String harvestingTs;
    @SerializedName("tank_biomass")
    @Expose
    private String tankBiomass;
    @SerializedName("legitimate_biomass")
    @Expose
    private String legitimateBiomass;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getHarvestingTs() {
        return harvestingTs;
    }

    public void setHarvestingTs(String harvestingTs) {
        this.harvestingTs = harvestingTs;
    }

    public String getTankBiomass() {
        return tankBiomass;
    }

    public void setTankBiomass(String tankBiomass) {
        this.tankBiomass = tankBiomass;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public String getLegitimateBiomass() {
        return legitimateBiomass;
    }

    public void setLegitimateBiomass(String legitimateBiomass) {
        this.legitimateBiomass = legitimateBiomass;
    }
}
