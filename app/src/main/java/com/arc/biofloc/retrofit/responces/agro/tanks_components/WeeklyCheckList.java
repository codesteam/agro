
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeeklyCheckList {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("entry_ts")
    @Expose
    private String entryTs;
    @SerializedName("meadin_biomass")
    @Expose
    private String meadinBiomass;
    @SerializedName("legitimate_biomass")
    @Expose
    private String legitimateBiomass;
    @SerializedName("meadin_size")
    @Expose
    private String meadinSize;
    @SerializedName("legitimate_size")
    @Expose
    private String legitimateSize;
    @SerializedName("ammonia_level")
    @Expose
    private String ammoniaLevel;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getMeadinBiomass() {
        return meadinBiomass;
    }

    public void setMeadinBiomass(String meadinBiomass) {
        this.meadinBiomass = meadinBiomass;
    }

    public String getLegitimateBiomass() {
        return legitimateBiomass;
    }

    public void setLegitimateBiomass(String legitimateBiomass) {
        this.legitimateBiomass = legitimateBiomass;
    }

    public String getMeadinSize() {
        return meadinSize;
    }

    public void setMeadinSize(String meadinSize) {
        this.meadinSize = meadinSize;
    }

    public String getLegitimateSize() {
        return legitimateSize;
    }

    public void setLegitimateSize(String legitimateSize) {
        this.legitimateSize = legitimateSize;
    }

    public String getAmmoniaLevel() {
        return ammoniaLevel;
    }

    public void setAmmoniaLevel(String ammoniaLevel) {
        this.ammoniaLevel = ammoniaLevel;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public String getEntryTs() {
        return entryTs;
    }

    public void setEntryTs(String entryTs) {
        this.entryTs = entryTs;
    }
}
