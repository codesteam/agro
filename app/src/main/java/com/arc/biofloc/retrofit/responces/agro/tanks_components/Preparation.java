
package com.arc.biofloc.retrofit.responces.agro.tanks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Preparation {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("tank_id")
    @Expose
    private Long tankId;
    @SerializedName("prep_start_ts")
    @Expose
    private String prepStartTs;
    @SerializedName("created_ts")
    @Expose
    private String createdTs;
    @SerializedName("modified_ts")
    @Expose
    private String modifiedTs;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("ph_level")
    @Expose
    private String phLevel;
    @SerializedName("tds_level")
    @Expose
    private String tdsLevel;
    @SerializedName("do_level")
    @Expose
    private String doLevel;
    @SerializedName("arsenic_level")
    @Expose
    private String arsenicLevel;
    @SerializedName("clorin_level")
    @Expose
    private String clorinLevel;
    @SerializedName("iron_level")
    @Expose
    private String ironLevel;
    @SerializedName("areation_ts")
    @Expose
    private String areationTs;
    @SerializedName("areation_unit")
    @Expose
    private String areationUnit;
    @SerializedName("areation_point")
    @Expose
    private String areationPoint;
    @SerializedName("areation_wait_duration")
    @Expose
    private String areationWaitDuration;
    @SerializedName("ground_water_tds")
    @Expose
    private String groundWaterTds;
    @SerializedName("target_tds")
    @Expose
    private String targetTds;
    @SerializedName("salt_amount")
    @Expose
    private String saltAmount;
    @SerializedName("tds_wait_duration")
    @Expose
    private String tdsWaitDuration;
    @SerializedName("molasses_amount")
    @Expose
    private String molassesAmount;
    @SerializedName("molasses_wait_duration")
    @Expose
    private String molassesWaitDuration;
    @SerializedName("probiotics_name")
    @Expose
    private String probioticsName;
    @SerializedName("probiotics_dosage")
    @Expose
    private String probioticsDosage;
    @SerializedName("probiotic_state")
    @Expose
    private String probioticState;
    @SerializedName("probiotic_strength")
    @Expose
    private String probioticStrength;
    @SerializedName("probiotic_strain")
    @Expose
    private String probioticStrain;

    int synced = 1;

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public String getPrepStartTs() {
        return prepStartTs;
    }

    public void setPrepStartTs(String prepStartTs) {
        this.prepStartTs = prepStartTs;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(String modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPhLevel() {
        return phLevel;
    }

    public void setPhLevel(String phLevel) {
        this.phLevel = phLevel;
    }

    public String getTdsLevel() {
        return tdsLevel;
    }

    public void setTdsLevel(String tdsLevel) {
        this.tdsLevel = tdsLevel;
    }

    public String getDoLevel() {
        return doLevel;
    }

    public void setDoLevel(String doLevel) {
        this.doLevel = doLevel;
    }

    public String getArsenicLevel() {
        return arsenicLevel;
    }

    public void setArsenicLevel(String arsenicLevel) {
        this.arsenicLevel = arsenicLevel;
    }

    public String getClorinLevel() {
        return clorinLevel;
    }

    public void setClorinLevel(String clorinLevel) {
        this.clorinLevel = clorinLevel;
    }

    public String getIronLevel() {
        return ironLevel;
    }

    public void setIronLevel(String ironLevel) {
        this.ironLevel = ironLevel;
    }

    public String getAreationTs() {
        return areationTs;
    }

    public void setAreationTs(String areationTs) {
        this.areationTs = areationTs;
    }

    public String getAreationUnit() {
        return areationUnit;
    }

    public void setAreationUnit(String areationUnit) {
        this.areationUnit = areationUnit;
    }

    public String getAreationPoint() {
        return areationPoint;
    }

    public void setAreationPoint(String areationPoint) {
        this.areationPoint = areationPoint;
    }

    public String getAreationWaitDuration() {
        return areationWaitDuration;
    }

    public void setAreationWaitDuration(String areationWaitDuration) {
        this.areationWaitDuration = areationWaitDuration;
    }

    public String getGroundWaterTds() {
        return groundWaterTds;
    }

    public void setGroundWaterTds(String groundWaterTds) {
        this.groundWaterTds = groundWaterTds;
    }

    public String getTargetTds() {
        return targetTds;
    }

    public void setTargetTds(String targetTds) {
        this.targetTds = targetTds;
    }

    public String getSaltAmount() {
        return saltAmount;
    }

    public void setSaltAmount(String saltAmount) {
        this.saltAmount = saltAmount;
    }

    public String getTdsWaitDuration() {
        return tdsWaitDuration;
    }

    public void setTdsWaitDuration(String tdsWaitDuration) {
        this.tdsWaitDuration = tdsWaitDuration;
    }

    public String getMolassesAmount() {
        return molassesAmount;
    }

    public void setMolassesAmount(String molassesAmount) {
        this.molassesAmount = molassesAmount;
    }

    public String getMolassesWaitDuration() {
        return molassesWaitDuration;
    }

    public void setMolassesWaitDuration(String molassesWaitDuration) {
        this.molassesWaitDuration = molassesWaitDuration;
    }

    public String getProbioticsName() {
        return probioticsName;
    }

    public void setProbioticsName(String probioticsName) {
        this.probioticsName = probioticsName;
    }

    public String getProbioticsDosage() {
        return probioticsDosage;
    }

    public void setProbioticsDosage(String probioticsDosage) {
        this.probioticsDosage = probioticsDosage;
    }

    public String getProbioticState() {
        return probioticState;
    }

    public void setProbioticState(String probioticState) {
        this.probioticState = probioticState;
    }

    public String getProbioticStrength() {
        return probioticStrength;
    }

    public void setProbioticStrength(String probioticStrength) {
        this.probioticStrength = probioticStrength;
    }

    public String getProbioticStrain() {
        return probioticStrain;
    }

    public void setProbioticStrain(String probioticStrain) {
        this.probioticStrain = probioticStrain;
    }

}
