package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetProjects;
import com.arc.biofloc.retrofit.responces.agro.project.Project;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestProjects extends APICallHandler<Long> {
	public static final String tag = "Projects";
	private  static RequestProjects singleton = null;

	private RequestProjects() {
	}

	public static RequestProjects getInstance() {
		singleton = new RequestProjects();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getProjects(ref1,new Callback<GetProjects>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetProjects arg0,
								Response arg1) {
				if(arg0.getProject() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					ProjectDao dao = new ProjectDao(CommonApplication.getAppContext());
					for (Project result: arg0.getProject()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
