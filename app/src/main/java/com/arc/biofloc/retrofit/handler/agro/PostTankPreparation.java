package com.arc.biofloc.retrofit.handler.agro;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PostTankPreparation extends APICallHandler<Long> {

	private static PostTankPreparation singleton;

	private PostTankPreparation() {
	}

	public static PostTankPreparation getInstance() {
		singleton = new PostTankPreparation();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).tankPreparation(ref1,args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],
				args[9],args[10],args[11],args[12],args[13],args[14],args[15],args[16],args[17],args[18],
				args[19],args[20],args[21],args[22],args[23], new Callback<Preparation>() {

			@Override
			public void failure(RetrofitError arg0) {
			/*	Print.e(this, "RetrofitError response body: "+ arg0.getResponse().getBody().toString());
				Print.e(this, "RetrofitError message: "+ arg0.getMessage());*/

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Preparation arg0,
								Response arg1) {
				Print.e(this, "RegistrationResponse");
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}else{
					Print.e(this, "call back null");
				}
			}

		});


	}

}
