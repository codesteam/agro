package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.db.dao.SubjectDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.subject.Subject;
import com.arc.biofloc.retrofit.responces.subject.Result;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetSubject extends APICallHandler<Long> {

	private  static GetSubject singleton = null;

	private GetSubject() {
	}

	public static GetSubject getInstance() {
		singleton = new GetSubject();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).getSubjects(args[1], args[2],new Callback<Subject>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Subject arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_SUBJECTS, arg0.getRespondedAt());
					SubjectDao dao = new SubjectDao(CommonApplication.getAppContext());
					for (Result result: arg0.getResults()){
						dao.insert(result);
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});
	}

	private void sendSuccessFeedBack(APIClientResponse callback, Subject arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
