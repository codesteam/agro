package com.arc.biofloc.retrofit.api;

import android.util.Log;

import retrofit.ErrorHandler;
import retrofit.Profiler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class APIHandler {


	private static RestAdapter restAdapter;
	public static final int CONNECTION_TIME_OUT = 2;
	
    
	private static RestAdapter getRestAdapter(String endPoint) {
		restAdapter = new RestAdapter.Builder()
				.setErrorHandler(new ErrorRetrofitHandlerException())
				.setEndpoint(endPoint)
				.setLogLevel(RestAdapter.LogLevel.FULL)
				//.setProfiler(new CustomProfiler())
				// .setClient(new OkClient(getClient()))
				.build();
		return restAdapter;
	}

	public static APIs getApiInterface(String endPoint) {

		APIs apiS = null;

		try {
			restAdapter = getRestAdapter(endPoint);
			apiS = restAdapter.create(APIs.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return apiS;
	}
	
	static class ErrorRetrofitHandlerException implements ErrorHandler{
	    @Override
	    public Throwable handleError(RetrofitError cause) {
	        /*if (cause.isNetworkError()) {
	            if (cause.getCause() instanceof SocketTimeoutException) {
	               // Log.e("Retrofit Error handler", "SocketTimeoutException, url=" + cause.getUrl());
	            } else {
	                //Log.e("Retrofit Error handler", "NoConnectionException, url=" + cause.getUrl());
	            }
	        } else {
	           // Log.e("Retrofit Error handler", "Error status:" + cause.getResponse().getStatus());
	        }*/

	        return cause;
	    }
	}
	
	static class CustomProfiler implements Profiler<Object>{

		@Override
		public void afterCall(RequestInformation requestInfo,
				long elapsedTime, int statusCode, Object beforeCallData) {
			Log.e("API Handler", String.format("HTTP %d %s %s (%dms) %s",
					statusCode, requestInfo.getMethod(), requestInfo.getRelativePath(), elapsedTime, beforeCallData.toString()));
		}

		@Override
		public Object beforeCall() {
			// TODO Auto-generated method stub
			return new String("Custom profiler");
		}
		
	}
}
