package com.arc.biofloc.retrofit.sync;

import android.content.Context;

import com.arc.biofloc.R;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.get.RequestDailyChecklist;
import com.arc.biofloc.retrofit.handler.agro.get.RequestFeeding;
import com.arc.biofloc.retrofit.handler.agro.get.RequestFeeds;
import com.arc.biofloc.retrofit.handler.agro.get.RequestFloc;
import com.arc.biofloc.retrofit.handler.agro.get.RequestHarvest;
import com.arc.biofloc.retrofit.handler.agro.get.RequestPreparation;
import com.arc.biofloc.retrofit.handler.agro.get.RequestSeeds;
import com.arc.biofloc.retrofit.handler.agro.get.RequestSeedsRelease;
import com.arc.biofloc.retrofit.handler.agro.get.RequestWeeklyChecklist;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.Comparator;

public class SyncTankComponents {
    ArrayList<SyncObj> apis;
    int position=0;
    Long projectId, tankId;
    ApiStatusDao apiStatusDao;
    Context context;
    String baseUrl;
    APIClientResponse apiClientResponse;
    public static boolean busy = false;
    public SyncTankComponents(Context context, Long projectId, Long tankId, APIClientResponse apiClientResponse){
        this.apis = getTankComponents();
        position = 0;
        this.projectId = projectId;
        this.tankId = tankId;
        this.apiStatusDao = new ApiStatusDao(context);
        this.context = context;
        baseUrl = context.getString(R.string.base_url);
       /* if(busy){
            return;
        }
        busy=true;*/
        this.apiClientResponse = apiClientResponse;
        start(0);
    }

    private void start(int pos){
        Print.e(this, "Project id: "+projectId+"SyncTankComponents: start(); pos "+pos );
        if (pos < apis.size()){
            String tag = apis.get(pos).getTag();
            Print.e(this, "SyncTankComponents: start();"+tag );
            if (tag.equals(RequestDailyChecklist.tag)){
                syncDailyCheckList();
            }else if (tag.equals(RequestFeeding.tag)){
                syncFeeding();
            }else if (tag.equals(RequestFeeds.tag)){
                syncFeed();
            }else if (tag.equals(RequestFloc.tag)){
                syncFloc();
            }else if (tag.equals(RequestHarvest.tag)){
                syncHarvest();
            }else if (tag.equals(RequestPreparation.tag)){
                syncPreparaton();
            }else if (tag.equals(RequestSeeds.tag)){
                syncSeeds();
            }else if (tag.equals(RequestSeedsRelease.tag)){
                syncSeedsRelease();
            }else if (tag.equals(RequestWeeklyChecklist.tag)){
                syncWeeklyChecklist();
            }
        }else{
            busy = false;
            apiClientResponse.onSuccess("completed", null);
        }
    }

    private void syncDailyCheckList(){
        RequestDailyChecklist.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);
    }
    private void syncFeeding(){
        RequestFeeding.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncFeed(){
        RequestFeeds.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncFloc(){
        RequestFloc.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncHarvest(){
        RequestHarvest.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncPreparaton(){
        RequestPreparation.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncSeeds(){
        RequestSeeds.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncSeedsRelease(){
        RequestSeedsRelease.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }
    private void syncWeeklyChecklist(){
        RequestWeeklyChecklist.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                position++;
                start(position);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                position++;
                start(position);
            }
        },null, (long)apiStatusDao.getRepondedTime(apis.get(position).getTag()),projectId,tankId,baseUrl);

    }

    public class CustomComparator implements Comparator<SyncObj> {
        @Override
        public int compare(SyncObj o1, SyncObj o2) {
            return (o1.getPriority()+"").compareTo(o2.getPriority()+"");
        }
    }

    public static ArrayList<SyncObj> getTankComponents(){
        ArrayList<SyncObj> syncObjs = new ArrayList<>();
        syncObjs.add(new SyncObj(RequestDailyChecklist.tag));
        syncObjs.add(new SyncObj(RequestFeeding.tag));
        syncObjs.add(new SyncObj(RequestFeeds.tag));
        syncObjs.add(new SyncObj(RequestFloc.tag));
        syncObjs.add(new SyncObj(RequestHarvest.tag));
        syncObjs.add(new SyncObj(RequestPreparation.tag));
        syncObjs.add(new SyncObj(RequestSeeds.tag));
        syncObjs.add(new SyncObj(RequestSeedsRelease.tag));
        syncObjs.add(new SyncObj(RequestWeeklyChecklist.tag));

        return syncObjs;
    }

}
