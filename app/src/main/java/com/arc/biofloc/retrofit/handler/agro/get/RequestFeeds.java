package com.arc.biofloc.retrofit.handler.agro.get;


import com.arc.biofloc.common.CommonApplication;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.FeedsDao;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.agro.get.GetFeeds;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RequestFeeds extends APICallHandler<Long> {
	public static final String tag = "Feeds";
	private  static RequestFeeds singleton = null;

	private RequestFeeds() {
	}

	public static RequestFeeds getInstance() {
		singleton = new RequestFeeds();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2, Long ref3, final String... args) {

		getAPIs(args[0]).getFeed(ref1,ref2, ref3, new Callback<GetFeeds>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(GetFeeds arg0,
								Response arg1) {
				if(arg0.getFeeds() != null){
					ApiStatusDao apiStatusDao = new ApiStatusDao(CommonApplication.getAppContext());
					apiStatusDao.insert(tag, arg0.getRespondedAt());
					FeedsDao dao = new FeedsDao(CommonApplication.getAppContext());
					for (Feed result: arg0.getFeeds()){
						dao.insert(result);
					}
				}
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}

}
