package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.RegistrationResponse;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PostRegistration extends APICallHandler<Long> {

	private static PostRegistration singleton;

	private PostRegistration() {
	}

	public static PostRegistration getInstance() {
		singleton = new PostRegistration();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

		getAPIs(args[0]).registration(args[1], args[2],args[3], args[4], args[5], new Callback<RegistrationResponse>() {

			@Override
			public void failure(RetrofitError arg0) {
				Print.e(this, "RetrofitError response body: "+ arg0.getResponse().getBody().toString());
				Print.e(this, "RetrofitError message: "+ arg0.getMessage());
				Print.e(this, "RetrofitError body: "+ arg0.getBody().toString());
				RegistrationResponse response = (RegistrationResponse)arg0.getBody();
				if (callback != null) {
					callback.onFailure("failed", response);
				}
			}

			@Override
			public void success(RegistrationResponse arg0,
								Response arg1) {
				Print.e(this, "RegistrationResponse");
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}else{
					Print.e(this, "call back null");
				}
			}

		});


	}

}
