package com.arc.biofloc.retrofit.handler;


import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.retrofit.api.APICallHandler;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.responces.subject.Subject;
import com.arc.biofloc.retrofit.responces.submissions.ExamSubmissionsResponce;
import com.arc.biofloc.retrofit.responces.submitexam.SubmitExam;
import com.arc.biofloc.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PutExamPaper extends APICallHandler<Long> {

	private static PutExamPaper singleton;
	SubmitExam submitExam;

	private PutExamPaper(SubmitExam submitExam) {
		this.submitExam = submitExam;
	}

	public static PutExamPaper getInstance(SubmitExam submitExam) {
		singleton = new PutExamPaper(submitExam);
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer,Long ref1, Long ref2,  Long ref3, final String... args) {

         getAPIs(args[0]).submitExam(args[1], submitExam, new Callback<ExamSubmissionsResponce>() {
             @Override
             public void success(ExamSubmissionsResponce examSubmissionsResponce, Response response) {
                 Print.e(this, "abc success");
                 if (callback!=null){
                 	callback.onSuccess("success", examSubmissionsResponce);
				 }
             }

             @Override
             public void failure(RetrofitError error) {
                 Print.e(this, "abc failed");
                 callback.onFailure("failed", error);
             }
         });

	}

	private void sendSuccessFeedBack(APIClientResponse callback, Subject arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
