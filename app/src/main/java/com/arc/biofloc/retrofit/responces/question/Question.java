
package com.arc.biofloc.retrofit.responces.question;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("exam_history_id")
    @Expose
    private Long examHistoryId;
    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("new_balance")
    @Expose
    private String newBalance;
    @SerializedName("results")
    @Expose
    private List<Result> results = null;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public Object getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(String newBalance) {
        this.newBalance = newBalance;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Long getExamHistoryId() {
        return examHistoryId;
    }

    public void setExamHistoryId(Long examHistoryId) {
        this.examHistoryId = examHistoryId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
