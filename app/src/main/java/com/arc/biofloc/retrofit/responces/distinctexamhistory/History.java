
package com.arc.biofloc.retrofit.responces.distinctexamhistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class History {

    @SerializedName("option")
    @Expose
    private Long option;
    @SerializedName("question")
    @Expose
    private Long question;

    public Long getOption() {
        return option;
    }

    public void setOption(Long option) {
        this.option = option;
    }

    public Long getQuestion() {
        return question;
    }

    public void setQuestion(Long question) {
        this.question = question;
    }

}
