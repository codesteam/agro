package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class FlocDao extends BaseDao {


    public static final String tank_id = "tank_id";
    public static final String examine_ts = "examine_ts";
    public static final String examine_duration = "examine_duration";
    public static final String volume = "volume";
    public static final String temperature = "temperature";
    public static final String probiotics = "probiotics";
    public static final String molasses = "molasses";
    public static final String feed = "feed";
    public static final String remark = "remark";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_floc";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            remark+" text, "+
            feed+" text, "+
            molasses+" text, "+
            probiotics+" text, "+
            temperature+" text, "+
            volume+" text, "+
            examine_duration+" text, "+
            examine_ts +" text NOT NULL, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public FlocDao(Context context) {
      super(context);
    }
    public void insert(Floc result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(remark, result.getRemark());
        contentValues.put(feed, result.getFeed());
        contentValues.put(molasses, result.getMolasses());
        contentValues.put(probiotics, result.getProbiotics());
        contentValues.put(temperature, result.getTemperature());
        contentValues.put(volume, result.getVolume());
        contentValues.put(examine_duration, result.getExamineDuration());
        contentValues.put(examine_ts, result.getExamineTs());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Floc result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(remark, result.getRemark());
            contentValues.put(feed, result.getFeed());
            contentValues.put(molasses, result.getMolasses());
            contentValues.put(probiotics, result.getProbiotics());
            contentValues.put(temperature, result.getTemperature());
            contentValues.put(volume, result.getVolume());
            contentValues.put(examine_duration, result.getExamineDuration());
            contentValues.put(examine_ts, result.getExamineTs());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Floc> getResults(long tankId){
        ArrayList<Floc>  flocs = new ArrayList<Floc> ();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" ORDER BY "+examine_ts+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Floc floc = new Floc();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            floc.setId(id);
            floc.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            floc.setExamineTs(cursor.getString(cursor.getColumnIndex(examine_ts)));
            floc.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            floc.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            floc.setExamineDuration(cursor.getString(cursor.getColumnIndex(examine_duration)));
            floc.setVolume(cursor.getString(cursor.getColumnIndex(volume)));
            floc.setTemperature(cursor.getString(cursor.getColumnIndex(temperature)));
            floc.setProbiotics(cursor.getString(cursor.getColumnIndex(probiotics)));
            floc.setMolasses(cursor.getString(cursor.getColumnIndex(molasses)));
            floc.setFeed(cursor.getString(cursor.getColumnIndex(feed)));
            floc.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            cursor.moveToNext();
            flocs.add(floc);
        }
        return flocs;
    }
    public ArrayList<Floc> getUnsynced(long tankId){
        ArrayList<Floc>  flocs = new ArrayList<Floc> ();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Floc floc = new Floc();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            floc.setId(id);
            floc.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            floc.setExamineTs(cursor.getString(cursor.getColumnIndex(examine_ts)));
            floc.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            floc.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            floc.setExamineDuration(cursor.getString(cursor.getColumnIndex(examine_duration)));
            floc.setVolume(cursor.getString(cursor.getColumnIndex(volume)));
            floc.setTemperature(cursor.getString(cursor.getColumnIndex(temperature)));
            floc.setProbiotics(cursor.getString(cursor.getColumnIndex(probiotics)));
            floc.setMolasses(cursor.getString(cursor.getColumnIndex(molasses)));
            floc.setFeed(cursor.getString(cursor.getColumnIndex(feed)));
            floc.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            cursor.moveToNext();
            flocs.add(floc);
        }
        return flocs;
    }
    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
