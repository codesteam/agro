package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.SeedsRelease;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class SeedsReleaseDao extends BaseDao {


    public static final String tank_id = "tank_id";
    public static final String release_ts = "release_ts";
    public static final String quantity = "quantity";
    public static final String temperature_adjustment = "temperature_adjustment";
    public static final String pm_wash = "pm_wash";
    public static final String wash_duration = "wash_duration";
    public static final String hour = "hour";
    public static final String mortality = "mortality";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_seeds_release";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            release_ts+" text, "+
            quantity+" text, "+
            temperature_adjustment+" text, "+
            pm_wash+" text, "+
            wash_duration+" text, "+
            hour+" text, "+
            mortality+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public SeedsReleaseDao(Context context) {
      super(context);
    }
    public void insert(SeedsRelease result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(release_ts, result.getReleaseTs());
        contentValues.put(quantity, result.getQuantity());
        contentValues.put(temperature_adjustment, result.getTemperatureAdjustment());
        contentValues.put(pm_wash, result.getPmWash());
        contentValues.put(wash_duration, result.getWashDuration());
        contentValues.put(hour, result.getHour());
        contentValues.put(mortality, result.getMortality());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(SeedsRelease result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(release_ts, result.getReleaseTs());
            contentValues.put(quantity, result.getQuantity());
            contentValues.put(temperature_adjustment, result.getTemperatureAdjustment());
            contentValues.put(pm_wash, result.getPmWash());
            contentValues.put(wash_duration, result.getWashDuration());
            contentValues.put(hour, result.getHour());
            contentValues.put(mortality, result.getMortality());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public SeedsRelease getResult(long tankId){
        SeedsRelease obj=null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            obj  = new SeedsRelease();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            obj.setId(id);
            obj.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            obj.setReleaseTs(cursor.getString(cursor.getColumnIndex(release_ts)));
            obj.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            obj.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            obj.setQuantity(cursor.getString(cursor.getColumnIndex(quantity)));
            obj.setTemperatureAdjustment(cursor.getString(cursor.getColumnIndex(temperature_adjustment)));
            obj.setPmWash(cursor.getString(cursor.getColumnIndex(pm_wash)));
            obj.setWashDuration(cursor.getString(cursor.getColumnIndex(wash_duration)));
            obj.setHour(cursor.getString(cursor.getColumnIndex(hour)));
            obj.setMortality(cursor.getString(cursor.getColumnIndex(mortality)));
            cursor.moveToNext();

        }
        return obj;
    }
    public ArrayList<SeedsRelease> getUnsynced(long tankId){
        ArrayList<SeedsRelease> seedsReleases= new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            SeedsRelease obj  = new SeedsRelease();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            obj.setId(id);
            obj.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            obj.setReleaseTs(cursor.getString(cursor.getColumnIndex(release_ts)));
            obj.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            obj.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            obj.setQuantity(cursor.getString(cursor.getColumnIndex(quantity)));
            obj.setTemperatureAdjustment(cursor.getString(cursor.getColumnIndex(temperature_adjustment)));
            obj.setPmWash(cursor.getString(cursor.getColumnIndex(pm_wash)));
            obj.setWashDuration(cursor.getString(cursor.getColumnIndex(wash_duration)));
            obj.setHour(cursor.getString(cursor.getColumnIndex(hour)));
            obj.setMortality(cursor.getString(cursor.getColumnIndex(mortality)));
            cursor.moveToNext();
            seedsReleases.add(obj);
        }
        return seedsReleases;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
