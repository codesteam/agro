package com.arc.biofloc.db;

public interface BackgroundRenderer {
    public void updateForeground();
}
