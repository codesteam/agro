package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.utility.Print;


/**
 * Created by water on 12/27/16.
 */

public class ApiStatusDao extends BaseDao {
    public static final String LAST_RESPONCED_TIME = "lrt";
    public static final String TAG = "tag";
    public static String TABLE_NAME = "table_api_status";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            TAG+" text unique not null," +
            LAST_RESPONCED_TIME+" integer)";

    public ApiStatusDao(Context context) {
      super(context);
    }
    public void insert(String tag, Long responcedTime){

        ContentValues contentValues = new ContentValues();
        contentValues.put(TAG, tag);
        contentValues.put(LAST_RESPONCED_TIME, responcedTime);
        try{
            int count = getRowCount(tag);
            Print.e(this, "Tag: "+tag+" , Row count:"+count);
            if (count > 0){
                update(tag, responcedTime);
            }else{
                database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(String tag, Long responcedTime){
        try {
            ContentValues cv = new ContentValues();
            cv.put(LAST_RESPONCED_TIME,responcedTime);
            database.update(TABLE_NAME, cv, TAG+" = '"+tag+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    
    public int getRepondedTime(String tag)
    {
        String q = "select * from "+TABLE_NAME+" where "+TAG+"='"+tag+"'";
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();
        int status = 1234;
        while(res.isAfterLast() == false){
            status = res.getInt(res.getColumnIndex(LAST_RESPONCED_TIME));
            res.moveToNext();
        }
        return status;
    }

    private int getRowCount(String tag) // if return 0 first time run
    {
        String q = "select * from "+TABLE_NAME+" where "+TAG+"='"+tag+"'";
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();
        return res.getCount();
    }
}
