package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class SeedsDao extends BaseDao {
    public static final String tank_id = "tank_id";
    public static final String tank_capacity = "tank_capacity";
    public static final String targetPer = "targetPer";
    public static final String arrival_date = "arrival_date";
    public static final String preparation_details = "preparation_details";
    public static final String initial_avarage_size = "initial_avarage_size";
    public static final String marketable_size = "marketable_size";
    public static final String number_Of_seeds = "number_Of_seeds";
    public static final String release_date = "release_date";
    public static final String harvesting_date = "harvesting_date";
    public static final String seeds_details = "seeds_details";
    public static final String supplier_details = "supplier_details";
    public static final String initial_biomass = "initial_biomass";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_seeds";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            tank_capacity+" text, "+
            targetPer+" text, "+
            arrival_date+" text, "+
            preparation_details+" text, "+
            initial_avarage_size+" text, "+
            marketable_size+" text, "+
            number_Of_seeds+" text, "+
            release_date+" text, "+
            harvesting_date+" text, "+
            supplier_details+" text, "+
            seeds_details+" text, "+
            initial_biomass +" text NOT NULL, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public SeedsDao(Context context) {
      super(context);
    }
    public void insert(Seeds result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(tank_capacity, result.getTankCapacity());
        contentValues.put(targetPer, result.getTarget());
        contentValues.put(arrival_date, result.getArrivalDate());
        contentValues.put(preparation_details, result.getPreparationDetails());
        contentValues.put(initial_avarage_size, result.getInitialAvarageSize());
        contentValues.put(marketable_size, result.getMarketableSize());
        contentValues.put(number_Of_seeds, result.getNumberOfSeeds());
        contentValues.put(release_date, result.getReleaseDate());
        contentValues.put(harvesting_date, result.getHarvestingDate());
        contentValues.put(supplier_details, result.getSupplierDetails());
        contentValues.put(seeds_details, result.getSeedsDetails());
        contentValues.put(initial_biomass, result.getInitialBiomass());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Seeds result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(tank_capacity, result.getTankCapacity());
            contentValues.put(targetPer, result.getTarget());
            contentValues.put(arrival_date, result.getArrivalDate());
            contentValues.put(preparation_details, result.getPreparationDetails());
            contentValues.put(initial_avarage_size, result.getInitialAvarageSize());
            contentValues.put(marketable_size, result.getMarketableSize());
            contentValues.put(number_Of_seeds, result.getNumberOfSeeds());
            contentValues.put(release_date, result.getReleaseDate());
            contentValues.put(harvesting_date, result.getHarvestingDate());
            contentValues.put(supplier_details, result.getSupplierDetails());
            contentValues.put(seeds_details, result.getSeedsDetails());
            contentValues.put(initial_biomass, result.getInitialBiomass());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public Seeds getResult(long tankId){
        Seeds obj = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            obj = new Seeds();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            obj.setId(id);
            obj.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            obj.setTankCapacity(cursor.getString(cursor.getColumnIndex(tank_capacity)));
            obj.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            obj.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            obj.setTarget(cursor.getString(cursor.getColumnIndex(targetPer)));
            obj.setArrivalDate(cursor.getString(cursor.getColumnIndex(arrival_date)));
            obj.setPreparationDetails(cursor.getString(cursor.getColumnIndex(preparation_details)));
            obj.setInitialAvarageSize(cursor.getString(cursor.getColumnIndex(initial_avarage_size)));
            obj.setMarketableSize(cursor.getString(cursor.getColumnIndex(marketable_size)));
            obj.setNumberOfSeeds(cursor.getString(cursor.getColumnIndex(number_Of_seeds)));
            obj.setReleaseDate(cursor.getString(cursor.getColumnIndex(release_date)));
            obj.setHarvestingDate(cursor.getString(cursor.getColumnIndex(harvesting_date)));
            obj.setSupplierDetails(cursor.getString(cursor.getColumnIndex(supplier_details)));
            obj.setSeedsDetails(cursor.getString(cursor.getColumnIndex(seeds_details)));
            obj.setInitialBiomass(cursor.getString(cursor.getColumnIndex(initial_biomass)));
            cursor.moveToNext();
        }
        return obj;
    }
    public ArrayList<Seeds> getUnsynced(long tankId){
        ArrayList<Seeds> seeds = new ArrayList<Seeds>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Seeds  obj = new Seeds();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            obj.setId(id);
            obj.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            obj.setTankCapacity(cursor.getString(cursor.getColumnIndex(tank_capacity)));
            obj.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            obj.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            obj.setTarget(cursor.getString(cursor.getColumnIndex(targetPer)));
            obj.setArrivalDate(cursor.getString(cursor.getColumnIndex(arrival_date)));
            obj.setPreparationDetails(cursor.getString(cursor.getColumnIndex(preparation_details)));
            obj.setInitialAvarageSize(cursor.getString(cursor.getColumnIndex(initial_avarage_size)));
            obj.setMarketableSize(cursor.getString(cursor.getColumnIndex(marketable_size)));
            obj.setNumberOfSeeds(cursor.getString(cursor.getColumnIndex(number_Of_seeds)));
            obj.setReleaseDate(cursor.getString(cursor.getColumnIndex(release_date)));
            obj.setHarvestingDate(cursor.getString(cursor.getColumnIndex(harvesting_date)));
            obj.setSupplierDetails(cursor.getString(cursor.getColumnIndex(supplier_details)));
            obj.setSeedsDetails(cursor.getString(cursor.getColumnIndex(seeds_details)));
            obj.setInitialBiomass(cursor.getString(cursor.getColumnIndex(initial_biomass)));
            cursor.moveToNext();
            seeds.add(obj);
        }
        return seeds;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
