package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class TankDao extends BaseDao {

    public static final String NAME = "project";
    public static final String PROJECT_ID = "project_id";
    public static final String DETAILS = "DETAILS";
    public static final String TYPE = "TYPE";
    public static final String HEIGHT = "HEIGHT";
    public static final String WATER_LEVEL = "WATER_LEVEL";
    public static final String SHAPE = "SHAPE";
    public static final String RADIUS = "RADIUS";
    public static final String CONICAL_HEIGHT = "CONICAL_HEIGHT";
    public static final String MAX_CAPACITY = "MAX_CAPACITY";
    public static final String START_TS = "START_DATE";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_tanks";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            PROJECT_ID+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            START_TS+" text, "+
            DETAILS+" text, "+
            TYPE+" text, "+
            HEIGHT+" text, "+
            WATER_LEVEL+" text, "+
            SHAPE+" text, "+
            RADIUS+" text, "+
            CONICAL_HEIGHT+" text, "+
            MAX_CAPACITY+" text, "+
            NAME +" text NOT NULL, "+
            "FOREIGN KEY("+PROJECT_ID+") REFERENCES "+ProjectDao.TABLE_NAME+"("+ProjectDao.ID+") ON DELETE CASCADE"+");";;

    public TankDao(Context context) {
      super(context);
    }
    public void insert(Tank result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(PROJECT_ID, result.getProjectId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(NAME, result.getName());
        contentValues.put(DETAILS, result.getDetails());
        contentValues.put(TYPE, result.getType());
        contentValues.put(HEIGHT, result.getHeight());
        contentValues.put(WATER_LEVEL, result.getWaterLevel());
        contentValues.put(SHAPE, result.getTankShape());
        contentValues.put(RADIUS, result.getRadius());
        contentValues.put(CONICAL_HEIGHT, result.getConicalHeight());
        contentValues.put(MAX_CAPACITY, result.getMaxCapacity());
        contentValues.put(START_TS, result.getStartTs());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Tank result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(PROJECT_ID, result.getProjectId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(NAME, result.getName());
            contentValues.put(DETAILS, result.getDetails());
            contentValues.put(TYPE, result.getType());
            contentValues.put(HEIGHT, result.getHeight());
            contentValues.put(WATER_LEVEL, result.getWaterLevel());
            contentValues.put(SHAPE, result.getTankShape());
            contentValues.put(RADIUS, result.getRadius());
            contentValues.put(CONICAL_HEIGHT, result.getConicalHeight());
            contentValues.put(MAX_CAPACITY, result.getMaxCapacity());
            contentValues.put(START_TS, result.getStartTs());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Tank> getResults(long projectId){
        ArrayList<Tank> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PROJECT_ID +"="+projectId+" ORDER BY "+START_TS+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
          Tank result = new Tank();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setProjectId(cursor.getLong(cursor.getColumnIndex(PROJECT_ID)));
            result.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setStartTs(cursor.getString(cursor.getColumnIndex(START_TS)));
            result.setDetails(cursor.getString(cursor.getColumnIndex(DETAILS)));
            result.setType(cursor.getString(cursor.getColumnIndex(TYPE)));
            result.setHeight(cursor.getString(cursor.getColumnIndex(HEIGHT)));
            result.setWaterLevel(cursor.getString(cursor.getColumnIndex(WATER_LEVEL)));
            result.setTankShape(cursor.getString(cursor.getColumnIndex(SHAPE)));
            result.setRadius(cursor.getString(cursor.getColumnIndex(RADIUS)));
            result.setConicalHeight(cursor.getString(cursor.getColumnIndex(CONICAL_HEIGHT)));
            result.setMaxCapacity(cursor.getString(cursor.getColumnIndex(MAX_CAPACITY)));
            results.add(result);
            cursor.moveToNext();
        }
        return results;
    }
    public ArrayList<Tank> getUnsynced(long projectId){
        ArrayList<Tank> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PROJECT_ID +"="+projectId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Tank result = new Tank();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setProjectId(cursor.getLong(cursor.getColumnIndex(PROJECT_ID)));
            result.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setStartTs(cursor.getString(cursor.getColumnIndex(START_TS)));
            result.setDetails(cursor.getString(cursor.getColumnIndex(DETAILS)));
            result.setType(cursor.getString(cursor.getColumnIndex(TYPE)));
            result.setHeight(cursor.getString(cursor.getColumnIndex(HEIGHT)));
            result.setWaterLevel(cursor.getString(cursor.getColumnIndex(WATER_LEVEL)));
            result.setTankShape(cursor.getString(cursor.getColumnIndex(SHAPE)));
            result.setRadius(cursor.getString(cursor.getColumnIndex(RADIUS)));
            result.setConicalHeight(cursor.getString(cursor.getColumnIndex(CONICAL_HEIGHT)));
            result.setMaxCapacity(cursor.getString(cursor.getColumnIndex(MAX_CAPACITY)));
            results.add(result);
            cursor.moveToNext();
        }
        return results;
    }

    public Tank getResult(long requestId){
        Print.e(this, "request id: "+requestId);
        Print.e(this, "query: "+"select * from "+ TABLE_NAME +" where id="+""+requestId+"");
        Tank result  = null;
        String q = "select * from "+TABLE_NAME+" where "+ID+"='"+requestId+"'";
        Cursor cursor =  database.rawQuery( q, null );
        cursor.moveToFirst();
        Print.e(this, "courser count: "+cursor.getCount());
        while(cursor.isAfterLast() == false){
            result  =new Tank();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            Print.e(this, "id id : "+id);
            result.setId(id);
            result.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setStartTs(cursor.getString(cursor.getColumnIndex(START_TS)));
            result.setDetails(cursor.getString(cursor.getColumnIndex(DETAILS)));
            result.setType(cursor.getString(cursor.getColumnIndex(TYPE)));
            result.setHeight(cursor.getString(cursor.getColumnIndex(HEIGHT)));
            result.setWaterLevel(cursor.getString(cursor.getColumnIndex(WATER_LEVEL)));
            result.setTankShape(cursor.getString(cursor.getColumnIndex(SHAPE)));
            result.setRadius(cursor.getString(cursor.getColumnIndex(RADIUS)));
            result.setConicalHeight(cursor.getString(cursor.getColumnIndex(CONICAL_HEIGHT)));
            result.setMaxCapacity(cursor.getString(cursor.getColumnIndex(MAX_CAPACITY)));
            cursor.moveToNext();
        }
        return result;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
