package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class FeedsDao extends BaseDao {



    public static final String tank_id = "tank_id";
    public static final String start_date = "start_date";
    public static final String brand = "brand";
    public static final String feed_type = "feed_type";
    public static final String protein_level = "protein_level";
    public static final String fish_age = "fish_age";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static final String synced = "synced";
    public static String TABLE_NAME = "table_feed";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            start_date +" text, "+
            brand+" text, "+
            feed_type+" text, "+
            protein_level+" text, "+
            fish_age+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public FeedsDao(Context context) {
      super(context);
    }
    public void insert(Feed result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(start_date, result.getStartDate());
        contentValues.put(brand, result.getBrand());
        contentValues.put(feed_type, result.getType());
        contentValues.put(protein_level, result.getProteinLevel());
        contentValues.put(fish_age, result.getFishAge());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Feed result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(start_date, result.getStartDate());
            contentValues.put(brand, result.getBrand());
            contentValues.put(feed_type, result.getType());
            contentValues.put(protein_level, result.getProteinLevel());
            contentValues.put(fish_age, result.getFishAge());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Feed> getResults(long tankId){
        ArrayList<Feed> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" ORDER BY "+MODIFIED_TS+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Feed result = new Feed();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setStartDate(cursor.getString(cursor.getColumnIndex(start_date)));
            result.setBrand(cursor.getString(cursor.getColumnIndex(brand)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setType(cursor.getString(cursor.getColumnIndex(feed_type)));
            result.setProteinLevel(cursor.getString(cursor.getColumnIndex(protein_level)));
            result.setFishAge(cursor.getString(cursor.getColumnIndex(fish_age)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }
    public ArrayList<Feed> getUnsynced(long tankId){
        ArrayList<Feed> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Feed result = new Feed();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setStartDate(cursor.getString(cursor.getColumnIndex(start_date)));
            result.setBrand(cursor.getString(cursor.getColumnIndex(brand)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setType(cursor.getString(cursor.getColumnIndex(feed_type)));
            result.setProteinLevel(cursor.getString(cursor.getColumnIndex(protein_level)));
            result.setFishAge(cursor.getString(cursor.getColumnIndex(fish_age)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }

    public Feed getResult(long tankId){
        Feed result = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId +" ORDER BY "+ID+ " ASC limit 1" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            result = new Feed();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setStartDate(cursor.getString(cursor.getColumnIndex(start_date)));
            result.setBrand(cursor.getString(cursor.getColumnIndex(brand)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setType(cursor.getString(cursor.getColumnIndex(feed_type)));
            result.setProteinLevel(cursor.getString(cursor.getColumnIndex(protein_level)));
            result.setFishAge(cursor.getString(cursor.getColumnIndex(fish_age)));
            cursor.moveToNext();

        }
        return result;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
