package com.arc.biofloc.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.arc.biofloc.R;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.DailyChecklistDao;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.db.agrodao.FeedsDao;
import com.arc.biofloc.db.agrodao.FlocDao;
import com.arc.biofloc.db.agrodao.HarvestDao;
import com.arc.biofloc.db.agrodao.PreparationDao;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.db.agrodao.SeedsDao;
import com.arc.biofloc.db.agrodao.SeedsReleaseDao;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.db.agrodao.WeeklyChecklistDao;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.db.dao.BoardDao;
import com.arc.biofloc.db.dao.ExamHistoriyDao;
import com.arc.biofloc.db.dao.PartStatusDao;
import com.arc.biofloc.db.dao.ParticipationDao;
import com.arc.biofloc.db.dao.PartsDao;
import com.arc.biofloc.db.dao.QuestionSetDao;
import com.arc.biofloc.db.dao.SubjectDao;
import com.arc.biofloc.db.dao.UserDao;
import com.arc.biofloc.db.dao.YearDao;

/**
 * Created by water on 3/19/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance = null;

    public static synchronized DBHelper getHelper(Context context) {
        if (instance == null)
            instance = new DBHelper(context);
        return instance;
    }

    public static String getDatabseName(Context context){
        return context.getString(R.string.app_db_name)+".db";
    }
    private DBHelper(Context context)
    {
        super(context, getDatabseName(context), null, 1);
    }
    @Override
    public void onOpen(SQLiteDatabase db){
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(AppStatusDao.CREATE);
        db.execSQL(PartsDao.CREATE);
        db.execSQL(QuestionSetDao.CREATE);
        db.execSQL(ParticipationDao.CREATE);
        db.execSQL(PartStatusDao.CREATE);
        db.execSQL(UserDao.CREATE);
        db.execSQL(BoardDao.CREATE);
        db.execSQL(YearDao.CREATE);
        db.execSQL(SubjectDao.CREATE);
        db.execSQL(ExamHistoriyDao.CREATE);
        db.execSQL(ProjectDao.CREATE);
        db.execSQL(TankDao.CREATE);
        db.execSQL(PreparationDao.CREATE);
        db.execSQL(FlocDao.CREATE);
        db.execSQL(DailyChecklistDao.CREATE);
        db.execSQL(SeedsDao.CREATE);
        db.execSQL(SeedsReleaseDao.CREATE);
        db.execSQL(WeeklyChecklistDao.CREATE);
        db.execSQL(FeedsDao.CREATE);
        db.execSQL(HarvestDao.CREATE);
        db.execSQL(FeedingDao.CREATE);
        db.execSQL(ApiStatusDao.CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
       db.execSQL("DROP TABLE IF EXISTS "+AppStatusDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ QuestionSetDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ParticipationDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ PartsDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ PartStatusDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ UserDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ BoardDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ YearDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ SubjectDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ExamHistoriyDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ FeedingDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ HarvestDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ FeedsDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ SeedsDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ SeedsReleaseDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ WeeklyChecklistDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ DailyChecklistDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ FlocDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ PreparationDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ TankDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ProjectDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ApiStatusDao.TABLE_NAME);

        onCreate(db);
    }
}