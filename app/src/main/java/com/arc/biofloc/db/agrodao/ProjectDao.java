package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;

import com.arc.biofloc.retrofit.responces.agro.project.Project;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class ProjectDao extends BaseDao {

    public static final String PROJECT = "project";
    public static final String synced = "synced";
    public static final String CREATED_AT = "createdAt";
    public static final String MODIFIED_AT = "modifiedAt";
    public static final String ADDRESS = "address";
    public static String TABLE_NAME = "table_project";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_AT+" integer, "+
            MODIFIED_AT+" integer, "+
            synced+" integer, "+
            ADDRESS+" text, "+
            PROJECT +" text NOT NULL)";

    public ProjectDao(Context context) {
      super(context);
    }
    public void insert(Project result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(CREATED_AT, result.getCreatedAt());
        contentValues.put(MODIFIED_AT, result.getModifiedAt());
        contentValues.put(PROJECT, result.getName());
        contentValues.put(ADDRESS, result.getAddress());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Project result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CREATED_AT, result.getCreatedAt());
            contentValues.put(MODIFIED_AT, result.getModifiedAt());
            contentValues.put(PROJECT, result.getName());
            contentValues.put(ADDRESS, result.getAddress());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Project> getResult(){
        ArrayList<Project> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME+" ORDER BY "+MODIFIED_AT+" DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Project result = new Project();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setName(cursor.getString(cursor.getColumnIndex(PROJECT)));
            result.setCreatedAt(cursor.getLong(cursor.getColumnIndex(CREATED_AT)));
            result.setModifiedAt(cursor.getLong(cursor.getColumnIndex(MODIFIED_AT)));
            result.setAddress(cursor.getString(cursor.getColumnIndex(ADDRESS)));
            results.add(result);
            cursor.moveToNext();
        }
        return results;
    }
    public ArrayList<Project> getUnsynced(){
        ArrayList<Project> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME+" where "+synced+" =0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Project result = new Project();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setName(cursor.getString(cursor.getColumnIndex(PROJECT)));
            result.setCreatedAt(cursor.getLong(cursor.getColumnIndex(CREATED_AT)));
            result.setModifiedAt(cursor.getLong(cursor.getColumnIndex(MODIFIED_AT)));
            result.setAddress(cursor.getString(cursor.getColumnIndex(ADDRESS)));
            results.add(result);
            cursor.moveToNext();
        }
        return results;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
