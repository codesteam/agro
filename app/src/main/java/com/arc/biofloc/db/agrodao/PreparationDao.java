package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;

public class PreparationDao extends BaseDao {

    public static String TABLE_NAME= "tank_prepration";
    public static String PrepStartTimeStamp = "PrepStartDate";
    public static String Temperature = "Temperature";
    public static String PHLevel = "PHLevel";
    public static String TDSLevel = "TDSLevel";
    public static String TANK_ID = "TANK_ID";
    public static String DOLevel = "DOLevel";
    public static String ArsenicLevel = "ArsenicLevel";
    public static String ClorinLevel = "ClorinLevel";
    public static String IronLevel = "IronLevel";
    public static String AreationTimeStamp = "AreationTime";
    public static String AreationUnit = "AreationUnit";
    public static String AreationPoint = "AreationPoint";
    public static String AreationWaitDuration = "AreationWaitDuration";
    public static String GroundWaterTDS = "GroundWaterTDS";
    public static String TargetTDS = "TargetTDS";
    public static String SaltAmount = "SaltAmount";
    public static String TDSWaitDuration = "TDSWaitDuration";
    public static String MolassesAmount = "MolassesAmount";
    public static String MolassesWaitDuration = "MolassesWaitDuration";
    public static String ProbioticsName = "ProbioticsName";
    public static String ProbioticsDosage = "ProbioticsDosage";
    public static String ProbioticState = "ProbioticState";
    public static String ProbioticStrength = "ProbioticStrength";
    public static String ProbioticStrain = "ProbioticStrain";
    public static final String synced = "synced";
    public static final String CREATED_TIME_STAMP = "CREATED_AT";
    public static final String MODIFIED_TIME_STAMP = "MODIFIED_AT";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            PrepStartTimeStamp+" text, "+
            CREATED_TIME_STAMP+" text, "+
            MODIFIED_TIME_STAMP+" text, "+
            TANK_ID +" integer, "+
            synced+" integer, "+
            Temperature+" text, "+
            PHLevel+" text, "+
            TDSLevel+" text, "+
            DOLevel+" text, "+
            ArsenicLevel+" text, "+
            ClorinLevel+" text, "+
            IronLevel+" text, "+
            AreationTimeStamp+" text, "+
            AreationUnit+" text, "+
            AreationPoint+" text, "+
            AreationWaitDuration+" text, "+
            GroundWaterTDS+" text, "+
            TargetTDS+" text, "+
            SaltAmount+" text, "+
            TDSWaitDuration+" text, "+
            MolassesAmount+" text, "+
            MolassesWaitDuration+" text, "+
            ProbioticsName+" text, "+
            ProbioticsDosage+" text, "+
            ProbioticState+" text, "+
            ProbioticStrength+" text, "+
            ProbioticStrain+" text, "+
            "FOREIGN KEY("+TANK_ID+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";

    public PreparationDao(Context context) {
        super(context);
    }


    public void insert(Preparation preparation){
        Print.e(this, "entry insertion start: ");
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, preparation.getId());
        contentValues.put(PrepStartTimeStamp, preparation.getPrepStartTs());
        contentValues.put(CREATED_TIME_STAMP, preparation.getCreatedTs());
        contentValues.put(MODIFIED_TIME_STAMP, preparation.getModifiedTs());
        contentValues.put(TANK_ID, preparation.getTankId());
        contentValues.put(Temperature, preparation.getTemperature());
        contentValues.put(PHLevel, preparation.getPhLevel());
        contentValues.put(TDSLevel, preparation.getTdsLevel());
        contentValues.put(DOLevel, preparation.getDoLevel());
        contentValues.put(ArsenicLevel, preparation.getArsenicLevel());
        contentValues.put(ClorinLevel, preparation.getClorinLevel());
        contentValues.put(IronLevel, preparation.getIronLevel());
        contentValues.put(AreationTimeStamp, preparation.getAreationTs());
        contentValues.put(AreationUnit, preparation.getAreationUnit());
        contentValues.put(AreationPoint, preparation.getAreationPoint());
        contentValues.put(AreationWaitDuration, preparation.getAreationWaitDuration());
        contentValues.put(GroundWaterTDS, preparation.getGroundWaterTds());
        contentValues.put(TargetTDS, preparation.getTargetTds());

        contentValues.put(SaltAmount, preparation.getSaltAmount());
        contentValues.put(TDSWaitDuration, preparation.getTdsWaitDuration());
        contentValues.put(MolassesAmount, preparation.getMolassesAmount());
        contentValues.put(MolassesWaitDuration, preparation.getMolassesWaitDuration());
        contentValues.put(ProbioticsName, preparation.getProbioticsName());
        contentValues.put(ProbioticsDosage, preparation.getProbioticsDosage());
        contentValues.put(ProbioticState, preparation.getProbioticState());
        contentValues.put(ProbioticStrength, preparation.getProbioticStrength());
        contentValues.put(ProbioticStrain, preparation.getProbioticStrain());
        contentValues.put(synced, preparation.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(preparation);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public long update(Preparation preparation){
        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, preparation.getId());
            contentValues.put(PrepStartTimeStamp, preparation.getPrepStartTs());
            contentValues.put(CREATED_TIME_STAMP, preparation.getCreatedTs());
            contentValues.put(MODIFIED_TIME_STAMP, preparation.getModifiedTs());
            contentValues.put(TANK_ID, preparation.getTankId());
            contentValues.put(Temperature, preparation.getTemperature());
            contentValues.put(PHLevel, preparation.getPhLevel());
            contentValues.put(TDSLevel, preparation.getTdsLevel());
            contentValues.put(DOLevel, preparation.getDoLevel());
            contentValues.put(ArsenicLevel, preparation.getArsenicLevel());
            contentValues.put(ClorinLevel, preparation.getClorinLevel());
            contentValues.put(IronLevel, preparation.getIronLevel());

            contentValues.put(AreationTimeStamp, preparation.getAreationTs());
            contentValues.put(AreationUnit, preparation.getAreationUnit());
            contentValues.put(AreationPoint, preparation.getAreationPoint());
            contentValues.put(AreationWaitDuration, preparation.getAreationWaitDuration());
            contentValues.put(GroundWaterTDS, preparation.getGroundWaterTds());
            contentValues.put(TargetTDS, preparation.getTargetTds());

            contentValues.put(SaltAmount, preparation.getSaltAmount());
            contentValues.put(TDSWaitDuration, preparation.getTdsWaitDuration());
            contentValues.put(MolassesAmount, preparation.getMolassesAmount());
            contentValues.put(MolassesWaitDuration, preparation.getMolassesWaitDuration());
            contentValues.put(ProbioticsName, preparation.getProbioticsName());
            contentValues.put(ProbioticsDosage, preparation.getProbioticsDosage());
            contentValues.put(ProbioticState, preparation.getProbioticState());
            contentValues.put(ProbioticStrength, preparation.getProbioticStrength());
            contentValues.put(ProbioticStrain, preparation.getProbioticStrain());
            contentValues.put(synced, preparation.getSynced());
            //contentValues.put(TAG, result.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {preparation.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            return 0;
        }
    }

    public Preparation getPreparation(Long tankId){
        Preparation preparation = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+ TANK_ID +"="+tankId, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            preparation = new Preparation();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            preparation.setId(id);
            preparation.setPrepStartTs(cursor.getString(cursor.getColumnIndex(PrepStartTimeStamp)));
            preparation.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TIME_STAMP)));
            preparation.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TIME_STAMP)));
            preparation.setTankId(cursor.getLong(cursor.getColumnIndex(TANK_ID)));
            preparation.setTemperature(cursor.getString(cursor.getColumnIndex(Temperature)));
            preparation.setTdsLevel(cursor.getString(cursor.getColumnIndex(TDSLevel)));
            preparation.setPhLevel(cursor.getString(cursor.getColumnIndex(PHLevel)));
            preparation.setDoLevel(cursor.getString(cursor.getColumnIndex(DOLevel)));
            preparation.setArsenicLevel(cursor.getString(cursor.getColumnIndex(ArsenicLevel)));
            preparation.setClorinLevel(cursor.getString(cursor.getColumnIndex(ClorinLevel)));
            preparation.setIronLevel(cursor.getString(cursor.getColumnIndex(IronLevel)));

            preparation.setAreationTs(cursor.getString(cursor.getColumnIndex(AreationTimeStamp)));
            preparation.setAreationPoint(cursor.getString(cursor.getColumnIndex(AreationPoint)));
            preparation.setAreationUnit(cursor.getString(cursor.getColumnIndex(AreationUnit)));
            preparation.setAreationWaitDuration(cursor.getString(cursor.getColumnIndex(AreationWaitDuration)));
            preparation.setGroundWaterTds(cursor.getString(cursor.getColumnIndex(GroundWaterTDS)));
            preparation.setTargetTds(cursor.getString(cursor.getColumnIndex(TargetTDS)));
            preparation.setSaltAmount(cursor.getString(cursor.getColumnIndex(SaltAmount)));
            preparation.setTdsWaitDuration(cursor.getString(cursor.getColumnIndex(TDSWaitDuration)));
            preparation.setMolassesAmount(cursor.getString(cursor.getColumnIndex(MolassesAmount)));
            preparation.setMolassesWaitDuration(cursor.getString(cursor.getColumnIndex(MolassesWaitDuration)));
            preparation.setProbioticsName(cursor.getString(cursor.getColumnIndex(ProbioticsName)));
            preparation.setProbioticsDosage(cursor.getString(cursor.getColumnIndex(ProbioticsDosage)));
            preparation.setProbioticState(cursor.getString(cursor.getColumnIndex(ProbioticState)));
            preparation.setProbioticStrain(cursor.getString(cursor.getColumnIndex(ProbioticStrain)));
            preparation.setProbioticStrength(cursor.getString(cursor.getColumnIndex(ProbioticStrength)));
            cursor.moveToNext();

        }
        return preparation;
    }
    public ArrayList<Preparation> getUnsynced(Long tankId){
        ArrayList<Preparation>  preparations = new ArrayList<Preparation> ();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+ TANK_ID +"="+tankId+" and "+synced+"=0", null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Preparation  preparation = new Preparation();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            preparation.setId(id);
            preparation.setPrepStartTs(cursor.getString(cursor.getColumnIndex(PrepStartTimeStamp)));
            preparation.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TIME_STAMP)));
            preparation.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TIME_STAMP)));
            preparation.setTankId(cursor.getLong(cursor.getColumnIndex(TANK_ID)));
            preparation.setTemperature(cursor.getString(cursor.getColumnIndex(Temperature)));
            preparation.setTdsLevel(cursor.getString(cursor.getColumnIndex(TDSLevel)));
            preparation.setPhLevel(cursor.getString(cursor.getColumnIndex(PHLevel)));
            preparation.setDoLevel(cursor.getString(cursor.getColumnIndex(DOLevel)));
            preparation.setArsenicLevel(cursor.getString(cursor.getColumnIndex(ArsenicLevel)));
            preparation.setClorinLevel(cursor.getString(cursor.getColumnIndex(ClorinLevel)));
            preparation.setIronLevel(cursor.getString(cursor.getColumnIndex(IronLevel)));

            preparation.setAreationTs(cursor.getString(cursor.getColumnIndex(AreationTimeStamp)));
            preparation.setAreationPoint(cursor.getString(cursor.getColumnIndex(AreationPoint)));
            preparation.setAreationUnit(cursor.getString(cursor.getColumnIndex(AreationUnit)));
            preparation.setAreationWaitDuration(cursor.getString(cursor.getColumnIndex(AreationWaitDuration)));
            preparation.setGroundWaterTds(cursor.getString(cursor.getColumnIndex(GroundWaterTDS)));
            preparation.setTargetTds(cursor.getString(cursor.getColumnIndex(TargetTDS)));
            preparation.setSaltAmount(cursor.getString(cursor.getColumnIndex(SaltAmount)));
            preparation.setTdsWaitDuration(cursor.getString(cursor.getColumnIndex(TDSWaitDuration)));
            preparation.setMolassesAmount(cursor.getString(cursor.getColumnIndex(MolassesAmount)));
            preparation.setMolassesWaitDuration(cursor.getString(cursor.getColumnIndex(MolassesWaitDuration)));
            preparation.setProbioticsName(cursor.getString(cursor.getColumnIndex(ProbioticsName)));
            preparation.setProbioticsDosage(cursor.getString(cursor.getColumnIndex(ProbioticsDosage)));
            preparation.setProbioticState(cursor.getString(cursor.getColumnIndex(ProbioticState)));
            preparation.setProbioticStrain(cursor.getString(cursor.getColumnIndex(ProbioticStrain)));
            preparation.setProbioticStrength(cursor.getString(cursor.getColumnIndex(ProbioticStrength)));
            cursor.moveToNext();
            preparations.add(preparation);

        }
        return preparations;
    }


}
