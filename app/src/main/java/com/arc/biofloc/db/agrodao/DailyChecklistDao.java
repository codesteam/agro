package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class DailyChecklistDao extends BaseDao {

    public static final String tank_id = "tank_id";
    public static final String entry_ts = "entry_ts";
    public static final String temperature = "temperature";
    public static final String ph_level = "ph_level";
    public static final String tds_level = "tds_level";
    public static final String do_level = "do_level";
    public static final String ammonia_level = "ammonia_level";
    public static final String mortality = "mortality";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static final String synced = "synced";
    public static String TABLE_NAME = "table_daily_checklist";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            entry_ts+" text, "+
            MODIFIED_TS+" text, "+
            ph_level+" text, "+
            tds_level+" text, "+
            do_level+" text, "+
            ammonia_level+" text, "+
            temperature+" text, "+
            mortality+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public DailyChecklistDao(Context context) {
      super(context);
    }
    public void insert(DailyChecklist result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(entry_ts, result.getEntryTs());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(ph_level, result.getPhLevel());
        contentValues.put(tds_level, result.getTdsLevel());
        contentValues.put(ammonia_level, result.getAmmoniaLevel());
        contentValues.put(do_level, result.getDoLevel());
        contentValues.put(mortality, result.getMortality());
        contentValues.put(temperature, result.getTemperature());
        contentValues.put(synced, result.getSynced());
        Print.e(this, "get ph level: "+result.getPhLevel());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(DailyChecklist result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(entry_ts, result.getEntryTs());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(ph_level, result.getPhLevel());
            contentValues.put(tds_level, result.getTdsLevel());
            contentValues.put(ammonia_level, result.getAmmoniaLevel());
            contentValues.put(do_level, result.getDoLevel());
            contentValues.put(mortality, result.getMortality());
            contentValues.put(temperature, result.getTemperature());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public  ArrayList<DailyChecklist>  getResults(long tankId){
        ArrayList<DailyChecklist> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" ORDER BY "+entry_ts+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            DailyChecklist result = new DailyChecklist();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setDoLevel(cursor.getString(cursor.getColumnIndex(do_level)));
            result.setTdsLevel(cursor.getString(cursor.getColumnIndex(tds_level)));
            result.setTemperature(cursor.getString(cursor.getColumnIndex(temperature)));
            result.setPhLevel(cursor.getString(cursor.getColumnIndex(ph_level)));
            result.setMortality(cursor.getString(cursor.getColumnIndex(mortality)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }

    public  ArrayList<DailyChecklist>  getUnsynced(long tankId){
        ArrayList<DailyChecklist> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            DailyChecklist result = new DailyChecklist();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setDoLevel(cursor.getString(cursor.getColumnIndex(do_level)));
            result.setTdsLevel(cursor.getString(cursor.getColumnIndex(tds_level)));
            result.setTemperature(cursor.getString(cursor.getColumnIndex(temperature)));
            result.setPhLevel(cursor.getString(cursor.getColumnIndex(ph_level)));
            result.setMortality(cursor.getString(cursor.getColumnIndex(mortality)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }

    public DailyChecklist getResult(long tankId){
        DailyChecklist result = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId +" ORDER BY "+entry_ts+ " DESC limit 1" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            result = new DailyChecklist();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setDoLevel(cursor.getString(cursor.getColumnIndex(do_level)));
            result.setTdsLevel(cursor.getString(cursor.getColumnIndex(tds_level)));
            result.setTemperature(cursor.getString(cursor.getColumnIndex(temperature)));
            result.setPhLevel(cursor.getString(cursor.getColumnIndex(ph_level)));
            result.setMortality(cursor.getString(cursor.getColumnIndex(mortality)));
            cursor.moveToNext();
        }
        return result;
    }

    public int getSumOfMortality(long tankId){
        ArrayList<DailyChecklist> dailyChecklists = getResults(tankId);
        int mortality = 0;
        if(dailyChecklists != null){
            for (DailyChecklist dailyChecklist : dailyChecklists){
                mortality+=Integer.valueOf(dailyChecklist.getMortality());
            }
        }
        return mortality;
    }
    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
