package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class WeeklyChecklistDao extends BaseDao {
    public static final String tank_id = "tank_id";
    public static final String entry_ts = "entry_ts";
    public static final String meadin_biomass = "meadin_biomass";
    public static final String legitimate_biomass = "legitimate_biomass";
    public static final String meadin_size = "meadin_size";
    public static final String legitimate_size = "legitimate_size";
    public static final String ammonia_level = "ammonia_level";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_weekly_checklist";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            entry_ts+" text, "+
            meadin_biomass+" text, "+
            legitimate_biomass+" text, "+
            meadin_size+" text, "+
            legitimate_size+" text, "+
            ammonia_level+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public WeeklyChecklistDao(Context context) {
      super(context);
    }
    public void insert(WeeklyCheckList result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(entry_ts, result.getEntryTs());
        contentValues.put(meadin_biomass, result.getMeadinBiomass());
        contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
        contentValues.put(meadin_size, result.getMeadinSize());
        contentValues.put(legitimate_size, result.getLegitimateSize());
        contentValues.put(ammonia_level, result.getAmmoniaLevel());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(WeeklyCheckList result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(entry_ts, result.getEntryTs());
            contentValues.put(meadin_biomass, result.getMeadinBiomass());
            contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
            contentValues.put(meadin_size, result.getMeadinSize());
            contentValues.put(legitimate_size, result.getLegitimateSize());
            contentValues.put(ammonia_level, result.getAmmoniaLevel());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<WeeklyCheckList> getResults(long tankId){
        ArrayList<WeeklyCheckList> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" ORDER BY "+entry_ts+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            WeeklyCheckList result = new WeeklyCheckList();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setLegitimateSize(cursor.getString(cursor.getColumnIndex(legitimate_size)));
            result.setMeadinSize(cursor.getString(cursor.getColumnIndex(meadin_size)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setMeadinBiomass(cursor.getString(cursor.getColumnIndex(meadin_biomass)));

            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }
    public ArrayList<WeeklyCheckList> getUnsynced(long tankId){
        ArrayList<WeeklyCheckList> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            WeeklyCheckList result = new WeeklyCheckList();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setLegitimateSize(cursor.getString(cursor.getColumnIndex(legitimate_size)));
            result.setMeadinSize(cursor.getString(cursor.getColumnIndex(meadin_size)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setMeadinBiomass(cursor.getString(cursor.getColumnIndex(meadin_biomass)));

            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }

    public WeeklyCheckList getResult(long tankId){
        WeeklyCheckList result =null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId +" ORDER BY "+entry_ts+ " ASC limit 1" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            result = new WeeklyCheckList();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setEntryTs(cursor.getString(cursor.getColumnIndex(entry_ts)));
            result.setAmmoniaLevel(cursor.getString(cursor.getColumnIndex(ammonia_level)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setLegitimateSize(cursor.getString(cursor.getColumnIndex(legitimate_size)));
            result.setMeadinSize(cursor.getString(cursor.getColumnIndex(meadin_size)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setMeadinBiomass(cursor.getString(cursor.getColumnIndex(meadin_biomass)));

            cursor.moveToNext();

        }
        return result;
    }
    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
