package com.arc.biofloc.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.retrofit.responces.User;
import com.arc.biofloc.utility.Print;

public class UserDao extends BaseDao {

    public static String TABLE_NAME= "table_user";
    public static String PHONE = "PHONE";
    public static String IMAGE = "image";
    public static String FULL_NAME = "FULL_NAME";
    public static String EMAIL = "EMAIL";
    public static String EXAM_APPEARD = "EXAM_APPEARD";
    public static String REWARD_POINT = "REWARD_POINT";
    public static String BALANCE = "BALANCE";
    public static String LAST_MODIFIED = "last_modified";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            FULL_NAME +" text NOT NULL, "+
            EMAIL +" text, "+
            REWARD_POINT +" integer, "+
            BALANCE +" text, "+
            EXAM_APPEARD +" integer, "+
            LAST_MODIFIED+" text, "+
            IMAGE+" text, "+
            PHONE +" text NOT NULL"+");";

    //public static String ORDER_BY = " ORDER BY "+ FULL_NAME +" ASC";
    public UserDao(Context context) {
        super(context);
    }


    public void insert(User result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(PHONE, result.getPhone());
        contentValues.put(FULL_NAME, result.getFullName());
        contentValues.put(LAST_MODIFIED, result.getModifiedAt());
        contentValues.put(BALANCE, result.getBalance());
        contentValues.put(REWARD_POINT, result.getRewardPoint());
        contentValues.put(EXAM_APPEARD, result.getExamAppeared());
        contentValues.put(EMAIL, result.getEmail());
        contentValues.put(IMAGE, result.getImage());

            try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public long update(User result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PHONE, result.getPhone());
            contentValues.put(FULL_NAME, result.getFullName());
            contentValues.put(LAST_MODIFIED, result.getModifiedAt());
            contentValues.put(BALANCE, result.getBalance());
            contentValues.put(REWARD_POINT, result.getRewardPoint());
            contentValues.put(EXAM_APPEARD, result.getExamAppeared());
            contentValues.put(EMAIL, result.getEmail());
            contentValues.put(IMAGE, result.getImage());
            //contentValues.put(TAG, result.getTag()); // TAG NOT UPDATABLE
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {result.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Parts: "+result.getFullName());
            return 0;
        }
    }


    public User getUser(Long userId){
        User user = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME + " WHERE "+ID+"="+userId , null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            user = new User();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            user.setId(id);
            user.setPhone(cursor.getString(cursor.getColumnIndex(PHONE)));
            user.setFullName(cursor.getString(cursor.getColumnIndex(FULL_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            user.setBalance(cursor.getString(cursor.getColumnIndex(BALANCE)));
            user.setModifiedAt(cursor.getLong(cursor.getColumnIndex(LAST_MODIFIED)));
            user.setExamAppeared(cursor.getLong(cursor.getColumnIndex(EXAM_APPEARD)));
            user.setRewardPoint(cursor.getLong(cursor.getColumnIndex(REWARD_POINT)));
            cursor.moveToNext();
        }
        return user;
    }

}
