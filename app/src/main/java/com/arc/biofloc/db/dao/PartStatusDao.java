package com.arc.biofloc.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.utility.Print;


/**
 * Created by water on 12/27/16.
 */

public class PartStatusDao extends BaseDao {
    public static final String LAST_RESPONCED_TIME = "lrt";
    public static final String TAG = "tag";
    public static String TABLE_NAME = "table_part_status";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            TAG+" text unique not null," +
            LAST_RESPONCED_TIME+" text)";

    public PartStatusDao(Context context) {
      super(context);
    }
    public void insert(String tag, Long responcedTime){

        ContentValues contentValues = new ContentValues();
        contentValues.put(TAG, tag);
        contentValues.put(LAST_RESPONCED_TIME, responcedTime);
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(tag, responcedTime);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(String tag, Long responcedTime){
        try {
            ContentValues cv = new ContentValues();
            cv.put(LAST_RESPONCED_TIME,responcedTime);
            database.update(TABLE_NAME, cv, TAG+" = '"+tag+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
 /*   public void insert(String tag_, Long responcedTime){
        String sql = "insert or replace into "+TABLE_NAME+" ("+TAG+", "+LAST_RESPONCED_TIME+") values ((select "+TAG+","+LAST_RESPONCED_TIME+" from "+TABLE_NAME+" where "+TAG+" = "+tag_+"));";
        //String sql2 = "INSERT OR REPLACE INTO table_part_status (last_responced_time, tag) VALUES ((SELECT ID FROM class WHERE tag = ‘search_name’),‘last_responced_time’,’tag’);";
        String sql3 = "INSERT OR REPLACE INTO table_part_status(tag, lrt) SELECT tag, lrt FROM table_part_status WHERE tag = "+tag_;
        String sql4 = "INSERT OR REPLACE INTO table_part_status (tag,lrt) VALUES(1,2) WHERE tag = '1'";

        String sql5 = "INSERT OR REPLACE INTO table_part_status (tag, lrt) VALUES (?,COALESCE((SELECT lrt FROM table_part_status WHERE tag = '"+tag_+"'), ?));";
        //String sql6 = "INSERT INTO table_part_status (tag, lrt) VALUES (?,?);";
        database.execSQL(sql5, new String[]{tag_,responcedTime+""});
        //database.rawQuery(sql6, null);
        Print.e(this, "PartStatusDao: inserted "+responcedTime);
        *//* ContentValues contentValues = new ContentValues();
        contentValues.put(TAG, tag);
        contentValues.put(LAST_RESPONCED_TIME, responcedTime);
        try{
            database.insert(TABLE_NAME, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }*//*

    }*/
    
    public int getStatus(String tag)
    {
        String q = "select * from "+TABLE_NAME+" where "+TAG+"='"+tag+"'";
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();
        int status = 0;
        while(res.isAfterLast() == false){
            status = res.getInt(res.getColumnIndex(LAST_RESPONCED_TIME));
            res.moveToNext();
        }
        return status;
    }
}
