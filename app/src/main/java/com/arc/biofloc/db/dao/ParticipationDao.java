package com.arc.biofloc.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.retrofit.responces.participation.Result;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;

public class ParticipationDao extends BaseDao {

    public static String TABLE_NAME= "participation";
    public static String TITLE = "title";
    public static String IMAGE = "image";
    public static String PRICE = "price";
    public static String PART_ID = "part_id";
    public static String PARENT_ID = "parent_id";
    public static String ORDER = "order_";
    public static String LAST_MODIFIED = "last_modified";
    public static String FIRST_LETTER = "first_letter";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            ORDER+" integer, "+
            PARENT_ID+" integer, "+
            PART_ID+" integer, "+
            LAST_MODIFIED+" text, "+
            PRICE+" text, "+
            IMAGE+" text, "+
            FIRST_LETTER+" text, "+
            TITLE+" text NOT NULL, "+
            "FOREIGN KEY("+PART_ID+") REFERENCES "+PartsDao.TABLE_NAME+"("+PartsDao.ID+") ON DELETE CASCADE"+");";

    public static String ORDER_BY = " ORDER BY "+ORDER+" ASC";
    public ParticipationDao(Context context) {
        super(context);
    }


    public void insert(Result result){
        Print.e(this, "entry insertion start: ");
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(PARENT_ID, result.getParent());
        contentValues.put(TITLE, result.getTitle());
        contentValues.put(PART_ID, result.getParticipationType());
        contentValues.put(PRICE, result.getPrice());
        contentValues.put(ORDER, result.getOrder());
        contentValues.put(LAST_MODIFIED, result.getModifiedAt());
        contentValues.put(IMAGE, result.getImage());
        contentValues.put(FIRST_LETTER, result.getFirstLetter());
            try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public long update(Result result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(PARENT_ID, result.getParent());
            contentValues.put(TITLE, result.getTitle());
            contentValues.put(PART_ID, result.getParticipationType());
            contentValues.put(PRICE, result.getPrice());
            contentValues.put(ORDER, result.getOrder());
            contentValues.put(LAST_MODIFIED, result.getModifiedAt());
            contentValues.put(IMAGE, result.getImage());
            contentValues.put(FIRST_LETTER, result.getFirstLetter());
            //contentValues.put(TAG, result.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {result.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Parts: "+result.getTitle());
            return 0;
        }
    }


    public ArrayList<Result> getParents(String partId){
        ArrayList<Result> examtypes = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME+" where "+PARENT_ID+" IS NULL AND "+PART_ID+"="+partId+" "+ORDER_BY, null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Result row = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            row.setId(id);
            row.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            row.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
            row.setParticipationType(cursor.getLong(cursor.getColumnIndex(PART_ID)));
            row.setPrice(cursor.getString(cursor.getColumnIndex(PRICE)));
            row.setFirstLetter(cursor.getString(cursor.getColumnIndex(FIRST_LETTER)));
            examtypes.add(row);
            cursor.moveToNext();
        }
        return examtypes;
    }
    public ArrayList<Result> getChilds(Long parentId){
        ArrayList<Result> examtypes = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PARENT_ID+"="+parentId+" "+ORDER_BY, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Result row = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            row.setId(id);
            row.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            row.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
            row.setParticipationType(cursor.getLong(cursor.getColumnIndex(PART_ID)));
            row.setPrice(cursor.getString(cursor.getColumnIndex(PRICE)));
            row.setFirstLetter(cursor.getString(cursor.getColumnIndex(FIRST_LETTER)));
            examtypes.add(row);
            cursor.moveToNext();
        }
        return examtypes;
    }

    public int childCount(String parentId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PARENT_ID+"="+parentId+" "+ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }

    public int count(String partId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PART_ID+"="+partId+" "+ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
