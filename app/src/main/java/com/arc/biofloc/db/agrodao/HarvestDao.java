package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Harvest;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class HarvestDao extends BaseDao {

    public static final String tank_id = "tank_id";
    public static final String harvesting_ts = "harvesting_ts";
    public static final String tank_biomass = "tank_biomass";
    public static final String legitimate_biomass = "legitimate_biomass";
    public static final String remark = "remark";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_harvest";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            MODIFIED_TS+" text, "+
            remark+" text, "+
            harvesting_ts+" text, "+
            tank_biomass+" text, "+
            legitimate_biomass+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public HarvestDao(Context context) {
      super(context);
    }
    public void insert(Harvest result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(remark, result.getRemark());
        contentValues.put(harvesting_ts, result.getHarvestingTs());
        contentValues.put(tank_biomass, result.getTankBiomass());
        contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Harvest result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(remark, result.getRemark());
            contentValues.put(harvesting_ts, result.getHarvestingTs());
            contentValues.put(tank_biomass, result.getTankBiomass());
            contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public Harvest getResult(long tankId){
        Harvest result = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result = new Harvest();
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setHarvestingTs(cursor.getString(cursor.getColumnIndex(harvesting_ts)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setTankBiomass(cursor.getString(cursor.getColumnIndex(tank_biomass)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            result.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            cursor.moveToNext();
        }
        return result;
    }
    public ArrayList<Harvest> getUnsynced(long tankId){
        ArrayList<Harvest>  harvests = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            Harvest result = new Harvest();
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setHarvestingTs(cursor.getString(cursor.getColumnIndex(harvesting_ts)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setTankBiomass(cursor.getString(cursor.getColumnIndex(tank_biomass)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            result.setRemark(cursor.getString(cursor.getColumnIndex(remark)));
            cursor.moveToNext();
            harvests.add(result);
        }
        return harvests;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
