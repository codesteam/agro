package com.arc.biofloc.db.agrodao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arc.biofloc.db.dao.BaseDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class FeedingDao extends BaseDao {

    public static final String tank_id = "tank_id";
    public static final String feeding_ts = "feeding_ts";
    public static final String legitimate_biomass = "legitimate_biomass";
    public static final String percentage = "percentage";
    public static final String feed_amount = "feed_amount";
    public static final String protein_amount = "protein_amount";
    public static final String produced_nitrogen = "produced_nitrogen";
    public static final String wasted_nitrogen = "wasted_nitrogen";
    public static final String carbon_source_type = "carbon_source_type";
    public static final String carbon_source = "carbon_source";
    public static final String catalyst_amount = "catalyst_amount";
    public static final String synced = "synced";
    public static final String CREATED_TS = "CREATED_AT";
    public static final String MODIFIED_TS = "MODIFIED_AT";
    public static String TABLE_NAME = "table_feeding";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            CREATED_TS+" text, "+
            tank_id+" integer, "+
            synced+" integer, "+
            feeding_ts+" text, "+
            MODIFIED_TS+" text, "+
            legitimate_biomass+" text, "+
            percentage+" text, "+
            feed_amount+" text, "+
            protein_amount+" text, "+
            produced_nitrogen+" text, "+
            wasted_nitrogen+" text, "+
            carbon_source_type+" text, "+
            carbon_source+" text, "+
            catalyst_amount+" text, "+
            "FOREIGN KEY("+tank_id+") REFERENCES "+TankDao.TABLE_NAME+"("+TankDao.ID+") ON DELETE CASCADE"+");";;

    public FeedingDao(Context context) {
      super(context);
    }
    public void insert(Feeding result){

        ContentValues contentValues = new ContentValues();
        Print.e(this, "entry insertion id: "+result.getId());
        contentValues.put(ID, result.getId());
        contentValues.put(tank_id, result.getTankId());
        contentValues.put(feeding_ts, result.getFeedingTs());
        contentValues.put(CREATED_TS, result.getCreatedTs());
        contentValues.put(MODIFIED_TS, result.getModifiedTs());
        contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
        contentValues.put(percentage, result.getFeedPercentage());
        contentValues.put(feed_amount, result.getFeedAmount());
        contentValues.put(protein_amount, result.getProteinAmount());
        contentValues.put(produced_nitrogen, result.getProducedNitrogen());
        contentValues.put(wasted_nitrogen, result.getWastedNitrogen());
        contentValues.put(carbon_source_type, result.getCarbonSourceType());
        contentValues.put(carbon_source, result.getCarbonPercentage());
        contentValues.put(catalyst_amount, result.getCatalystAmount());
        contentValues.put(synced, result.getSynced());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Feeding result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, result.getId());
            contentValues.put(tank_id, result.getTankId());
            contentValues.put(feeding_ts, result.getFeedingTs());
            contentValues.put(CREATED_TS, result.getCreatedTs());
            contentValues.put(MODIFIED_TS, result.getModifiedTs());
            contentValues.put(legitimate_biomass, result.getLegitimateBiomass());
            contentValues.put(percentage, result.getFeedPercentage());
            contentValues.put(feed_amount, result.getFeedAmount());
            contentValues.put(protein_amount, result.getProteinAmount());
            contentValues.put(produced_nitrogen, result.getProducedNitrogen());
            contentValues.put(wasted_nitrogen, result.getWastedNitrogen());
            contentValues.put(carbon_source_type, result.getCarbonSourceType());
            contentValues.put(carbon_source, result.getCarbonPercentage());
            contentValues.put(catalyst_amount, result.getCatalystAmount());
            contentValues.put(synced, result.getSynced());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public  ArrayList<Feeding>  getResults(long tankId){
        ArrayList<Feeding> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" ORDER BY "+feeding_ts+ " DESC" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Feeding result = new Feeding();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setFeedingTs(cursor.getString(cursor.getColumnIndex(feeding_ts)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setFeedPercentage(cursor.getString(cursor.getColumnIndex(percentage)));
            result.setFeedAmount(cursor.getString(cursor.getColumnIndex(feed_amount)));
            result.setProteinAmount(cursor.getString(cursor.getColumnIndex(protein_amount)));
            result.setProducedNitrogen(cursor.getString(cursor.getColumnIndex(produced_nitrogen)));
            result.setWastedNitrogen(cursor.getString(cursor.getColumnIndex(wasted_nitrogen)));
            result.setCarbonSourceType(cursor.getString(cursor.getColumnIndex(carbon_source_type)));
            result.setCarbonPercentage(cursor.getString(cursor.getColumnIndex(carbon_source)));
            result.setCatalystAmount(cursor.getString(cursor.getColumnIndex(catalyst_amount)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }
    public  ArrayList<Feeding>  getUnsynced(long tankId){
        ArrayList<Feeding> results = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId+" and "+synced+"=0" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            Feeding result = new Feeding();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setFeedingTs(cursor.getString(cursor.getColumnIndex(feeding_ts)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setFeedPercentage(cursor.getString(cursor.getColumnIndex(percentage)));
            result.setFeedAmount(cursor.getString(cursor.getColumnIndex(feed_amount)));
            result.setProteinAmount(cursor.getString(cursor.getColumnIndex(protein_amount)));
            result.setProducedNitrogen(cursor.getString(cursor.getColumnIndex(produced_nitrogen)));
            result.setWastedNitrogen(cursor.getString(cursor.getColumnIndex(wasted_nitrogen)));
            result.setCarbonSourceType(cursor.getString(cursor.getColumnIndex(carbon_source_type)));
            result.setCarbonPercentage(cursor.getString(cursor.getColumnIndex(carbon_source)));
            result.setCatalystAmount(cursor.getString(cursor.getColumnIndex(catalyst_amount)));
            cursor.moveToNext();
            results.add(result);
        }
        return results;
    }
    public  Feeding  getResult(long tankId){
        Feeding result = null;
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+tank_id+"="+tankId +" ORDER BY "+feeding_ts+ " ASC limit 1" , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            result = new Feeding();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            result.setId(id);
            result.setTankId(cursor.getLong(cursor.getColumnIndex(tank_id)));
            result.setFeedingTs(cursor.getString(cursor.getColumnIndex(feeding_ts)));
            result.setLegitimateBiomass(cursor.getString(cursor.getColumnIndex(legitimate_biomass)));
            result.setCreatedTs(cursor.getString(cursor.getColumnIndex(CREATED_TS)));
            result.setModifiedTs(cursor.getString(cursor.getColumnIndex(MODIFIED_TS)));
            result.setFeedPercentage(cursor.getString(cursor.getColumnIndex(percentage)));
            result.setFeedAmount(cursor.getString(cursor.getColumnIndex(feed_amount)));
            result.setProteinAmount(cursor.getString(cursor.getColumnIndex(protein_amount)));
            result.setProducedNitrogen(cursor.getString(cursor.getColumnIndex(produced_nitrogen)));
            result.setWastedNitrogen(cursor.getString(cursor.getColumnIndex(wasted_nitrogen)));
            result.setCarbonSourceType(cursor.getString(cursor.getColumnIndex(carbon_source_type)));
            result.setCarbonPercentage(cursor.getString(cursor.getColumnIndex(carbon_source)));
            result.setCatalystAmount(cursor.getString(cursor.getColumnIndex(catalyst_amount)));
            cursor.moveToNext();
        }
        return result;
    }
    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
