package com.arc.biofloc.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.arc.biofloc.activity.agro.LoginActivity;
import com.arc.biofloc.activity.MainActivity;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class CommonConstants {
    public static final int ERROR_CODE_EXAM_PARICIPITED = 403;
    public static final int ERROR_CODE_PAYMENT_REQUIRED = 402;
    public static final boolean OFFLINE_MODE = false;
    public static final String TAG_SUGGESTION = "suggestion";
    public static final String TAG_PBQ = "pbq";
    public static final String TAG_MODEL_TEST = "model_test";
    public static final String TAG_DAILY_EXAM = "dally_exam";
    public static final String TAG_PACKAGE = "package";
    public static final String TAG_EXAM_HISTORY = "exam_history";
    public static final String TAG_NOTICE_BOARD = "notice";
    public static final String TAG_TEST_EXAM = "test_exam";
    public static final String TANK_TYPE_CYL = " Bottom conical cylinder tank";
    public static final String TANK_TYPE_RECT = " Rectangular tank";
    public static final String PROBOTIC_STATE_SOLID = "Solid";
    public static final String PROBOTIC_STATE_LIQUID = " Liquid";
    public static final String FEED_STATE_STARTER = " Starter";
    public static final String FEED_STATE_GROWER = " Grower";
    public static final String CARBON_SOURCE_MOLASSES = " Molasses";
    public static final String CARBON_SOURCE_SUGAR = " Sugar";


    public static final String EXTRA_TAG = "tag";
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_PART_ID = "partid";
    public static final String EXTRA_PART_CHILD_ID = "partchildid";
    public static final String EXTRA_PARTICIPATION_CHILD_ID = "participitionchildid";
    public static final String EXTRA_PARTICIPATION_ID = "participitionid";
    public static final String EXTRA_EXAM_HISTORY_ID = "examhistoryid";
    public static final String EXTRA_BOARD_ID = "boardid";
    public static final String EXTRA_EXAM_TIME = "time";
    public static final String EXTRA_YEAR = "extrayear";
    public static final String EXTRA_SUBJECT_ID = "subid";
    public static final String EXTRA_CHAPTER_ID = "chapid";
    public static final String EXTRA_USER_ID = "user_id";

    public static final String EXTRA_QUESTION_SET = "question_set";
    public static final String EXTRA_EXAM_TITLE = "exam_title";
    public static final String EXTRA_OPEN_SIGN_UP_DIALOG = "opensignupdialog";
    public static final String EXTRA_FROM_PARTICIPTION_ACTIVITY = "fromparticipitionactivity";
    public static final String EXTRA_FROM_SGNIN_ACTIVITY = "fromsigninactivity";
    public static final String EXTRA_TOOL_BAR_TITLE = "toolbartitle";

    public static final boolean dummy_data_active = false;
    public static final String KEY_IS_FIRST_ITEM="is_first_item";
    public static final String MAX_ENTRIES_FOR_API_CALL = "200";
    public static final String START_TIME_STAMP=    "1111";
    public static final String DEFAULT_PRICE="0.0";
    public static Long GHUEST_USER_ID = -1L;
    public static int REQUEST_CODE_FOR_DETAILS_ACTIVITY = 65;
    public static int DEFAULT_CAT_LIST_POSITION=0;  // for those case when we have to pick a category from entry itself
                                                    // since entry return list of categoris so pick the first index position
                                                    // e.g when user click on notification we have to find entry cat from catlist that entry has
                                                    // but we dont no for which category entry is pointing specifically
                                                    // so using index 0

    public static int DEFAULT_MIDIA_LIST_POSITION=0; // since there is media list (small list, large list, video list)
                                                    // which one we will sow for thumbnil, there is no selection specifically
                                                    // so using the index 0
    public static int DEFAULT_CAT_SEARCH_ID = 0;
    public static int DEFAULT_CAT_HOME_ID = 99;
    public static int MAX_NUMBER_OF_SPECIAL_NEWS = 3;
    public static int DEFAULT_SELECTED_DRAWER_ITME = 1;
    public static String NOT_FOUND = "404";
    public static int MIN_SDK_VERSION_FOR_SEARCH_VEIW = Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static int swipeRefreshLayoutProgressOfset = 2; // must be even number// this value multiplied with action bar size
    public static final int AD_WAIT_IN_SEC = 10;

    public static void NoHistoryIntent(Context packageContext, Class<?> cls) {
        Intent intent = new Intent(packageContext, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        packageContext.startActivity(intent);

    }
    public static void NoHistoryIntentFinishCurrent(Activity requester, Class<?> cls) {
        Intent intent = new Intent(requester, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        requester.startActivity(intent);
        requester.finish();

    }
    public static void GotoMainActivityFinishCurrent(Activity requester){
        Intent i=new Intent(requester,MainActivity.class);
        requester.startActivity(i);
        requester.finish();
    }
    public static void GotoLoginActivity(Activity requester){
        Intent i=new Intent(requester,LoginActivity.class);
        requester.startActivity(i);
        requester.finish();
    }
    public static String getFormatedPhoneNumber(String phone){
        String tempPhone = phone;
        if(!phone.contains("+88")){
            tempPhone = "+88"+phone;
        }
        if(!tempPhone.contains("+880")){
            tempPhone = "+880"+phone;
        }
        phone = tempPhone;
        return  phone;
    }
    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static String getDateAndTime(String milliSeconds)
    {
        // Create a DateFormatter object for displaying date in specified format.
        if (milliSeconds != null){
            String dateFormat = "dd/MM/yyyy hh:mm";
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(milliSeconds));
            return formatter.format(calendar.getTime());
        }else{
            return "";
        }

    }
    public static String getDate(String milliSeconds)
    {
        // Create a DateFormatter object for displaying date in specified format.
        if (milliSeconds != null){
            String dateFormat = "dd/MM/yyyy";
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(milliSeconds));
            return formatter.format(calendar.getTime());
        }else{
            return "";
        }

    }

    public static String getUpto2DecimalPoint(double d){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(d);
    }

    public static int getRandomInt(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static boolean isPositiveInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
            return  false;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }


    public static boolean isFloat(String str){
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
            return  false;
        }
        try {
            Double.parseDouble(str);
            return true;
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }



    public static boolean isValidDate(String input) {
        if (input == null || !input.matches("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((18|19|20|21)\\\\d\\\\d)"))
            return false;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.parse(input);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

}
