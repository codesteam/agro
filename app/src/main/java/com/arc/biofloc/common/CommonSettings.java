package com.arc.biofloc.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.arc.biofloc.R;


public class CommonSettings {


    public static String PACKAGE_NAME;
    private static CommonSettings singleton = null;
    private String KEY_SD_CARD_STATE= ".KEY_SD_CARD_STATE";
    private String KEY_STORAGE_ROOT_PATH = ".KEY_STORAGE_ROOT_PATH";
    private String KEY_TEXT_SIZE = ".KEY_TEXT_SIZE";
    private String KEY_SD_CARD_FOLDER_NAME = "KEY_SD_CARD_FOLDER_NAME";
    private String KEY_PROFILE_IMAGE_PATH = "KEY_PROFILE_IMAGE_PATH";
    private String KEY_USER_NAME = "KEY_USER_NAME";
    private String KEY_PHONE = "KEY_PHONE";
    private String KEY_REMEMBER_ME = "KEY_REMEMBER_ME";
    private String KEY_REMEMBERED_PHONE = "KEY_REMEMBERED_PHONE";
    private String KEY_REMEMBERED_PASS = "KEY_REMEMBERED_PASS";
    private String KEY_API_TOKEN = "KEY_API_TOKEN";
    private String KEY_CURRENT_USER_ID = "KEY_CURRENT_USER_ID";
    private String KEY_PROJECT_ID = "KEY_PROJECT_ID";
    private String KEY_PROJECT_NAME = "KEY_PROJECT_NAME";
    private SharedPreferences _prefs;
    private SharedPreferences.Editor _editor;
    private static Context context;
    private String KEY_DETAILS_PAGE_TEXT_SIZE= ".KEY_DETAILS_PAGE_TEXT_SIZE";
    private String KEY_NOTIFICATION_STATE = ".KEY_NOTIFICATION_STATE";
    private String NOTIFICATION_SOUND_ENABLE = ".NOTIFICATION_SOUND_ENABLE";
    private String NOTIFICATION_VIVRATION_ENABLE = ".NOTIFICATION_VIVRATION_ENABLE";
    private CommonSettings(Context context) {
        PACKAGE_NAME = context.getPackageName();
        _prefs = PreferenceManager.getDefaultSharedPreferences(context);
        _editor = _prefs.edit();
    }

    public static CommonSettings getInstance(Context context) {
        if (singleton == null){
            CommonSettings.context = context;
            singleton = new CommonSettings(context);
        }
        return singleton;
    }

    public void setTextSize(int size){
        _editor.putInt(PACKAGE_NAME + KEY_TEXT_SIZE, size);
        _editor.commit();
    }

    public int getTextSize(){
        return _prefs.getInt(PACKAGE_NAME + KEY_TEXT_SIZE, 15);

    }
    @Override
    protected Object clone() throws CloneNotSupportedException {

        return new CloneNotSupportedException();
    }

    public void setString(String key, String value){
        _editor.putString(key, value);
        _editor.commit();
    }

    public String getString(String key){
        return _prefs.getString(key, null);
    }



    public void setSDCardState(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, value);
        _editor.commit();
    }

    public boolean getSDCardState()
    {
        return _prefs.getBoolean(PACKAGE_NAME + KEY_SD_CARD_STATE, true);

    }

    public String getStorageRootPath() {

        return _prefs.getString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, null);
    }

    public void setStorageRootPath(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_STORAGE_ROOT_PATH, value);
        _editor.commit();
    }

    private String KEY_GPAD_VERSION = ".KEY_GPAD_VERSION";

    public int getKEY_GPAD_VERSION() {

        return _prefs.getInt(PACKAGE_NAME + KEY_GPAD_VERSION, 0); // for first time 0 in loacal, in server >0
    }

    public void setKEY_GPAD_VERSION(int value)
    {
        _editor.putInt(PACKAGE_NAME + KEY_GPAD_VERSION, value);
        _editor.commit();
    }

    public String getStorageFolderName() {

        return _prefs.getString(PACKAGE_NAME + KEY_SD_CARD_FOLDER_NAME, null);
    }

    public void setStorageFolderName(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_SD_CARD_FOLDER_NAME, value);
        _editor.commit();
    }


    public void setNotificationState(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_NOTIFICATION_STATE, value);
        _editor.commit();
    }

    public boolean getNotificationState()
    {
        return _prefs.getBoolean(PACKAGE_NAME + KEY_NOTIFICATION_STATE, true);

    }
    public void setNotificationVibrationEnable(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFICATION_VIVRATION_ENABLE, value);
        _editor.commit();
    }

    public boolean getNotificationVibrationEnable(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFICATION_VIVRATION_ENABLE, true);
    }

    public void setNotificationSoundEnable(boolean value){
        _editor.putBoolean(PACKAGE_NAME + NOTIFICATION_SOUND_ENABLE, value);
        _editor.commit();
    }

    public boolean getNotificationSoundEnable(){
        return _prefs.getBoolean(PACKAGE_NAME + NOTIFICATION_SOUND_ENABLE, true);
    }
    private String BACK_TO_MAIN_ACTIVITY = ".BACK_TO_MAIN_ACTIVITY";
    public void setBACK_TO_MAIN_ACTIVITY(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + BACK_TO_MAIN_ACTIVITY, value);
        _editor.commit();
    }

    public boolean getBACK_TO_MAIN_ACTIVITY()
    {
        return _prefs.getBoolean(PACKAGE_NAME + BACK_TO_MAIN_ACTIVITY, false);

    }

    public String getProfileImagePath() {

        return _prefs.getString(PACKAGE_NAME + KEY_PROFILE_IMAGE_PATH, null);
    }

    public void setProfileImagePath(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_PROFILE_IMAGE_PATH, value);
        _editor.commit();
    }
    public String getRememberedPhone() {

        return _prefs.getString(PACKAGE_NAME + KEY_REMEMBERED_PHONE, null);
    }

    public void setRememberedPhone(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_REMEMBERED_PHONE, value);
        _editor.commit();
    }
    public String getRememberedPass() {

        return _prefs.getString(PACKAGE_NAME + KEY_REMEMBERED_PASS, null);
    }

    public void setRememberedPass(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_REMEMBERED_PASS, value);
        _editor.commit();
    }
    public String getPhone() {

        return _prefs.getString(PACKAGE_NAME + KEY_PHONE, null);
    }

    public void setPhone(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_PHONE, value);
        _editor.commit();
    }

    public void setRememberMe(boolean value)
    {
        _editor.putBoolean(PACKAGE_NAME + KEY_REMEMBER_ME, value);
        _editor.commit();
    }

    public boolean isRememberMe()
    {
        return _prefs.getBoolean(PACKAGE_NAME + KEY_REMEMBER_ME, false);

    }

    public String getApiToken() {

        return _prefs.getString(PACKAGE_NAME + KEY_API_TOKEN, "");
    }

    public void setApiToken(String value)
    {
        if(value!=null){
            _editor.putString(PACKAGE_NAME + KEY_API_TOKEN, "Token "+value);
            _editor.commit();
        }else{
            _editor.putString(PACKAGE_NAME + KEY_API_TOKEN, "null");
            _editor.commit();
        }
    }

    public Long getUserId() {

        return _prefs.getLong(PACKAGE_NAME + KEY_CURRENT_USER_ID, CommonConstants.GHUEST_USER_ID);
    }

    public void setUserId(Long value)
    {
        _editor.putLong(PACKAGE_NAME + KEY_CURRENT_USER_ID, value);
        _editor.commit();
    }

    public Long getProjectId() {

        return _prefs.getLong(PACKAGE_NAME + KEY_PROJECT_ID, -1L);
    }

    public void setProjectId(Long value)
    {
        _editor.putLong(PACKAGE_NAME + KEY_PROJECT_ID, value);
        _editor.commit();
    }

    public String getProjectName() {

        return _prefs.getString(PACKAGE_NAME + KEY_PROJECT_NAME, context.getString(R.string.app_name));
    }

    public void setProjectName(String value)
    {
        _editor.putString(PACKAGE_NAME + KEY_PROJECT_NAME, value);
        _editor.commit();
    }
}
