package com.arc.biofloc.common;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import java.lang.reflect.Method;


/**
 * Created by water on 5/14/15.
 */


public class CommonApplication extends Application {

    private static Context mContext;

    public static Context getAppContext() {
        return CommonApplication.mContext;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        CommonApplication.mContext = getApplicationContext();

       if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }


    }


}
