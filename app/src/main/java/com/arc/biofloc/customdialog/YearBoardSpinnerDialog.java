package com.arc.biofloc.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arc.biofloc.R;
import com.arc.biofloc.retrofit.responces.board.Result;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;

import angmarch.views.NiceSpinner;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class YearBoardSpinnerDialog {

    private String title,message,positiveBtnText,negativeBtnText,pBtnColor,nBtnColor;
    private Activity activity;
    private YearBoardDialogListener pListener,nListener;
    private boolean cancel;


    private YearBoardSpinnerDialog(Builder builder){
        this.title=builder.title;
        this.message=builder.message;
        this.activity=builder.activity;
        this.pListener=builder.pListener;
        this.nListener=builder.nListener;
        this.pBtnColor=builder.pBtnColor;
        this.nBtnColor=builder.nBtnColor;
        this.positiveBtnText=builder.positiveBtnText;
        this.negativeBtnText=builder.negativeBtnText;
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String title,message,positiveBtnText,negativeBtnText,pBtnColor,nBtnColor;
        private ArrayList<Result> boards = new ArrayList<>();
        private List<String> years = new ArrayList<>();
        private List<String> boardNames = new ArrayList<>();
        private List<com.arc.biofloc.retrofit.responces.year.Result> yearsObj = new ArrayList<>();
        private Activity activity;
        private YearBoardDialogListener pListener,nListener;
        private boolean cancel;
        Long selectedBoardId;
        String selectedYear;
        public Builder(Activity activity){
            this.activity=activity;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }

        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder setYears(List<com.arc.biofloc.retrofit.responces.year.Result> yearsObj) {
            this.yearsObj = yearsObj;
            if(yearsObj != null){
                for(com.arc.biofloc.retrofit.responces.year.Result year: yearsObj){
                    this.years.add(year.getYear());
                    Print.e(this, "year: "+year.getYear());
                }
            }

            return this;
        }

        public Builder setBoards(ArrayList<Result> boards) {
            this.boards = boards;
            if(boards != null){
                for(Result board: boards){
                    boardNames.add(board.getName());
                }
            }
            return this;
        }

        public Builder setPositiveBtnText(String positiveBtnText){
            this.positiveBtnText=positiveBtnText;
            return this;
        }

        public Builder setPositiveBtnBackground(String pBtnColor){
            this.pBtnColor=pBtnColor;
            return this;
        }


        public Builder setNegativeBtnText(String negativeBtnText){
            this.negativeBtnText=negativeBtnText;
            return this;
        }

        public Builder setNegativeBtnBackground(String nBtnColor){
            this.nBtnColor=nBtnColor;
            return this;
        }

        //set Positive listener
        public Builder OnPositiveClicked(YearBoardDialogListener pListener){
            this.pListener=pListener;
            return this;
        }

        //set Negative listener
        public Builder OnNegativeClicked(YearBoardDialogListener nListener){
            this.nListener=nListener;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public YearBoardSpinnerDialog build(){

            ImageView iconImg;
            Button nBtn,pBtn;
            NiceSpinner spinnerBoard, spinnerYear;
            LinearLayout llBoard, llYear;
            final Dialog dialog;
            dialog=new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_two_spinner);

            //getting resources

            nBtn=(Button)dialog.findViewById(R.id.negativeBtn);
            pBtn=(Button)dialog.findViewById(R.id.positiveBtn);
            spinnerBoard =(NiceSpinner) dialog.findViewById(R.id.spBoard);
            spinnerYear =(NiceSpinner)dialog.findViewById(R.id.spYear);
            llBoard = (LinearLayout) dialog.findViewById(R.id.llBoard);
            llYear = (LinearLayout) dialog.findViewById(R.id.llYear);
            if(years.size() > 0){
                selectedYear = years.get(0);
                spinnerYear.attachDataSource(years);
                spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedYear = years.get(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }else{
                llYear.setVisibility(View.GONE);
            }

            if(boardNames.size() > 0){
                selectedBoardId = boards.get(0).getId();
                spinnerBoard.attachDataSource(boardNames);
                spinnerBoard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedBoardId = boards.get(position).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }else{
                llBoard.setVisibility(View.GONE);
            }

            if(positiveBtnText!=null)
            pBtn.setText(positiveBtnText);
            if(negativeBtnText!=null)
            nBtn.setText(negativeBtnText);
            if(pBtnColor!=null)
            { GradientDrawable bgShape = (GradientDrawable)pBtn.getBackground();
              bgShape.setColor(Color.parseColor(pBtnColor));
            }
            if(nBtnColor!=null)
            { GradientDrawable bgShape = (GradientDrawable)nBtn.getBackground();
              bgShape.setColor(Color.parseColor(nBtnColor));
            }
            if(pListener!=null) {
                pBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pListener.OnClick(selectedBoardId, selectedYear);
                        dialog.dismiss();
                    }
                });
            }
            else{
                pBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }

                });
            }

            if(nListener!=null){
                nBtn.setVisibility(View.VISIBLE);
                nBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        nListener.OnClick(null, null);

                        dialog.dismiss();
                    }
                });
            }


            dialog.show();

            return new YearBoardSpinnerDialog(this);

        }
    }

}
