package com.arc.biofloc.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.arc.biofloc.R;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class GoBackDialog {


    private YearBoardDialogListener pListener,nListener;
    private boolean cancel;


    private GoBackDialog(Builder builder){
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String title,message;
        private Activity activity;
        Button btnBack;

        TextView tvMessage;

        private boolean cancel;
        Dialog dialog;
        public Builder(Activity activity){
            this.activity=activity;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }


        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public GoBackDialog build(final GoBackListner goBackListner){
            dialog=new Dialog(activity);
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setTitle("WARNING!!!");
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_back);
            dialog.show();
            btnBack = dialog.findViewById(R.id.btnBack);

            tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goBackListner.onBack();
                }
            });

            return new GoBackDialog(this);

        }

        public void dismiss(){
            if(dialog != null){
                dialog.dismiss();
            }
        }

    }

}
