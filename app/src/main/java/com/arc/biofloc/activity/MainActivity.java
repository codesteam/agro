package com.arc.biofloc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.agro.ProjectActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.fragments.home.FragmentHomeTanks;
import com.arc.biofloc.fragments.report.FragmentReportTanks;
import com.arc.biofloc.retrofit.responces.agro.project.Project;
import com.arc.biofloc.retrofit.sync.ISync;
import com.arc.biofloc.retrofit.sync.Sync;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public String selectedPartParentTag = null;

   /* public void setSelectedPartParentTag(String selectedPartParentTag) {
        if (this.selectedPartParentTag == null){
            this.selectedPartParentTag = selectedPartParentTag;
        }
    }*/
    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;
    boolean mToolBarNavigationListenerIsRegistered =false;
    NavigationView navigationView;
   /* public String getSelectedPartParentTag() {
        return selectedPartParentTag;
    }*/
   LoadingDialog.Builder builder;
    FragmentManager fragmentManager;
    public static int projectActivityRequestCode = 8888;
    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
       toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        ProjectDao projectDao = new ProjectDao(this);
        ArrayList<Project> projects = projectDao.getResult();
        if (projects!=null && projects.size() <= 0){
            Intent intent = new Intent(this, ProjectActivity.class);
            startActivityForResult(intent , projectActivityRequestCode);
        }else{
            gotoHomeFragment();
        }

        //enableViews(false);
    }
    public void enableBack(boolean enable) {

        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if(enable) {
            //You may not want to open the drawer on swipe from the left in this case
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            // Remove hamburger
            toggle.setDrawerIndicatorEnabled(false);
            // Show back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if(!mToolBarNavigationListenerIsRegistered) {
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                       onBackPressed();
                        //onClickListener.onClick(v);
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            //You must regain the power of swipe for the drawer.
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            // Remove back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            toggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            toggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Print.e(this, "onActivityResult");
        if (requestCode == projectActivityRequestCode && resultCode == RESULT_OK && data != null) {
            try {
                if (data.hasExtra("go_home")){
                    FragmentManager fm = this.getSupportFragmentManager();
                    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }

                    gotoHomeFragment();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }

    private void gotoHomeFragment(){
        //onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_home));
        //navigationView.setCheckedItem(R.id.nav_home);
        checkForProjectSettings();
        FragmentHomeTanks fragmentHomeTanks = new FragmentHomeTanks();
        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.EXTRA_TOOL_BAR_TITLE, getString(R.string.app_name));
        fragmentHomeTanks.setArguments(bundle);
        fragmentManager = MainActivity.this.getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container_body, fragmentHomeTanks, fragmentHomeTanks.getClass().getName())
                .commitAllowingStateLoss();
    }
    private void gotoReportFragment(){
        //onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_home));
        //navigationView.setCheckedItem(R.id.nav_home);

        checkForProjectSettings();
        FragmentReportTanks fragmentHomeTanks = new FragmentReportTanks();
        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.EXTRA_TOOL_BAR_TITLE, getString(R.string.app_name));
        fragmentHomeTanks.setArguments(bundle);
        fragmentManager = MainActivity.this.getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container_body, fragmentHomeTanks, fragmentHomeTanks.getClass().getName())
                .commitAllowingStateLoss();
    }

    private void checkForProjectSettings(){
        if (CommonSettings.getInstance(this).getProjectId() == -1L){
            ProjectDao projectDao = new ProjectDao(this);
            ArrayList<Project> projects = projectDao.getResult();
            if (projects!= null && projects.size()>0){
                Project project = projects.get(0);
                CommonSettings.getInstance(this).setProjectId(project.getId());
                CommonSettings.getInstance(this).setProjectName(project.getName());
            }
        }
    }
    @Override
    public void onBackPressed() {
        if(!mToolBarNavigationListenerIsRegistered){
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }else{
            int count = fragmentManager.getBackStackEntryCount();
            if (count == 0) {
                super.onBackPressed();
            } else {
                fragmentManager.popBackStack();
                count = fragmentManager.getBackStackEntryCount();
                if(count == 1){
                    enableBack(false);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar exam_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_sync) {
            builder = new LoadingDialog.Builder(MainActivity.this).isCancellable(false);
            builder.setMessage("Syncing... please wait").build();
            Sync sync = new Sync(this, new ISync() {
                @Override
                public void completed() {
                    ToastMsg.Toast(MainActivity.this, "Sync completed.");
                    builder.dismiss();
                }
            });
            sync.start();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view exam_item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            gotoHomeFragment();
        }else if (id == R.id.nav_project) {
            Intent intent = new Intent(this, ProjectActivity.class);
            startActivityForResult(intent , projectActivityRequestCode);

        }else if (id == R.id.nav_report) {
            gotoReportFragment();
        }else if (id == R.id.nav_chart) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
