/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.activity.agro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.dao.UserDao;
import com.arc.biofloc.utility.ToastMsg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginActivity extends AppCompatActivity{

    

    AppCompatEditText etPhone, etPass;
    AppCompatButton btnSignin, btnSignup;
    String phone, pass;
    AppCompatCheckBox cbAgree, cbRememberMe;
    TextView tvForgotInfo;
    LoadingDialog.Builder builder;
    UserDao userDao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        etPhone = (AppCompatEditText)findViewById(R.id.etPhone);
        tvForgotInfo = (TextView)findViewById(R.id.tvForgotInfo);
        etPass = (AppCompatEditText)findViewById(R.id.etPassword);
        btnSignin = (AppCompatButton) findViewById(R.id.btnSignin);
        btnSignup = (AppCompatButton) findViewById(R.id.btnSignup);
        //btnGuest = (AppCompatButton) findViewById(R.id.btnGuest);
        userDao = new UserDao(this);
        tvForgotInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg.Toast(LoginActivity.this, "Feature locked");
            }
        });
        SpannableString content = new SpannableString(getString(R.string.forgot_login_info));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvForgotInfo.setText(content);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phone = etPhone.getText().toString().trim();
                CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(phone);
                CommonSettings.getInstance(LoginActivity.this).setRememberMe(cbRememberMe.isChecked());
                if (cbRememberMe.isChecked()){
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPass(pass);
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(phone);
                }else{
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPass(null);
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(null);
                }
              /*  phone = etPhone.getText().toString().trim();
                pass = etPass.getText().toString();

                if (phone == null || phone.length() <= 0 || (!phone.equals("Admin") && !phone.equals("admin"))){
                   // Toast.makeText(LoginActivity.this, "Please enter valid user name", Toast.LENGTH_SHORT).show();
                    ToastMsg.Toast(LoginActivity.this, "Please enter valid user name");
                    return;
                }
                if (pass == null || pass.length() <=0 || !pass.equals("11235")){
                    //Toast.makeText(LoginActivity.this, "Please enter correct password", Toast.LENGTH_SHORT).show();
                    ToastMsg.Toast(LoginActivity.this, "Please enter correct password");
                    return;
                }
                CommonSettings.getInstance(LoginActivity.this).setRememberMe(cbRememberMe.isChecked());

                if (cbRememberMe.isChecked()){
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPass(pass);
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(phone);
                }else{
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPass(null);
                    CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(null);
                }*/
                CommonConstants.GotoMainActivityFinishCurrent(LoginActivity.this);

           /*
                //phone = CommonConstants.getFormatedPhoneNumber(phone);
                builder = new LoadingDialog.Builder(LoginActivity.this).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();

                PostLogin.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        builder.dismiss();
                        if (sender!= null){
                            LoginResponse response = (LoginResponse)sender;
                            if(response.getNonFieldErrors()!= null && response.getNonFieldErrors().size() > 0){
                                String msgT="";
                                for (String s:response.getNonFieldErrors()
                                        ) {
                                    msgT+=s+'\n';
                                }
                                ToastMsg.Toast(LoginActivity.this, msgT);
                            }else if(response.getPassword() != null && response.getPassword().size() > 0){
                                String msgT="";
                                for (String s:response.getPassword()
                                        ) {
                                    msgT+=s+'\n';
                                }
                                ToastMsg.Toast(LoginActivity.this, msgT);
                            }
                            else{
                                ToastMsg.Toast(LoginActivity.this, getString(R.string.please_tray_again_later));
                            }
                        }else{
                            ToastMsg.Toast(LoginActivity.this, getString(R.string.please_tray_again_later));

                        }

                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        builder.dismiss();
                        LoginResponse response = (LoginResponse) sender;
                        if(response.getKey() == null){
                            ToastMsg.Toast(LoginActivity.this, getString(R.string.please_tray_again_later));
                        }else{
                            ToastMsg.Toast(LoginActivity.this, getString(R.string.successfully_loggedin));


                            CommonSettings.getInstance(LoginActivity.this).setApiToken(response.getKey());
                            CommonSettings.getInstance(LoginActivity.this).setUserId(response.getUserId());
                            CommonSettings.getInstance(LoginActivity.this).setRememberMe(cbRememberMe.isChecked());
                            if (cbRememberMe.isChecked()){
                                CommonSettings.getInstance(LoginActivity.this).setRememberedPass(pass);
                                CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(phone);
                            }else{
                                CommonSettings.getInstance(LoginActivity.this).setRememberedPass(null);
                                CommonSettings.getInstance(LoginActivity.this).setRememberedPhone(null);
                            }
                            if (userDao.getUser(CommonSettings.getInstance(LoginActivity.this).getUserId()) == null){
                                User user = new User();
                                user.setId(response.getUserId());
                                user.setFullName(response.getFirstName()+"");
                                user.setPhone(response.getPhoneNumber()+"");
                                user.setEmail(response.getEmail()+"");
                                userDao.insert(user);
                            }
                            CommonConstants.GotoMainActivityFinishCurrent(LoginActivity.this);
                        }
                    }
                }, null,getString(R.string.base_url), phone, pass);*/
            }
        });

      btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent i=new Intent(LoginActivity.this, SignUpActivity.class);
                i.putExtra(CommonConstants.EXTRA_FROM_SGNIN_ACTIVITY, true);
                startActivity(i);
                finish();*/
                ToastMsg.Toast(LoginActivity.this, "Feature locked");

            }
        });
    /*    btnGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonSettings.getInstance(LoginActivity.this).setApiToken(null);
                CommonSettings.getInstance(LoginActivity.this).setUserId(CommonConstants.GHUEST_USER_ID);
                CommonConstants.GotoMainActivityFinishCurrent(LoginActivity.this);
            }
        });*/

        boolean remembered = CommonSettings.getInstance(this).isRememberMe();
        if (remembered){
            Long userId = CommonSettings.getInstance(this).getUserId();
            if (userId != null){
                etPhone.setText(CommonSettings.getInstance(this).getRememberedPhone()+"");
                etPass.setText(CommonSettings.getInstance(this).getRememberedPass());
            }

        }

        cbRememberMe = (AppCompatCheckBox) findViewById(R.id.cbRememberMe); // get value of remember me after all api call;
        cbRememberMe.setChecked(remembered);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public boolean isAlphaNumeric(String s){
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

}