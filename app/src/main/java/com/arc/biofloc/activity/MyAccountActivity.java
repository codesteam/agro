/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.agro.LoginActivity;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.db.DBHelper;
import com.arc.biofloc.db.dao.UserDao;
import com.arc.biofloc.retrofit.responces.User;
import com.arc.biofloc.utility.CircleImageView;
import com.arc.biofloc.utility.ToastMsg;

import java.io.File;


public class MyAccountActivity extends AppCompatActivity{


    FloatingActionButton fabCamera;
    CircleImageView mImageView;
    //TextView tvEditProfileImage;
    LinearLayout llProfile;
    User user;
    TextView tvBalance, tvExamAppeard, tvRewardPoints, tvFullName, tvEmail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        DBHelper dbHelper = DBHelper.getHelper(this);
        UserDao userDao = new UserDao(this);
        user= userDao.getUser(CommonSettings.getInstance(this).getUserId());
        tvBalance = (TextView)findViewById(R.id.tvBalance);
        tvExamAppeard = (TextView)findViewById(R.id.tvExamAppeared);
        tvRewardPoints = (TextView)findViewById(R.id.tvRewardPoints);
        tvFullName = (TextView)findViewById(R.id.tvFullName);
        tvEmail = (TextView)findViewById(R.id.tvEmail);
        fabCamera = (FloatingActionButton)findViewById(R.id.fabCamera);
        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, ImagePickerActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.btnLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccountActivity.this,LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });
        findViewById(R.id.btnEditProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastMsg.Toast(MyAccountActivity.this, "Feature locked");
            }
        });
        mImageView = (CircleImageView) findViewById(R.id.ivProfile);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.profile));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        llProfile = (LinearLayout) findViewById(R.id.llProfile);
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, ImagePickerActivity.class);
                startActivity(intent);
            }
        });

        if (user.getEmail()==null || user.getEmail().length() <=0){
            tvEmail.setVisibility(View.GONE);
        }else{
            tvEmail.setText(user.getEmail());
        }
        tvExamAppeard.setText(user.getExamAppeared()+"");
        tvBalance.setText(user.getBalance()+"");
        tvFullName.setText(user.getFullName());
        tvRewardPoints.setText(user.getRewardPoint()+"");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String profileImagePath = CommonSettings.getInstance(this).getProfileImagePath();
        if (profileImagePath != null){
            File imgFile = new  File(profileImagePath);
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                mImageView.setImageBitmap(myBitmap);

            }
        }
    }
}