/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.dao.UserDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.PostRegistration;
import com.arc.biofloc.retrofit.responces.RegistrationResponse;
import com.arc.biofloc.retrofit.responces.User;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SignUpActivity extends AppCompatActivity{

    

    AppCompatEditText etUserName, etPhone, etPass, etRepeatPass, etEmail;
    AppCompatButton btnSubmit;
    String username, phone, pass, repass, email;
    AppCompatCheckBox cbAgree, cbRememberMe;
    LoadingDialog.Builder builder;
    UserDao userDao;
    boolean fromParticiptionActivity = false;
    boolean fromSigninActivity = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        if (getIntent().hasExtra(CommonConstants.EXTRA_FROM_PARTICIPTION_ACTIVITY)){
            fromParticiptionActivity = getIntent().getBooleanExtra(CommonConstants.EXTRA_FROM_PARTICIPTION_ACTIVITY, false);
        }
        if (getIntent().hasExtra(CommonConstants.EXTRA_FROM_SGNIN_ACTIVITY)){
            fromSigninActivity = getIntent().getBooleanExtra(CommonConstants.EXTRA_FROM_SGNIN_ACTIVITY, false);
        }
        etUserName = (AppCompatEditText)findViewById(R.id.et_username);
        etPhone = (AppCompatEditText)findViewById(R.id.et_phone);
        etEmail = (AppCompatEditText)findViewById(R.id.et_email);
        etPass = (AppCompatEditText)findViewById(R.id.et_password);
        etRepeatPass = (AppCompatEditText)findViewById(R.id.et_repeatpassword);
        btnSubmit = (AppCompatButton) findViewById(R.id.btn_submit);
        userDao = new UserDao(this);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUserName.getText().toString().trim();
                phone = etPhone.getText().toString().trim();
                pass = etPass.getText().toString();
                repass = etRepeatPass.getText().toString();
                email = etEmail.getText().toString().trim();
                if (username == null || username.toString().length() <= 0){
                    Toast.makeText(SignUpActivity.this, getString(R.string.enter_valid_user_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone == null || phone.length() <= 0){
                    Toast.makeText(SignUpActivity.this, getString(R.string.enter_valid_phone_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pass == null || pass.length() < 8){
                    Toast.makeText(SignUpActivity.this, getString(R.string.enter_valid_pass), Toast.LENGTH_SHORT).show();
                    return;
                }

                /*if (isAlphaNumeric(pass)){
                    Toast.makeText(SignUpActivity.this, "Please enter a valid password", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                if(!pass.equals(repass)){
                    Toast.makeText(SignUpActivity.this, getString(R.string.confirm_pass_dose_not_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!cbAgree.isChecked()){
                    Toast.makeText(SignUpActivity.this, getString(R.string.check_licence_agreement), Toast.LENGTH_SHORT).show();
                    return;
                }
                phone = CommonConstants.getFormatedPhoneNumber(phone);
                builder = new LoadingDialog.Builder(SignUpActivity.this).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();

                PostRegistration.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        builder.dismiss();
                        RegistrationResponse response = (RegistrationResponse)sender;
                        if(response.getPhoneNumber()!= null && response.getPhoneNumber().size() > 0){
                            String msgT="";
                            for (String s:response.getPhoneNumber()
                                    ) {
                                msgT+=s+'\n';
                            }
                            ToastMsg.Toast(SignUpActivity.this, msgT);
                        }else if(response.getFirstName() != null && response.getFirstName().size() > 0){
                            String msgT="";
                            for (String s:response.getFirstName()
                                    ) {
                                msgT+=s+'\n';
                            }
                            ToastMsg.Toast(SignUpActivity.this, msgT);
                        }
                        else if(response.getPassword() != null && response.getPassword().size() > 0){
                            String msgT="";
                            for (String s:response.getPassword()
                                 ) {
                                msgT+=s+'\n';
                            }
                            ToastMsg.Toast(SignUpActivity.this, msgT);
                        }
                        else{
                            ToastMsg.Toast(SignUpActivity.this, getString(R.string.please_tray_again_later));
                        }
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        builder.dismiss();
                        Print.e(this, "registration success");
                        RegistrationResponse response = (RegistrationResponse) sender;
                        if(response.getKey() == null){
                            ToastMsg.Toast(SignUpActivity.this, getString(R.string.please_tray_again_later));
                        }else{
                            ToastMsg.Toast(SignUpActivity.this, getString(R.string.successfully_registrered));
                            User user = new User();
                            user.setId(response.getUserId());
                            user.setFullName(username);
                            user.setPhone(phone);
                            user.setEmail(email);
                            user.setExamAppeared(0L);
                            user.setRewardPoint(0L);
                            user.setBalance("0.0");
                            userDao.insert(user);
                            CommonSettings.getInstance(SignUpActivity.this).setApiToken(response.getKey());
                            CommonSettings.getInstance(SignUpActivity.this).setUserId(response.getUserId());
                            Print.e(this, "responced user id:"+response.getUserId());
                            CommonSettings.getInstance(SignUpActivity.this).setRememberMe(cbRememberMe.isChecked());
                            if (cbRememberMe.isChecked()){
                                CommonSettings.getInstance(SignUpActivity.this).setRememberedPass(pass);
                                CommonSettings.getInstance(SignUpActivity.this).setRememberedPhone(phone);
                            }else{
                                CommonSettings.getInstance(SignUpActivity.this).setRememberedPass(null);
                                CommonSettings.getInstance(SignUpActivity.this).setRememberedPhone(null);
                            }
                            if (fromParticiptionActivity){ // just finish current activity back to previous activity in stack
                                finish();
                            }else if (fromSigninActivity){
                                CommonConstants.NoHistoryIntentFinishCurrent(SignUpActivity.this, MainActivity.class);
                            }
                            else{ // user click button on r
                                // egistration after finish return back to MyAccount
                                CommonConstants.NoHistoryIntentFinishCurrent(SignUpActivity.this, MyAccountActivity.class);
                            }
                        }
                    }
                }, null,null, null, null,  getString(R.string.base_url), phone, username, email, pass, pass);
                //TODO check if this user is available, if ok then save it to shared pref goto to MyAccount
               /* UserDao userDao = new UserDao(SignUpActivity.this);

                userDao.insert(user);
                Intent intent = new Intent(SignUpActivity.this, MyAccountActivity.class);
                startActivity(intent);*/
            }
        });
        btnSubmit = (AppCompatButton)findViewById(R.id.btn_submit);
        cbAgree = (AppCompatCheckBox) findViewById(R.id.cbLicenseAgree);
        cbRememberMe = (AppCompatCheckBox) findViewById(R.id.cbRememberMe); // get value of remember me after all api call;
        String checkBoxText = "I agree to all the <a href='"+getString(R.string.terms_condition_url)+"' > Terms and Conditions</a>";

        cbAgree.setText(Html.fromHtml(checkBoxText));
        cbAgree.setMovementMethod(LinkMovementMethod.getInstance());
       // btnSubmit.setEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public boolean isAlphaNumeric(String s){
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
}