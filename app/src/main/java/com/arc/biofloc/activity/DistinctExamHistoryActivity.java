package com.arc.biofloc.activity;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.DownloadCompleteListner;
import com.arc.biofloc.customdialog.LoadingWithProgressBarDialog;
import com.arc.biofloc.customdialog.ResolutionDialog;
import com.arc.biofloc.customdialog.ResolutionListnear;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.DBHelper;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.ApiCallManager;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.DistinctExamHistory;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.ExamQuestion;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.History;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.Option;
import com.arc.biofloc.retrofit.responces.distinctexamhistory.Results;
import com.arc.biofloc.utility.ConnectionDetector;
import com.arc.biofloc.utility.DividerItemDecoration;
import com.arc.biofloc.utility.GlideImageDownloader;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;

public class DistinctExamHistoryActivity extends AppCompatActivity implements BackgroundRenderer{
    boolean lock = true;
    private boolean innerAppStatusLock = false;
    private FragmentManager fm = null;
    private NoNet mNoNet;


    RequestOptions requestOptions = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.warning_gray)
            .error(R.drawable.warning_gray)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH);


    LoadingWithProgressBarDialog.Builder builder;
    private RecyclerView recyclerView;
    private Animator spruceAnimator;
    Bundle bundle;

    CollapsingToolbarLayout collapsingToolbarLayout;
    /*SpotsDialog.Builder spotsDialogBuilder;
    android.app.AlertDialog alertDialog;*/
    Long historyId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_history_details);
        recyclerView = (RecyclerView) findViewById(R.id.collapsing_toolbar_recycler_view);
        recyclerView.setHasFixedSize(true);

        // Use Toolbar to replace default activity action bar.
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar_layout);
       // collapsingToolbarLayout.setTitle("Time: ");
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            if (extras.containsKey(CommonConstants.EXTRA_EXAM_HISTORY_ID)){
                historyId = extras.getLong(CommonConstants.EXTRA_EXAM_HISTORY_ID);
            }
        }

       // spotsDialogBuilder = new SpotsDialog.Builder();
        if (!ConnectionDetector.isNetworkPresent(this)){
            fm = getSupportFragmentManager();
            mNoNet = new NoNet(this);
            mNoNet.initNoNet(R.layout.no_net_default_with_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            }, this, fm, false);
            mNoNet.setConnectionCallback(new ConnectionCallback() {
                @Override
                public void Networkupdate(boolean isConnectionActive) {
                    Print.e(this, "isConnectionActive: "+isConnectionActive);
                    if(isConnectionActive){
                        //restarting application to open this activity automatically from wifi screen
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
            });
            mNoNet.RegisterNoNet();
            mNoNet.showDefaultDialog();

        }else{
            builder = new LoadingWithProgressBarDialog.Builder(DistinctExamHistoryActivity.this, new ResolutionListnear() {
                @Override
                public void onClickRight() {
                    // retry again
                    startOperation();
                }

                @Override
                public void onClickLeft() {
                    //back to previous activity
                    finish();
                }
            }).isCancellable(false);
            builder.build();
            //getWindow().setWindowAnimations(3);
            DBHelper.getHelper(this);
            startOperation();
        }
    }


    @Override
    protected void onResume() {

        super.onResume();
        Print.e(this, "onResume");
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ResolutionDialog.Builder resolutionBuilder;
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void startOperation(){
        ApiCallManager apiCallManager = ApiCallManager.getInstance(DistinctExamHistoryActivity.this, getString(R.string.base_url), CommonSettings.getInstance(this).getApiToken());
        String timestamp = new AppStatusDao(DistinctExamHistoryActivity.this).getStatusString(AppStatusDao.LAST_RESPONDED_TIME_PARTS_WITH_DETIALS);
        Print.e(this, "history id:: "+historyId);
        apiCallManager.callDistinctExamHistoryAPI(String.valueOf(historyId), String.valueOf(
                CommonSettings.getInstance(this).getUserId()), new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                Print.e(this, "failed");
                innerAppStatusLock = false;
                lock = false;
                builder.modeRetry();

            }

            @Override
            public void onSuccess(String msg, Object sender) {
                Print.e(this, "success");
                innerAppStatusLock = false;
                lock = false;
                DistinctExamHistory history = (DistinctExamHistory) sender;

                ArrayList<String> imgs = new ArrayList<>();
                if (history.getResults()!=null){
                    for(ExamQuestion r: history.getResults().getExamQuestions()){
                        if (r.getImage()!=null){
                            imgs.add(r.getImage());
                        }
                    }
                }
                if (imgs.size()>0){
                    downloadImages(imgs, history.getResults());
                }else{
                    loadUI(history.getResults());
                }
            }
        }, DistinctExamHistoryActivity.this);

    }

    ArrayList<GlideImageDownloader> glideImageDownloaders = new ArrayList<>();
    private void downloadImages(final ArrayList<String> imgs, final Results results){
        builder.modePB();
        glideImageDownloaders.clear();

        for(String url: imgs){
            GlideImageDownloader glideImageDownloader = new GlideImageDownloader(url, this);
            glideImageDownloaders.add(glideImageDownloader);
        }

        for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
            glideImageDownloader.setDownloadCompleteListner(new DownloadCompleteListner() {
                @Override
                public void complete() {
                    boolean downloadcom = true;
                    builder.getPb().setProgress(builder.getPb().getProgress()+100/imgs.size());
                    for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
                        if(!glideImageDownloader.isDownloadComplete()){
                            downloadcom = false;
                            break;
                        }
                    }
                    if(downloadcom){
                        loadUI(results);
                    }

                }
            });

            glideImageDownloader.setImageViewPb(builder.getImageViewPb());
            glideImageDownloader.download();
        }

    }

    void loadUI(Results results){
        builder.dismiss();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                initSpruce();
            }
        };

        ArrayList<ExamQuestion> examQuestions = (ArrayList<ExamQuestion>) results.getExamQuestions();
        for (History history: results.getHistory()){
            for (int i=0; i <examQuestions.size(); i++){
                if (history.getQuestion().equals(examQuestions.get(i).getId())){
                    examQuestions.get(i).setSelectedOption(history.getOption());
                }
            }
        }

//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        recyclerView.setAdapter(new RecyclerAdapter(examQuestions));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

    }

    private void initSpruce() {
      /*  spruceAnimator = new Spruce.SpruceBuilder(recyclerView)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(recyclerView, 800),
                        ObjectAnimator.ofFloat(recyclerView, "translationX", -recyclerView.getWidth(), 0f).setDuration(800))
                .start();*/
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<ExamQuestion> examQuestions;

        class ViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout placeholderView;
            ImageView ivQueston;
            TextView tvQueston;
            RadioGroup radioGroup;
            RadioButton[] rbs= new RadioButton[4];
            ViewHolder(View itemView) {
                super(itemView);
                placeholderView = (RelativeLayout) itemView.findViewById(R.id.placeholder_view);
                ivQueston = (ImageView) itemView.findViewById(R.id.ivQuestion);
                tvQueston =(TextView) itemView.findViewById(R.id.tvQuestion);
                radioGroup = (RadioGroup) itemView.findViewById(R.id.rg1);
                rbs[0] = (RadioButton) itemView.findViewById(R.id.rb1);
                rbs[1] = (RadioButton) itemView.findViewById(R.id.rb2);
                rbs[2] = (RadioButton) itemView.findViewById(R.id.rb3);
                rbs[3] = (RadioButton) itemView.findViewById(R.id.rb4);
            }
        }

        RecyclerAdapter(List<ExamQuestion> placeholderList) {
            this.examQuestions = placeholderList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.placeholder_distinct_exam_history, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ExamQuestion examQuestion = examQuestions.get(position);
            List<Option> options = examQuestion.getOptions();
            if (options.size()<3){
                holder.rbs[2].setVisibility(View.GONE);
                holder.rbs[3].setVisibility(View.GONE);
            }
            if (options.size()<4){
                holder.rbs[3].setVisibility(View.GONE);
            }
            if (examQuestion.getImage() != null){
                Glide.with(DistinctExamHistoryActivity.this)
                        .applyDefaultRequestOptions(requestOptions)
                        .load(examQuestion.getImage())
                        .into( holder.ivQueston);
            }else{
                holder.ivQueston.setVisibility(View.GONE);
            }
            holder.tvQueston.setText(examQuestion.getTitle());
            long selAns = examQuestions.get(position).getSelectedOption();
            long rightAns = examQuestions.get(position).getAnswer();

            for (int i = 0; i < options.size(); i++){
                holder.rbs[i].setText(options.get(i).getTitle());
                if (options.get(i).getId().equals(selAns)){
                    holder.rbs[i].setSelected(true);
                    holder.rbs[i].toggle();
                    Print.e(this, "Selected Option:"+selAns);
                }
                if(options.get(i).getId().equals(rightAns)){
                    holder.rbs[i].setTextColor(getResources().getColor(R.color.green));
                }
            }
           /* holder.mCircleTextView.setText("ব");
           *//* holder.mCircleTextView.setTextColor(getResources().getColor(R.color.red));
            holder.mCircleTextView.setSolidColor(getResources().getColor(R.color.green));*//*
            holder.mCircleTextView.setStrokeWidth(0);*/
        }

        @Override
        public int getItemCount() {
            return examQuestions.size();
        }

    }

    @Override
    public void updateForeground() {

    }
}
