package com.arc.biofloc.activity.agro;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.agrodao.ApiStatusDao;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.get.RequestProjects;
import com.arc.biofloc.retrofit.handler.agro.get.RequestTanks;
import com.arc.biofloc.retrofit.responces.agro.project.Project;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.retrofit.sync.ISync;
import com.arc.biofloc.retrofit.sync.Sync;
import com.arc.biofloc.retrofit.sync.SyncTankComponents;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.ArrayList;

public class SplashScreenActivity extends AppCompatActivity implements BackgroundRenderer{

    ImageView logoView;

    /*SpotsDialog.Builder spotsDialogBuilder;
    android.app.AlertDialog alertDialog;*/
    ProjectDao projectDao;
    TankDao tankDao;
    ApiStatusDao apiStatusDao;
    ArrayList<Project> projects;
    ArrayList<Tank> tanks;
    int projectIndex =0;
    int tankIndex = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        logoView = (ImageView) findViewById(R.id.logo);
        projectDao = new ProjectDao(this);
        apiStatusDao = new ApiStatusDao(this);
        tankDao = new TankDao(this);

        Sync sync = new Sync(this, new ISync() {
            @Override
            public void completed() {
                ToastMsg.Toast(SplashScreenActivity.this, "Sync completed.");
                CommonConstants.GotoLoginActivity(SplashScreenActivity.this);

            }
        });
        sync.start();
        /*RequestProjects.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) { // no new projects recived
                projects = projectDao.getResults(); // fatchecd all projects from db
                if (projects!=null && projects.size() >0){
                    projectIndex = 0;
                    syncTanks(projects.get(projectIndex).getId());
                }else{
                    //No projects created till this day, one user must enter app and create a new project
                    CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) { // recived all new projects
                //ArrayList<Project> projects = (ArrayList<Project>) ((GetProjects) sender).getProject();
               projects = projectDao.getResults(); // fatchecd all projects from db
               if (projects!=null && projects.size() >0){
                   projectIndex = 0;
                   syncTanks(projects.get(projectIndex).getId());
               }else{
                   //No projects created till this day, one user must enter app and create a new project
                   CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
               }

            }
        },null,(long)apiStatusDao.getRepondedTime(RequestProjects.tag),null,null,this.getString(R.string.base_url));
  */  }


    private void syncTanks(final  long projectId){
        RequestTanks.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) { // no tanks recived under this project
                projectIndex++;
                if (projectIndex < projects.size()){
                    syncTanks(projects.get(projectIndex).getId());
                }else{
                    CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) { // recived all tanks under this project
                tanks = tankDao.getResults(projectId);
                Print.e(this, "Tanks size:"+tanks.size());
                tankIndex = 0;
                if (tanks!= null && tanks.size() > 0){
                    syncTanksComponents(projectId, tanks.get(0).getId());
                }else{ // since there is no tanks in this project move to new projects tanks set.
                    projectIndex++;
                    if (projectIndex < projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                        CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                    }
                }
                /*if (projectIndex < projects.size()){
                    syncTanks(projects.get(projectIndex).getId());
                }*/

            }
        },null,(long)apiStatusDao.getRepondedTime(RequestProjects.tag),projectId,null,this.getString(R.string.base_url));

    }

    private void syncTanksComponents(final long projectId, long tankId){
        final SyncTankComponents syncTankComponents = new SyncTankComponents(this, projectId, tankId, new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                tankIndex++;
                if(tankIndex<tanks.size()){
                    syncTanksComponents(projectId, tanks.get(tankIndex).getId());

                }else{
                    projectIndex++;
                    if (projectIndex<projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                        ToastMsg.Toast(SplashScreenActivity.this, "Sync completed.");
                        CommonConstants.GotoLoginActivity(SplashScreenActivity.this);

                    }
                }
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                tankIndex++;
                if(tankIndex<tanks.size()){
                   syncTanksComponents(projectId, tanks.get(tankIndex).getId());

                }else{
                    projectIndex++;
                    if (projectIndex<projects.size()){
                        syncTanks(projects.get(projectIndex).getId());
                    }else{
                        ToastMsg.Toast(SplashScreenActivity.this, "Sync completed.");
                        CommonConstants.GotoLoginActivity(SplashScreenActivity.this);

                    }
                }
            }
        });

    }
    @Override
    protected void onResume() {

        super.onResume();
        Print.e(this, "onResume");


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public void updateForeground() {

    }
}
