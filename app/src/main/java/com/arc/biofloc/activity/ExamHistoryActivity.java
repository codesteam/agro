package com.arc.biofloc.activity;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.DownloadCompleteListner;
import com.arc.biofloc.customdialog.LoadingWithProgressBarDialog;
import com.arc.biofloc.customdialog.ResolutionDialog;
import com.arc.biofloc.customdialog.ResolutionListnear;
import com.arc.biofloc.db.BackgroundRenderer;
import com.arc.biofloc.db.DBHelper;
import com.arc.biofloc.db.dao.AppStatusDao;
import com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.ApiCallManager;
import com.arc.biofloc.retrofit.responces.parts.Result;
import com.arc.biofloc.utility.ConnectionDetector;
import com.arc.biofloc.utility.DividerItemDecoration;
import com.arc.biofloc.utility.GlideImageDownloader;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;

public class ExamHistoryActivity extends AppCompatActivity implements BackgroundRenderer{
    boolean lock = true;
    private boolean innerAppStatusLock = false;
    private FragmentManager fm = null;
    private NoNet mNoNet;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.logo_first_frame)
            .error(R.drawable.logo_first_frame)
            .priority(Priority.HIGH);
    String board;
    String year;
    String partId;
    String chapterId;
    String examTitle = "";
    LoadingWithProgressBarDialog.Builder builder;
    private RecyclerView recyclerView;
    private Animator spruceAnimator;
    Bundle bundle;
    Button btnSubmit;
    CountDownTimer countDownTimer;
    int examTimeInMilis;
    TextView tvAnswerd;
    TextView tvExamTitle;
    TextView tvTotalMark;
    TextView tvTotalTime;
    TextView tvTotalQuestion;
    CollapsingToolbarLayout collapsingToolbarLayout;
    /*SpotsDialog.Builder spotsDialogBuilder;
    android.app.AlertDialog alertDialog;*/
    Long participitionId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        recyclerView = (RecyclerView) findViewById(R.id.collapsing_toolbar_recycler_view);
        recyclerView.setHasFixedSize(true);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        btnSubmit.setVisibility(View.GONE);
        tvAnswerd = (TextView) findViewById(R.id.tvAnswered);
        tvExamTitle = (TextView) findViewById(R.id.tvExamTitle );
        tvTotalMark = (TextView) findViewById(R.id.tvTotalMark);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        tvTotalQuestion = (TextView) findViewById(R.id.tvTotalQuestion);

        // Use Toolbar to replace default activity action bar.

        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle("Time: ");

        // Set collapsing tool bar image.
        /*ImageView collapsingToolbarImageView = (ImageView)findViewById(R.id.collapsing_toolbar_image_view);
        collapsingToolbarImageView.setImageResource(R.drawable.logo_png);*/
        //bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet));
        //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            if (extras.containsKey(CommonConstants.EXTRA_PARTICIPATION_ID)){
                participitionId = extras.getLong(CommonConstants.EXTRA_PARTICIPATION_ID);
            }
        }

        // load all participition  then check for historyId
        if (participitionId != null){
            // participition details fragment
        }
       // spotsDialogBuilder = new SpotsDialog.Builder();
        if (!ConnectionDetector.isNetworkPresent(this)){
            fm = getSupportFragmentManager();
            mNoNet = new NoNet(this);
            mNoNet.initNoNet(R.layout.no_net_default_with_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            }, this, fm, false);
            mNoNet.setConnectionCallback(new ConnectionCallback() {
                @Override
                public void Networkupdate(boolean isConnectionActive) {
                    Print.e(this, "isConnectionActive: "+isConnectionActive);
                    if(isConnectionActive){
                        //restarting application to open this activity automatically from wifi screen
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
            });
            mNoNet.RegisterNoNet();
            mNoNet.showDefaultDialog();

        }else{
            builder = new LoadingWithProgressBarDialog.Builder(ExamHistoryActivity.this, new ResolutionListnear() {
                @Override
                public void onClickRight() {
                    // retry again
                    startOperation();
                }

                @Override
                public void onClickLeft() {
                    //back to previous activity
                    finish();
                }
            }).isCancellable(false);
            builder.build();
            //getWindow().setWindowAnimations(3);
            DBHelper.getHelper(this);
            startOperation();
        }
       /* btnStateInactive();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOperation();
            }
        });*/
    }


    @Override
    protected void onResume() {

        super.onResume();
        Print.e(this, "onResume");
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ResolutionDialog.Builder resolutionBuilder;
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        resolutionBuilder = new ResolutionDialog.Builder(this);
        resolutionBuilder.isCancellable(false)
                .setRightBtnText("Back")
                .setLeftBtnText("Quit")
                .setMessage(getString(R.string.quiting_exam))
                .build(new ResolutionListnear() {
            @Override
            public void onClickRight() {
                resolutionBuilder.dismiss();

            }

            @Override
            public void onClickLeft() {
                finish();
            }
        });
    }

    private void gotoMainActivity(){

       /* if (alertDialog != null && alertDialog.isShowing()){
            alertDialog.dismiss();
        }*/
       Intent i=new Intent(getBaseContext(),MainActivity.class);
        startActivity(i);
        finish();
    }

    private void startOperation(){
        ApiCallManager apiCallManager = ApiCallManager.getInstance(ExamHistoryActivity.this, getString(R.string.base_url), CommonSettings.getInstance(this).getApiToken());
        String timestamp = new AppStatusDao(ExamHistoryActivity.this).getStatusString(AppStatusDao.LAST_RESPONDED_TIME_PARTS_WITH_DETIALS);

        apiCallManager.callForQuestionAPI(timestamp,board,year, partId, new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                Print.e(this, "failed");
                innerAppStatusLock = false;
                lock = false;
                //builder.modeRetry();
                downloadImages();
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                Print.e(this, "success");
                innerAppStatusLock = false;
                lock = false;
                downloadImages();
            }
        }, ExamHistoryActivity.this);

    }

    ArrayList<GlideImageDownloader> glideImageDownloaders = new ArrayList<>();
    private void downloadImages(){
        builder.modePB();
        glideImageDownloaders.clear();
        // get urls from db
        final ArrayList<String> urls = new ArrayList<>();
        urls.add("https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg");
        urls.add("https://static.toiimg.com/img/62355428/Master.jpg");
        urls.add("https://tukiem.com/wp-content/uploads/2017/09/434976-happy-valentines-day-timeline-cover.jpg");
        urls.add("https://ichef.bbci.co.uk/news/976/cpsprodpb/1572B/production/_88615878_976x1024n0037151.jpg");
        urls.add("https://ired.ac.uk/wp-content/uploads/2018/07/Thermal-Imaging-Training-Course-3.jpg");
        urls.add("https://www.oxforduniversityimages.com/images/rotate/Image_Spring_17_4.gif");

        if (urls != null && urls.size() >0){
            for(String url: urls){
                GlideImageDownloader glideImageDownloader = new GlideImageDownloader(url, this);
                glideImageDownloaders.add(glideImageDownloader);
            }

            for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
                glideImageDownloader.setDownloadCompleteListner(new DownloadCompleteListner() {
                    @Override
                    public void complete() {
                        boolean downloadcom = true;
                        builder.getPb().setProgress(builder.getPb().getProgress()+100/urls.size());
                        for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
                            if(!glideImageDownloader.isDownloadComplete()){
                                downloadcom = false;
                                break;
                            }
                        }
                        if(downloadcom){
                            loadUI();
                        }

                    }
                });

                glideImageDownloader.setImageViewPb(builder.getImageViewPb());
                glideImageDownloader.download();
            }


        }else{
            loadUI();
        }


    }

    void loadUI(){
        builder.dismiss();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                initSpruce();
            }
        };

        // Mock data objects
        List<Result> placeHolderList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            placeHolderList.add(new Result());
        }
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        recyclerView.setAdapter(new RecyclerAdapter(placeHolderList));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (btnSubmit.getVisibility()==View.GONE){
                    btnSubmit.setVisibility(View.VISIBLE);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        examTimeInMilis = 45 * 60*1000;

        countDownTimer = new CountDownTimer(examTimeInMilis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int sec = (int) millisUntilFinished/1000;
                int min = sec/60;
                int secLeft = sec%60;
                collapsingToolbarLayout.setTitle("Time: "+min+":"+secLeft);
            }

            @Override
            public void onFinish() {

            }
        };

        countDownTimer.start();

        if(examTitle != null){
            tvExamTitle.setText(examTitle);
        }
        tvTotalQuestion.setText("Total Question: 50, ");
        tvTotalTime.setText("Max Time: 60 min");
        tvTotalMark.setText("Total Mark: 50");
        tvAnswerd.setText("Answerd: 0/50");
        final int tootalQuestion = 50;
        final int answered = 30;
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answered< tootalQuestion){
                    resolutionBuilder = new ResolutionDialog.Builder(ExamHistoryActivity.this);
                    resolutionBuilder.isCancellable(false)
                            .setTitle("WARNING")
                            .setRightBtnText("Cancel")
                            .setLeftBtnText("Submit")
                            .setMessage("You have answerd "+answered+" of "+tootalQuestion+". Do you want to submit?")
                            .build(new ResolutionListnear() {
                                @Override
                                public void onClickRight() {
                                    resolutionBuilder.dismiss();

                                }

                                @Override
                                public void onClickLeft() {
                                   //submit call
                                    resolutionBuilder.dismiss();
                                }
                            });
                }else{
                    //submit success
                }
            }
        });
    }

    private void initSpruce() {
      /*  spruceAnimator = new Spruce.SpruceBuilder(recyclerView)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(recyclerView, 800),
                        ObjectAnimator.ofFloat(recyclerView, "translationX", -recyclerView.getWidth(), 0f).setDuration(800))
                .start();*/
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<Result> placeholderList;

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            RelativeLayout placeholderView;
            ViewHolder(View itemView) {
                super(itemView);
                placeholderView = (RelativeLayout) itemView.findViewById(R.id.placeholder_view);
                placeholderView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent(QuestionSetActivity.this,
                        QuestionSetActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtras(bundle);
                startActivity(intent);*/

                //initSpruce();
            }
        }

        RecyclerAdapter(List<Result> placeholderList) {
            this.placeholderList = placeholderList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.placeholder_question, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
           /* holder.mCircleTextView.setText("ব");
           *//* holder.mCircleTextView.setTextColor(getResources().getColor(R.color.red));
            holder.mCircleTextView.setSolidColor(getResources().getColor(R.color.green));*//*
            holder.mCircleTextView.setStrokeWidth(0);*/
        }

        @Override
        public int getItemCount() {
            return placeholderList.size();
        }

    }

    @Override
    public void updateForeground() {

    }
}
