/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.activity.agro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.DBHelper;
import com.arc.biofloc.db.agrodao.ProjectDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostProject;
import com.arc.biofloc.retrofit.responces.agro.project.Project;

import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.ArrayList;


public class ProjectActivity extends AppCompatActivity {




    //TextView tvEditProfileImage;


    RelativeLayout rlNewProject;
    LinearLayout llNewProjectForm, llSelectAproject;
    AppCompatEditText etNewProject, etProjectAddress;
    AppCompatButton btnSave, btnCancel, btnDone;
    AppCompatImageView ivAdd;
    AppCompatSpinner spinner;
    boolean state = false;
    LoadingDialog.Builder builder;
    ArrayList<Project> resultArrayList;
    long defId;
    String pName;
    CustomAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        DBHelper dbHelper = DBHelper.getHelper(this);
        rlNewProject = (RelativeLayout) findViewById(R.id.rlNewProject);
        etNewProject = (AppCompatEditText) findViewById(R.id.etProjectName);
        etProjectAddress = (AppCompatEditText) findViewById(R.id.etProjectAddress);
        llNewProjectForm = (LinearLayout) findViewById(R.id.llNewProjectForm);
        llSelectAproject = (LinearLayout) findViewById(R.id.llSelectAproject);
        btnSave = (AppCompatButton)findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton)findViewById(R.id.btnCancel);
        btnDone = (AppCompatButton)findViewById(R.id.btnDone);
        ivAdd = (AppCompatImageView) findViewById(R.id.ivAdd);
        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        llNewProjectForm.setVisibility(View.GONE);
        ivAdd.setBackgroundResource(R.drawable.add);
        rlNewProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!state){
                    CommonConstants.expand(llNewProjectForm);
                    ivAdd.setBackgroundResource(R.drawable.substract);
                    state = true;
                }else{
                    state = false;
                    CommonConstants.collapse(llNewProjectForm);
                    ivAdd.setBackgroundResource(R.drawable.add);

                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                state = false;
                CommonConstants.collapse(llNewProjectForm);
                ivAdd.setBackgroundResource(R.drawable.add);
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String projectName = etNewProject.getText().toString().trim();
                final String projectAddress = etProjectAddress.getText().toString().trim();
                if (projectName == null || projectName.length() <=0){
                    ToastMsg.Toast(ProjectActivity.this, "Please write a project name first!");
                    return;
                }
                builder = new LoadingDialog.Builder(ProjectActivity.this).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostProject.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        Print.e(this, "Api failed");
                        if (CommonConstants.OFFLINE_MODE){
                            ProjectDao projectDao = new ProjectDao(ProjectActivity.this);
                            Project result = new Project();
                            Long id = (long)CommonConstants.getRandomInt(0,10000);

                            result.setId(id);
                            result.setName(projectName);
                            result.setAddress(projectAddress);
                            result.setCreatedAt(System.currentTimeMillis());
                            result.setModifiedAt(System.currentTimeMillis());
                            projectDao.insert(result);
                            CommonSettings.getInstance(ProjectActivity.this).setProjectId(id);
                            CommonSettings.getInstance(ProjectActivity.this).setProjectName(result.getName());
                            finish();
                        }
                        ToastMsg.Toast(ProjectActivity.this,"Upload failed please try again");
                        builder.dismiss();


                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Project projectResponce = (Project) sender;
                        ProjectDao projectDao = new ProjectDao(ProjectActivity.this);
                        Project result = new Project();
                        result.setAddress(projectResponce.getAddress());
                        result.setCreatedAt(projectResponce.getCreatedAt());
                        result.setModifiedAt(projectResponce.getModifiedAt());
                        result.setName(projectResponce.getName());
                        result.setId(projectResponce.getId());
                        projectDao.insert(result);
                        CommonSettings.getInstance(ProjectActivity.this).setProjectId(projectResponce.getId());
                        CommonSettings.getInstance(ProjectActivity.this).setProjectName(projectResponce.getName());

                        builder.dismiss();
                        Intent output = new Intent();
                        output.putExtra("go_home", "true");
                        setResult(RESULT_OK, output);
                        //finish();
                        ProjectActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setSpinnerAdapter();
                                adapter.notifyDataSetChanged();
                            }
                        });
                        ToastMsg.Toast(ProjectActivity.this,"Project created successfully.");
                    }
                }, null, null, null, null, getString(R.string.base_url), projectName,projectAddress);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonSettings.getInstance(ProjectActivity.this).setProjectId(defId);
                CommonSettings.getInstance(ProjectActivity.this).setProjectName(pName);
                Intent output = new Intent();
                output.putExtra("go_home", "true");
                setResult(RESULT_OK, output);
                finish();
            }
        });
       setSpinnerAdapter();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                Project result = adapter.getItem(position);
                defId = result.getId();
                pName = result.getName();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

    }
     private void setSpinnerAdapter(){
         resultArrayList = new ArrayList<>();
         resultArrayList =  new ProjectDao(this).getResult();
         if (resultArrayList!=null && resultArrayList.size() > 0){
             defId = resultArrayList.get(0).getId();
             llSelectAproject.setVisibility(View.VISIBLE);
         }else{
             llSelectAproject.setVisibility(View.INVISIBLE);
         }
         adapter = new CustomAdapter(ProjectActivity.this,
                 R.layout.spinner_list_item,resultArrayList);
         spinner.setAdapter(adapter);
         if (resultArrayList.size() > 0){
             if (CommonSettings.getInstance(this).getProjectId()!=-1L){
                 for (int i = 0; i < resultArrayList.size(); i++){
                     if (resultArrayList.get(i).getId().equals(CommonSettings.getInstance(this).getProjectId())){
                         spinner.setSelection(i);
                         break;
                     }
                 }
             }

         }



     }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }





    public class CustomAdapter  extends ArrayAdapter<Project>{

        // Your sent context
        private Context context;
        // Your custom values for the spinner (User)
        private ArrayList<Project> values;

       public CustomAdapter(Context context, int textViewResourceId,  ArrayList<Project> values) {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount(){
            return values.size();
        }

        @Override
        public Project getItem(int position){
            return values.get(position);
        }

        @Override
        public long getItemId(int position){
            return position;
        }


        // And the "magic" goes here
        // This is for the "passive" state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            TextView label = (TextView) super.getView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            // Then you can get the current item using the values array (Users array) and the current position
            // You can NOW reference each method you has created in your bean object (User class)
            label.setText(values.get(position).getName());

            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        }

        // And here is when the "chooser" is popped up
        // Normally is the same view, but you can customize it if you want
        @Override
        public View getDropDownView(int position, View convertView,
                ViewGroup parent) {
            TextView label = (TextView) super.getDropDownView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            label.setText(values.get(position).getName());

            return label;
        }
    }
}