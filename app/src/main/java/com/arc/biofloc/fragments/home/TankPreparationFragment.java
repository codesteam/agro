/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.db.agrodao.PreparationDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostTankPreparation;
import com.arc.biofloc.retrofit.handler.agro.PutPreparation;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Preparation;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.Arrays;
import java.util.List;


public class TankPreparationFragment extends Fragment {

    public static TankPreparationFragment newInstance() {
        return new TankPreparationFragment();
    }


    TextView tvPrepStartDate, tvTemperature, tvPHLevel, tvTDSLevel,
            tvDOLevel, tvArsenicLevel, tvClorinLevel, tvIronLevel, tvAreationUnit,
            tvAreationPoint, tvAreationWaitDuration, tvGroundWaterTDS, tvTargetTDS,
            tvSaltAmount, tvTDSWaitDuration, tvMolassesAmount, tvMolassesWaitDuration,
            tvProbioticsName, tvProbioticsDosage, tvProbioticState,tvProbioticsStrenth, tvAreationStartTime;
    AppCompatEditText etTemperature, etPh, etTDS, etDo, etArsenic, etClorinLevel, etIron, etAreationUnit, etAreationPoint,
            etAreationWaitDuration, etGroundWaterTDS, etTargetTDS, etTDSWaitDuration, etMolassesAmount,
            etMolassesWaitDuration, etProbioticsName, etProbioticsDosage, etAreationStartDate, etAreationStartTime;
    RadioGroup rgProbiotic;
    RadioButton rbLiquid, rbSolid;
    RelativeLayout rlNewProbiotic;

    Button btnSave, btnCancel;
    LoadingDialog.Builder builder;
    Long tankId;
    public static String probioticStrain = null;
    public static String probioticStrength = null;
    Preparation preparation;
    //boolean prepComplete = true;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_preparation, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        etTemperature = (AppCompatEditText)mView.findViewById(R.id.etTemperature);
        etPh = (AppCompatEditText)mView.findViewById(R.id.etPh);
        etTDS = (AppCompatEditText)mView.findViewById(R.id.etTDS);
        etDo = (AppCompatEditText)mView.findViewById(R.id.etDo);
        etArsenic = (AppCompatEditText)mView.findViewById(R.id.etArsenic);
        etClorinLevel = (AppCompatEditText)mView.findViewById(R.id.etClorinLevel);
        etIron = (AppCompatEditText)mView.findViewById(R.id.etIron);
        etAreationUnit = (AppCompatEditText)mView.findViewById(R.id.etAreationUnit);
        etAreationPoint = (AppCompatEditText)mView.findViewById(R.id.etAreationPoint);
        etAreationWaitDuration = (AppCompatEditText)mView.findViewById(R.id.etAreationWaitDuration);
        etGroundWaterTDS = (AppCompatEditText)mView.findViewById(R.id.etGroundWaterTDS);
        etTargetTDS = (AppCompatEditText)mView.findViewById(R.id.etTargetTDS);
        etTDSWaitDuration = (AppCompatEditText)mView.findViewById(R.id.etTDSWaitDuration);
        etMolassesAmount = (AppCompatEditText)mView.findViewById(R.id.etMolassesAmount);
        etMolassesWaitDuration = (AppCompatEditText)mView.findViewById(R.id.etMolassesWaitDuration);
        etProbioticsName = (AppCompatEditText)mView.findViewById(R.id.etProbioticsName);
        etProbioticsDosage = (AppCompatEditText)mView.findViewById(R.id.etProbioticsDosage);
        etAreationStartDate = (AppCompatEditText)mView.findViewById(R.id.etAreationStartDate);
        etAreationStartTime = (AppCompatEditText)mView.findViewById(R.id.etAreationStartTime);


        tvPrepStartDate = (TextView) mView.findViewById(R.id.tvPrepStartDate);
        tvTemperature = (TextView) mView.findViewById(R.id.tvTemperature);
        tvPHLevel = (TextView) mView.findViewById(R.id.tvPHLevel);
        tvTDSLevel = (TextView) mView.findViewById(R.id.tvTDSLevel);
        tvDOLevel = (TextView) mView.findViewById(R.id.tvDOLevel);
        tvArsenicLevel = (TextView) mView.findViewById(R.id.tvArsenicLevel);
        tvClorinLevel = (TextView) mView.findViewById(R.id.tvClorinLevel);
        tvIronLevel = (TextView) mView.findViewById(R.id.tvIronLevel);

        tvAreationUnit = (TextView) mView.findViewById(R.id.tvAreationUnit);
        tvAreationPoint = (TextView) mView.findViewById(R.id.tvAreationPoint);
        tvAreationWaitDuration = (TextView) mView.findViewById(R.id.tvAreationWaitDuration);
        tvGroundWaterTDS = (TextView) mView.findViewById(R.id.tvGroundWaterTDS);
        tvTargetTDS = (TextView) mView.findViewById(R.id.tvTargetTDS);
        tvSaltAmount = (TextView) mView.findViewById(R.id.tvSaltAmount);
        tvTDSWaitDuration = (TextView) mView.findViewById(R.id.tvTDSWaitDuration);
        tvMolassesAmount = (TextView) mView.findViewById(R.id.tvMolassesAmount);
        tvMolassesWaitDuration = (TextView) mView.findViewById(R.id.tvMolassesWaitDuration);
        tvProbioticsName = (TextView) mView.findViewById(R.id.tvProbioticsName);
        tvProbioticsDosage = (TextView) mView.findViewById(R.id.tvProbioticsDosage);
        tvProbioticState = (TextView)mView.findViewById(R.id.tvProbioticState);
        tvProbioticsStrenth = (TextView)mView.findViewById(R.id.tvProbioticsStrenth);
        tvAreationStartTime = (TextView)mView.findViewById(R.id.tvAreationStartTime);

        rgProbiotic = (RadioGroup) mView.findViewById(R.id.rgProbiotic);
        rbLiquid = (RadioButton) mView.findViewById(R.id.rbLiquid);
        rbSolid = (RadioButton) mView.findViewById(R.id.rbSolid);

        btnSave = (AppCompatButton)mView.findViewById(R.id.btnSave);

        btnCancel = (AppCompatButton)mView.findViewById(R.id.btnCancel);
        rlNewProbiotic = (RelativeLayout) mView.findViewById(R.id.rlNewProbiotic);
        final PreparationDao preparationDao = new PreparationDao(this.getActivity());
        preparation = preparationDao.getPreparation(tankId);
        if(preparation == null){

            btnSave.setText("Save");
            Print.e(this, "No preparation found");
            noPreparationFound();
            //prepComplete = false;

        }else{
            btnSave.setText("Update");
            Print.e(this, "preparation id: "+preparation.getId() +" : tank id: "+preparation.getTankId());
            alterVisibility(tvPrepStartDate, null,CommonConstants.getDateAndTime(preparation.getPrepStartTs()), "Prep start date:", ".");
            alterVisibility(tvTemperature, etTemperature, preparation.getTemperature(),"Temperature:", "\u2103");
            alterVisibility(tvPHLevel, etPh, preparation.getPhLevel(),"PH Level:",".");
            alterVisibility(tvTDSLevel, etTDS, preparation.getTdsLevel(), "TDS Level:", ".");
            alterVisibility(tvDOLevel, etDo, preparation.getDoLevel(), "DO Level:", ".");
            alterVisibility(tvArsenicLevel, etArsenic, preparation.getArsenicLevel(), "Arsenic Level:", ".");
            alterVisibility(tvClorinLevel, etClorinLevel, preparation.getClorinLevel(), "Clorin Level:", ".");
            alterVisibility(tvIronLevel, etIron, preparation.getIronLevel(), "Iron Level:", ".");
            //alterVisibility();//for areation start time
            alterVisibility(tvAreationUnit, etAreationUnit, preparation.getAreationUnit(), "Areation Unit:", "Liters/Minute.");
            alterVisibility(tvAreationPoint, etAreationPoint, preparation.getAreationPoint(), "Areation point:","");
            alterVisibility(tvAreationWaitDuration, etAreationWaitDuration, preparation.getAreationWaitDuration(), "Wait duration:","hour.");
            alterVisibility(tvAreationStartTime, etAreationStartDate, preparation.getAreationTs(), "Areation start date:",".");
            alterVisibility(tvAreationStartTime, etAreationStartTime, preparation.getAreationTs(), "Areation start date:",".");

            alterVisibility(tvGroundWaterTDS, etGroundWaterTDS, preparation.getGroundWaterTds(), "Ground Water TDS",".");
            alterVisibility(tvTargetTDS, etTargetTDS, preparation.getTargetTds(), "Target TDS:", ".");
            alterVisibility(tvSaltAmount, null, preparation.getSaltAmount(), "Total raw salt amount:", "gram.");
            alterVisibility(tvTDSWaitDuration, etTDSWaitDuration, preparation.getTdsWaitDuration(), "Wait duration:", "hr");
            alterVisibility(tvMolassesAmount, etMolassesAmount, preparation.getMolassesAmount(), "Molasses amount:", "gram.");
            alterVisibility(tvMolassesWaitDuration, etMolassesWaitDuration, preparation.getMolassesWaitDuration(), "Wait duration:", "hour.");
            alterVisibility(tvProbioticsName, etProbioticsName, preparation.getProbioticsName(), "Probiotics Name:", ".");
            alterVisibility(tvProbioticState, rgProbiotic, preparation.getProbioticState(), "Probiotics State:", ".");
            if (preparation.getProbioticState()!=null){
                if (preparation.getProbioticState().equals(CommonConstants.PROBOTIC_STATE_LIQUID)){
                    alterVisibility(tvProbioticsDosage, etProbioticsDosage, preparation.getProbioticsDosage(), "Probiotics dosage:", "ml.");

                }else{
                    alterVisibility(tvProbioticsDosage, etProbioticsDosage, preparation.getProbioticsDosage(), "Probiotics dosage:", "gram.");

                }
            }else{
                alterVisibility(tvProbioticsDosage, etProbioticsDosage, preparation.getProbioticsDosage(), "Probiotics dosage:", ".");
            }

            if (preparation.getProbioticStrain()!= null){
                List<String> strain = Arrays.asList(preparation.getProbioticStrain().split("\\s*,\\s*"));
                List<String> strength = Arrays.asList(preparation.getProbioticStrength().split("\\s*,\\s*"));
                String text = "";
                int p = strain.size();
                int q = strength.size();
                for (int i = 0; i < p; i++){
                    text +=strain.get(i);
                    if (i < q){
                        text += " - "+strength.get(i)+"\n";
                    }
                }

                if (text.length() > 0){
                    alterVisibility(tvProbioticsStrenth, rlNewProbiotic, text, "Probiotic strain and strength:\n","");
                }
            }else{
                //prepComplete = false;
            }

        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preparation != null){
                    preparation.setTankId(tankId);
                    if (preparation.getPrepStartTs() == null){
                        String prepStartTs = System.currentTimeMillis()+"";
                        preparation.setPrepStartTs(prepStartTs);
                    }
                    if (preparation.getTemperature() == null){
                        String temparature = etTemperature.getText().toString().trim();
                        if(temparature != null && temparature.length() > 0){
                            preparation.setTemperature(temparature);
                        }
                    }
                    if (preparation.getPhLevel() == null){
                        String ph = etPh.getText().toString().trim();
                        if(ph != null && ph.length() > 0){
                            preparation.setPhLevel(ph);
                        }
                    }
                    if (preparation.getTdsLevel() == null){
                        String tds = etTDS.getText().toString().trim();
                        if(tds != null && tds.length() > 0){
                            preparation.setTdsLevel(tds);
                        }
                    }
                    if (preparation.getDoLevel() == null){
                        String dO = etDo.getText().toString().trim();
                        if(dO != null && dO.length() > 0){
                            preparation.setDoLevel(dO);
                        }
                    }
                    if (preparation.getArsenicLevel() == null){
                        String arsenic = etArsenic.getText().toString().trim();
                        if(arsenic != null && arsenic.length() > 0){
                            preparation.setArsenicLevel(arsenic);
                        }
                    }
                    if (preparation.getClorinLevel() == null){
                        String clorin = etClorinLevel.getText().toString().trim();
                        if(clorin != null && clorin.length() > 0){
                            preparation.setClorinLevel(clorin);
                        }
                    }
                    if (preparation.getIronLevel() == null){
                        String iron = etIron.getText().toString().trim();
                        if(iron != null && iron.length() > 0){
                            preparation.setIronLevel(iron);
                        }
                    }
                    if (preparation.getAreationUnit() == null){
                        String areationUnit = etAreationUnit.getText().toString().trim();
                        if(areationUnit != null && areationUnit.length() > 0){
                            preparation.setAreationUnit(areationUnit);
                        }
                    }

                    //////







                    if (preparation.getAreationPoint() == null){
                        String areationPoint = etAreationPoint.getText().toString().trim();
                        if(areationPoint != null && areationPoint.length() > 0){
                            preparation.setAreationPoint(areationPoint);
                        }
                    }


                    if (preparation.getAreationTs() == null){
                        String areationStartDate = etAreationStartDate.getText().toString().trim();
                        String areationStartTime = etAreationStartTime.getText().toString().trim();
                        if(areationStartDate != null && areationStartDate.length() > 0){
                            if (areationStartTime != null){
                                areationStartDate +=" "+areationStartTime;
                            }
                            preparation.setAreationTs(areationStartDate);
                        }
                    }
                    if (preparation.getAreationWaitDuration() == null){
                        String areationWaitDuration = etAreationWaitDuration.getText().toString().trim();
                        if(areationWaitDuration != null && areationWaitDuration.length() > 0){
                            preparation.setAreationWaitDuration(areationWaitDuration);
                        }
                    }


                    String groundWaterTds = etGroundWaterTDS.getText().toString().trim();
                    if (preparation.getGroundWaterTds() == null){
                        if(groundWaterTds != null && groundWaterTds.length() > 0){
                            preparation.setGroundWaterTds(groundWaterTds);
                        }
                    }

                    String targetTds = etTargetTDS.getText().toString().trim();
                    if (preparation.getTargetTds() == null){

                        if(targetTds != null && targetTds.length() > 0){
                            preparation.setTargetTds(targetTds);
                        }
                    }


                    if (preparation.getTdsWaitDuration() == null){
                        String tdsWaitDuration = etTDSWaitDuration.getText().toString().trim();
                        if(tdsWaitDuration != null && tdsWaitDuration.length() > 0){
                            preparation.setTdsWaitDuration(tdsWaitDuration);
                        }
                    }
                    if (preparation.getMolassesAmount() == null){
                        String molassesAmount = etMolassesAmount.getText().toString().trim();
                        if(molassesAmount != null && molassesAmount.length() > 0){
                            preparation.setMolassesAmount(molassesAmount);
                        }
                    }
                    if (preparation.getMolassesWaitDuration() == null){
                        String molassesWaitDuration = etMolassesWaitDuration.getText().toString().trim();
                        if(molassesWaitDuration != null && molassesWaitDuration.length() > 0){
                            preparation.setMolassesWaitDuration(molassesWaitDuration);
                        }
                    }
                    if (preparation.getProbioticsName() == null){
                        String probioticsName = etProbioticsName.getText().toString().trim();
                        if(probioticsName != null && probioticsName.length() > 0){
                            preparation.setProbioticsName(probioticsName);
                        }
                    }




                    ////
                    if (preparation.getProbioticsDosage() == null){
                        String probioticDosage = etProbioticsDosage.getText().toString().trim();
                        if(probioticDosage != null && probioticDosage.length() > 0){
                            preparation.setProbioticsDosage(probioticDosage);
                        }
                    }
                    if (preparation.getSaltAmount() == null){
                        if (groundWaterTds != null && groundWaterTds.length()>0 && targetTds != null && targetTds.length()>0){
                            try{
                                TankDao tankDao = new TankDao(TankPreparationFragment.this.getActivity());
                                Tank result = tankDao.getResult(tankId);
                                float tdsReq = (Integer.valueOf(targetTds)-Integer.valueOf(groundWaterTds));
                                float tankV = Float.valueOf(result.getMaxCapacity());
                                String saltAmount = "10000";

                                if(saltAmount != null && saltAmount.length() > 0){
                                    preparation.setSaltAmount(saltAmount);
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }
                    if (preparation.getProbioticStrain() == null){
                        if(probioticStrain != null && probioticStrain.length() > 0){
                            preparation.setProbioticStrain(probioticStrain);
                            preparation.setProbioticStrength(probioticStrength);
                        }
                    }
                    if (preparation.getProbioticState() == null){
                        String state = null;
                        if(rbLiquid.isChecked()){
                            state = CommonConstants.PROBOTIC_STATE_LIQUID;
                        }else if (rbSolid.isChecked()){
                            state = CommonConstants.PROBOTIC_STATE_SOLID;
                        }
                        if(state != null && state.length() > 0){
                            preparation.setProbioticState(state);
                        }
                    }

                    builder = new LoadingDialog.Builder(TankPreparationFragment.this.getActivity()).isCancellable(false);
                    builder.setMessage(getString(R.string.please_wait)).build();

                    PutPreparation.getInstance(preparation).callAPI(new APIClientResponse() {
                        @Override
                        public void onFailure(String msg, Object sender) {
                            if(CommonConstants.OFFLINE_MODE){
                                if (preparation.getId() == null){
                                    Print.e(this, "setting prep id:"+preparation.getTankId() +" tank id:"+preparation.getTankId());
                                    preparation.setId(preparation.getTankId());
                                }
                                preparationDao.insert(preparation);
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                if (Build.VERSION.SDK_INT >= 26) {
                                    ft.setReorderingAllowed(false);
                                }
                                ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();
                            }
                            builder.dismiss();
                            ToastMsg.Toast(TankPreparationFragment.this.getActivity(), "Upload failed please try again later");

                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            if (Build.VERSION.SDK_INT >= 26) {
                                ft.setReorderingAllowed(false);
                            }
                            ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();

                        }

                        @Override
                        public void onSuccess(String msg, Object sender) {
                            Preparation prep = (Preparation) sender;
                            preparationDao.insert(prep);
                            builder.dismiss();

                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            if (Build.VERSION.SDK_INT >= 26) {
                                ft.setReorderingAllowed(false);
                            }
                            ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();
                        }
                    },null,null, null, null, getString(R.string.base_url), preparation.getId()+"");


                }else{
                    preparation = new Preparation();
                    preparation.setTankId(tankId);
                    String prepStartTs = System.currentTimeMillis()+"";
                    preparation.setPrepStartTs(prepStartTs);

                    String temparature = etTemperature.getText().toString().trim();
                    if(CommonConstants.isFloat(temparature)){
                        preparation.setTemperature(temparature);
                    }
                    String ph = etPh.getText().toString().trim();
                    if(CommonConstants.isFloat(ph)){
                        preparation.setPhLevel(ph);
                    }
                    String tds = etTDS.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(tds)){
                        preparation.setTdsLevel(tds);
                    }
                    String dO = etDo.getText().toString().trim();
                    if(CommonConstants.isFloat(dO)){
                        preparation.setDoLevel(dO);
                    }
                    String arsenic = etArsenic.getText().toString().trim();
                    if(CommonConstants.isFloat(arsenic)){
                        preparation.setArsenicLevel(arsenic);
                    }
                    String clorin = etClorinLevel.getText().toString().trim();
                    if(CommonConstants.isFloat(clorin)){
                        preparation.setClorinLevel(clorin);
                    }
                    String iron = etIron.getText().toString().trim();
                    if(CommonConstants.isFloat(iron)){
                        preparation.setIronLevel(iron);
                    }

                    String areationUnit = etAreationUnit.getText().toString().trim();
                    if(CommonConstants.isFloat(areationUnit)){
                        preparation.setAreationUnit(areationUnit);
                    }
                    String areationPoint = etAreationPoint.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(areationPoint)){
                        preparation.setAreationPoint(areationPoint);
                    }

                    String areationStartDate = etAreationStartDate.getText().toString().trim();
                    String areationStartTime = etAreationStartTime.getText().toString().trim();
                    if(areationStartDate != null && areationStartDate.length() > 0){
                        if (areationStartTime != null){
                            areationStartDate +=" "+areationStartTime;
                        }
                        preparation.setAreationTs(areationStartDate);
                    }

                    String areationWaitDuration = etAreationWaitDuration.getText().toString().trim();
                    if(CommonConstants.isFloat(areationWaitDuration)){
                        preparation.setAreationWaitDuration(areationWaitDuration);
                    }

                    String groundWaterTds = etGroundWaterTDS.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(groundWaterTds)){
                        preparation.setGroundWaterTds(groundWaterTds);
                    }

                    String targetTds = etTargetTDS.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(targetTds)){
                        preparation.setTargetTds(targetTds);
                    }

                    String tdsWaitDuration = etTDSWaitDuration.getText().toString().trim();
                    if(CommonConstants.isFloat(tdsWaitDuration)){
                        preparation.setTdsWaitDuration(tdsWaitDuration);
                    }

                    String molassesAmount = etMolassesAmount.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(molassesAmount)){
                        preparation.setMolassesAmount(molassesAmount);
                    }

                    String molassesWaitDuration = etMolassesWaitDuration.getText().toString().trim();
                    if(CommonConstants.isFloat(molassesWaitDuration)){
                        preparation.setMolassesWaitDuration(molassesWaitDuration);
                    }

                    String probioticsName = etProbioticsName.getText().toString().trim();
                    if(probioticsName != null && probioticsName.length() > 0){
                        preparation.setProbioticsName(probioticsName);
                    }

                    String probioticDosage = etProbioticsDosage.getText().toString().trim();
                    if(CommonConstants.isPositiveInteger(probioticDosage)){
                        preparation.setProbioticsDosage(probioticDosage);
                    }

                    if (groundWaterTds != null && groundWaterTds.length()>0 && targetTds != null && targetTds.length()>0){
                        try{
                            TankDao tankDao = new TankDao(TankPreparationFragment.this.getActivity());
                            Tank result = tankDao.getResult(tankId);
                            float tdsReq = (Integer.valueOf(targetTds)-Integer.valueOf(groundWaterTds));
                            float tankV = Float.valueOf(result.getMaxCapacity());
                            String saltAmount = "10000";

                            if(saltAmount != null && saltAmount.length() > 0){
                                preparation.setSaltAmount(saltAmount);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }


                    if(probioticStrain != null && probioticStrain.length() > 0){
                        preparation.setProbioticStrain(probioticStrain);
                        preparation.setProbioticStrength(probioticStrength);
                    }

                    String state = null;
                    if(rbLiquid.isChecked()){
                        state = CommonConstants.PROBOTIC_STATE_LIQUID;
                    }else if (rbSolid.isChecked()){
                        state = CommonConstants.PROBOTIC_STATE_SOLID;
                    }
                    if(state != null && state.length() > 0){
                        preparation.setProbioticState(state);
                    }

                    builder = new LoadingDialog.Builder(TankPreparationFragment.this.getActivity()).isCancellable(false);
                    builder.setMessage(getString(R.string.please_wait)).build();
                    PostTankPreparation.getInstance().callAPI(new APIClientResponse() {
                                                                  @Override
                                                                  public void onFailure(String msg, Object sender) {
                                                                      if(CommonConstants.OFFLINE_MODE){
                                                                          if (preparation.getId() == null){
                                                                              Print.e(this, "setting prep id:"+preparation.getTankId() +" tank id:"+preparation.getTankId());
                                                                              preparation.setId(preparation.getTankId());
                                                                          }
                                                                          preparationDao.insert(preparation);
                                                                          FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                                          if (Build.VERSION.SDK_INT >= 26) {
                                                                              ft.setReorderingAllowed(false);
                                                                          }
                                                                          ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();
                                                                      }
                                                                      builder.dismiss();
                                                                      ToastMsg.Toast(TankPreparationFragment.this.getActivity(), "Upload failed please try again later");
                                                                      FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                                      if (Build.VERSION.SDK_INT >= 26) {
                                                                          ft.setReorderingAllowed(false);
                                                                      }
                                                                      ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();


                                                                  }

                                                                  @Override
                                                                  public void onSuccess(String msg, Object sender) {
                                                                      Preparation prep = (Preparation) sender;
                                                                      preparationDao.insert(prep);
                                                                      builder.dismiss();

                                                                      FragmentTransaction ft = getFragmentManager().beginTransaction();
                                                                      if (Build.VERSION.SDK_INT >= 26) {
                                                                          ft.setReorderingAllowed(false);
                                                                      }
                                                                      ft.detach(TankPreparationFragment.this).attach(TankPreparationFragment.this).commit();

                                                                  }
                                                              },null,preparation.getTankId(), null,null, getString(R.string.base_url),
                            preparation.getPrepStartTs(),preparation.getTemperature(), preparation.getPhLevel(),
                            preparation.getTdsLevel(), preparation.getDoLevel(), preparation.getArsenicLevel(), preparation.getClorinLevel(),
                            preparation.getIronLevel(),preparation.getAreationTs(), preparation.getAreationUnit(), preparation.getAreationPoint(),
                            preparation.getAreationWaitDuration(), preparation.getGroundWaterTds(), preparation.getTargetTds(), preparation.getSaltAmount(),
                            preparation.getTdsWaitDuration(), preparation.getMolassesAmount(), preparation.getMolassesWaitDuration(),
                            preparation.getProbioticsName(), preparation.getProbioticsDosage(), preparation.getProbioticState(), preparation.getProbioticStrength(),
                            preparation.getProbioticStrain());
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)TankPreparationFragment.this.getActivity()).onBackPressed();
            }
        });

        rlNewProbiotic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DialogFragment dialogFragment = new ProbioticDialogFragment();
                ((ProbioticDialogFragment) dialogFragment).setViews(rlNewProbiotic, tvProbioticsStrenth);
                dialogFragment.show(ft, "dialog");
            }
        });
/*
        if (prepComplete){
            btnSave.setVisibility(View.GONE);
            btnCancel.setText("Back");
        }*/

        return mView;
    }

    private void hideAView(View view){
        view.setVisibility(View.GONE);
    }
    private void alterVisibility(TextView textView, View hideView, String value, String prefix, String suffix){
        if(value!=null){
            if(textView!=null){
                textView.setVisibility(View.VISIBLE);
                setValue(textView, value, prefix, suffix);
            }

            if(hideView!=null){
                hideView.setVisibility(View.GONE);
            }
           // prepComplete = true;
        }else{
            if(textView!=null){
                textView.setVisibility(View.GONE);
            }
            if(hideView != null){
                hideView.setVisibility(View.VISIBLE);
            }
           // prepComplete = false;
        }

    }


    private void setValue(TextView tv, String value, String prefix, String suffix){
        if (prefix==null){
            prefix = "";
        }
        if (suffix == null){
            suffix = "";
        }
        tv.setText(prefix+" "+value+" "+suffix);
    }
    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
    private void noPreparationFound(){
        hideAView(tvPrepStartDate);
        hideAView(tvTemperature);
        hideAView(tvPHLevel);
        hideAView(tvTDSLevel);
        hideAView(tvDOLevel);
        hideAView(tvArsenicLevel);
        hideAView(tvClorinLevel);
        hideAView(tvIronLevel);
        hideAView(tvAreationUnit);
        hideAView(tvAreationPoint);
        hideAView(tvAreationWaitDuration);
        hideAView(tvGroundWaterTDS);
        hideAView(tvTargetTDS);
        hideAView(tvSaltAmount);
        hideAView(tvTDSWaitDuration);
        hideAView(tvMolassesAmount);
        hideAView(tvMolassesWaitDuration);
        hideAView(tvProbioticsName);
        hideAView(tvProbioticsDosage);
        hideAView(tvProbioticsStrenth);

    }

    @SuppressLint("ValidFragment")
    public static class ProbioticDialogFragment extends DialogFragment {
        AppCompatEditText etProbioticStrain, etProbioticStrength;
        TextView tvProbiotic, tvProbs;
        String strainsAndStreanth = "";
        RelativeLayout rlProbiotics;
        private void setViews(RelativeLayout rlProbiotics, TextView tvProbs){
            this.rlProbiotics = rlProbiotics;
            this.tvProbs = tvProbs;
        }
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            probioticStrain = null;
            probioticStrength = null;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.dialog_fragment_probiotic, container, false);
            etProbioticStrain = (AppCompatEditText) v.findViewById(R.id.etProbioticStrain);
            etProbioticStrength = (AppCompatEditText) v.findViewById(R.id.etProbioticStrength);
            tvProbiotic = (TextView) v.findViewById(R.id.tvProbiotic);
            v.findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String strains = etProbioticStrain.getText().toString().trim();
                    String strength = etProbioticStrength.getText().toString().trim();
                    if (strains != null && strains.length() >0 && strength != null && strength.length() > 0){
                        if (probioticStrain != null){
                            probioticStrain += ","+strains;
                        }else{
                            probioticStrain += strains;
                        }

                        if (probioticStrength != null){
                            probioticStrength += ","+strength;
                        }else{
                            probioticStrength += ","+strength;
                        }
                        strainsAndStreanth += strains+" - "+strength+"\n";
                    }

                    if (probioticStrain != null && probioticStrength != null){
                        rlProbiotics.setVisibility(View.GONE);
                        tvProbs.setVisibility(View.VISIBLE);
                        tvProbs.setText("Probiotic strain and strength: \n"+strainsAndStreanth);
                    }
                    ProbioticDialogFragment.this.dismiss();
                }


            });

            v.findViewById(R.id.btnAddMore).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String strains = etProbioticStrain.getText().toString().trim();
                    if (strains == null || strains.length() == 0){
                        ToastMsg.Toast(ProbioticDialogFragment.this.getActivity(), "Please add  probiotic strains");
                        return;
                    }
                    String strength = etProbioticStrength.getText().toString().trim();
                    if (strength == null || strength.length() == 0){
                        ToastMsg.Toast(ProbioticDialogFragment.this.getActivity(), "Please add  probiotic strength");
                        return;
                    }
                    if (probioticStrain != null){
                        probioticStrain += ","+strains;
                    }else{
                        probioticStrain += strains;
                    }

                    if (probioticStrength != null){
                        probioticStrength += ","+strength;
                    }else{
                        probioticStrength += ","+strength;
                    }

                    strainsAndStreanth += strains+" - "+strength+"\n";
                    etProbioticStrain.setText("");
                    etProbioticStrength.setText("");
                    tvProbiotic.setText(strainsAndStreanth);


                }


            });
            v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    probioticStrain = null;
                    probioticStrength = null;
                    ProbioticDialogFragment.this.dismiss();
                }
            });
            // Do all the stuff to initialize your custom view

            return v;
        }
    }
}
