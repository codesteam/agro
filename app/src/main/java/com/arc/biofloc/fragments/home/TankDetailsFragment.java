/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.Print;


public class TankDetailsFragment extends Fragment {

    public static TankDetailsFragment newInstance() {
        return new TankDetailsFragment();
    }


   TextView tvName, tvDetails, tvType, tvHeight, tvWaterLevel, tvTankShape, tvRadius,
           tvTextRadius, tvTextConicalHeight, tvConicalHeight, tvMaxCpacity, tvStartDate;
    AppCompatButton btnBack;
    Tank result;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_details, container, false);
        Bundle bundle = this.getArguments();
        long tankId = 0;
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        tvName = (TextView) mView.findViewById(R.id.tvName);
        tvDetails = (TextView) mView.findViewById(R.id.tvDetails);
        tvType = (TextView) mView.findViewById(R.id.tvType);
        tvHeight = (TextView) mView.findViewById(R.id.tvHeight);
        tvWaterLevel = (TextView) mView.findViewById(R.id.tvWaterLevel);
        tvTankShape = (TextView) mView.findViewById(R.id.tvTankShape);
        tvRadius = (TextView) mView.findViewById(R.id.tvRadius);
        tvTextRadius = (TextView) mView.findViewById(R.id.tvTextRadius);
        tvTextConicalHeight = (TextView) mView.findViewById(R.id.tvTextConicalHeight);
        tvConicalHeight = (TextView) mView.findViewById(R.id.tvConicalHeight);
        tvMaxCpacity = (TextView) mView.findViewById(R.id.tvMaxCpacity);
        tvStartDate = (TextView) mView.findViewById(R.id.tvStartDate);
        btnBack = (AppCompatButton) mView.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)TankDetailsFragment.this.getActivity()).onBackPressed();
            }
        });

        TankDao tankDao = new TankDao(TankDetailsFragment.this.getActivity());
        result = tankDao.getResult(tankId);
        if (result != null){
            tvName.setText(result.getName()+".");
            tvDetails.setText(result.getDetails()+".");
            tvType.setText(result.getType()+".");
            tvHeight.setText(result.getHeight()+".");
            tvWaterLevel.setText(result.getWaterLevel());
            tvStartDate.setText(CommonConstants.getDateAndTime(result.getStartTs()) +".");
            tvTankShape.setText(result.getTankShape()+".");
            if (result.getTankShape().equals(CommonConstants.TANK_TYPE_CYL)){
                tvTextConicalHeight.setText("Conical Height:");
                tvTextRadius.setText("Radius:");
            }else{
                tvTextConicalHeight.setText("Width:");
                tvTextRadius.setText("Length:");
            }
            tvRadius.setText(result.getRadius()+".");
            tvConicalHeight.setText(result.getConicalHeight()+".");
            tvMaxCpacity.setText(CommonConstants.getUpto2DecimalPoint(Double.parseDouble(result.getMaxCapacity())));
        }else{
            Print.e(this, "Result not found id:"+tankId);
        }
        return mView;
    }



    @Override
    public void onResume() {
        super.onResume();
        TankDao tankDao = new TankDao(this.getActivity());

        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
