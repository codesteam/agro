/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.DividerItemDecoration;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;


public class FragmentHomeTanks extends Fragment {

    public static FragmentHomeTanks newInstance() {
        return new FragmentHomeTanks();
    }

    RelativeLayout rlNewProject;

   // LinearLayout llCylinderTankParam, llRectTankParam;


    private RecyclerView recyclerView;
    TextView tvSelectAfishTank;
    ArrayList<Tank> results;
    RecyclerAdapter recyclerAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_home_tanks, container, false);
        rlNewProject = (RelativeLayout)  mView.findViewById(R.id.rlNewProject);
        recyclerView = (RecyclerView)mView.findViewById(R.id.recycler);
        tvSelectAfishTank = (TextView) mView.findViewById(R.id.tvSelectAFishTank);
        recyclerView.setHasFixedSize(true);
        rlNewProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewTankFragment homeFragment = new NewTankFragment();
                FragmentManager fragmentManager = FragmentHomeTanks.this.getActivity().getSupportFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.container_body, homeFragment, "NewTankFragment");

                // Do not add fragment three in back stack.
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
            }
        };

        TankDao tankDao = new TankDao(this.getActivity());
        results = tankDao.getResults(CommonSettings.getInstance(this.getActivity()).getProjectId());
        if (results != null){
            Print.e(this, "Results: "+results.size());
        }
        recyclerAdapter = new RecyclerAdapter(results);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

        return mView;
    }

    private void resetRecycleView( ArrayList<Tank> results){
        this.results = results;
        recyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();
        TankDao tankDao = new TankDao(this.getActivity());
        results = tankDao.getResults(CommonSettings.getInstance(this.getActivity()).getProjectId());
        if (results.size() > 0){
            resetRecycleView(results);
            recyclerView.setVisibility(View.VISIBLE);
            tvSelectAfishTank.setVisibility(View.VISIBLE);
        }else{
            recyclerView.setVisibility(View.GONE);
            tvSelectAfishTank.setVisibility(View.GONE);
        }
        ((MainActivity)this.getActivity()).setActionBarTitle("Home");


    }


    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<Tank> results;

        class ViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout placeholderView;
            TextView tvName, tvDetails;
            ViewHolder(View itemView) {
                super(itemView);
                placeholderView = (RelativeLayout) itemView.findViewById(R.id.placeholder_view);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvDetails = (TextView) itemView.findViewById(R.id.tvDetails);
            }
        }

        RecyclerAdapter(List<Tank> results) {
            this.results = results;
        }

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.placeholder_tank, parent, false);
            return new RecyclerAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {


            holder.tvName.setText(results.get(position).getName()+"");
            holder.tvDetails.setText(results.get(position).getDetails()+"");
            holder.placeholderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentPropertyTank fragment = new FragmentPropertyTank();
                    Bundle bundle = new Bundle();
                    bundle.putLong("id", results.get(position).getId());
                    bundle.putString("name", results.get(position).getName());
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = FragmentHomeTanks.this.getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
        }

        @Override
        public int getItemCount() {
            return results.size();
        }

    }
    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
