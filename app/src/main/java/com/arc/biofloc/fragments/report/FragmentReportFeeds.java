/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.report;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.db.agrodao.FeedsDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;
import com.arc.biofloc.utility.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;


public class FragmentReportFeeds extends Fragment {

    public static FragmentReportFeeds newInstance() {
        return new FragmentReportFeeds();
    }



   // LinearLayout llCylinderTankParam, llRectTankParam;


    private RecyclerView recyclerView;

    ArrayList<Feed> results;
    RecyclerAdapter recyclerAdapter;
    Long tankId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_report_feed, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        recyclerView = (RecyclerView)mView.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
            }
        };

        FeedsDao dao = new FeedsDao(this.getActivity());
        results = dao.getResults(tankId);
        if (results != null && results.size() > 0){
          mView.findViewById(R.id.noRecord).setVisibility(View.GONE);
        }
        recyclerAdapter = new RecyclerAdapter(results);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

        return mView;
    }

    private void resetRecycleView( ArrayList<Feed> results){
        this.results = results;
        recyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle("Report");

    }


    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<Feed> results;

        class ViewHolder extends RecyclerView.ViewHolder {

            //LinearLayout placeholderView;
            TextView tvDate, tvBrandName, tvFeedType, tvProtienLevel, tvFishAge;
            ViewHolder(View itemView) {
                super(itemView);
                //placeholderView = (LinearLayout) itemView.findViewById(R.id.row);
                tvDate = (TextView) itemView.findViewById(R.id.tvDate);
                tvBrandName = (TextView) itemView.findViewById(R.id.tvBrandName);
                tvFeedType = (TextView) itemView.findViewById(R.id.tvFeedType);
                tvFishAge = (TextView) itemView.findViewById(R.id.tvFishAge);
                tvProtienLevel = (TextView) itemView.findViewById(R.id.tvProtienLevel);
            }
        }

        RecyclerAdapter(List<Feed> results) {
            this.results = results;
        }

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ConstraintLayout view = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_feeds, parent, false);
            return new RecyclerAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {


            holder.tvDate.setText("Start date: "+results.get(position).getStartDate()+"");
            holder.tvBrandName.setText("Brand Name: "+results.get(position).getBrand()+"");
            holder.tvFeedType.setText("Feed type: "+results.get(position).getType()+"");
            holder.tvProtienLevel.setText("Protien level: "+results.get(position).getProteinLevel()+"");
            holder.tvFishAge.setText("Fish age: "+results.get(position).getFishAge()+"");

        }

        @Override
        public int getItemCount() {
            return results.size();
        }

    }
    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
