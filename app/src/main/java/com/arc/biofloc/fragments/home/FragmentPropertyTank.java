/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;


public class FragmentPropertyTank extends Fragment {

    public static FragmentPropertyTank newInstance() {
        return new FragmentPropertyTank();
    }

    RelativeLayout rlNewProject;

   // LinearLayout llCylinderTankParam, llRectTankParam;
    Long tankId;
    String tankName;

    AppCompatButton btnStatus, btnPreparation, btnFlocStatus, btnFishSeeds, btnFeeding,
            btnDaily, btnWeeklyChecklist,btnSeedsReleasing, btnFeeds, btnHarvesting;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_property_tank, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
            tankName = bundle.getString("name");
        }
        btnStatus = (AppCompatButton) mView.findViewById(R.id.btnStatus);
        btnPreparation = (AppCompatButton) mView.findViewById(R.id.btnPreparation);
        btnFlocStatus = (AppCompatButton) mView.findViewById(R.id.btnFlocStatus);
        btnFishSeeds = (AppCompatButton) mView.findViewById(R.id.btnFishSeeds);
        btnFeeding = (AppCompatButton) mView.findViewById(R.id.btnFeeding);
        btnDaily = (AppCompatButton) mView.findViewById(R.id.btnDaily);
        btnFeeds = (AppCompatButton) mView.findViewById(R.id.btnFeeds);
        btnHarvesting = (AppCompatButton) mView.findViewById(R.id.btnHarvesting);
        btnWeeklyChecklist = (AppCompatButton) mView.findViewById(R.id.btnWeeklyChecklist);
        btnSeedsReleasing = (AppCompatButton) mView.findViewById(R.id.btnFishSeedsRelease);

        btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TankDetailsFragment fragment = new TankDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnPreparation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               TankPreparationFragment fragment = new TankPreparationFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnFlocStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentFlocStatus fragment = new FragmentFlocStatus();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnFishSeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               FragmentFishSeeds fragment = new FragmentFishSeeds();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentFeeding fragment = new FragmentFeeding();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               FragmentDailyChecklist fragment = new FragmentDailyChecklist();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnWeeklyChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               FragmentWeeklyChecklist fragment = new FragmentWeeklyChecklist();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        btnSeedsReleasing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentFishSeedsRelease fragment = new FragmentFishSeedsRelease();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentFeed fragment = new FragmentFeed();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnHarvesting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHervesting fragment = new FragmentHervesting();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyTank.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle(tankName+"");
        ((MainActivity)this.getActivity()).enableBack(true);
    }

}
