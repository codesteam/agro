/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.FeedsDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostFeeds;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentFeed extends Fragment {

    public static FragmentFeed newInstance() {
        return new FragmentFeed();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etStartDate, etBrand, etProtienLevel, etFishAge;


    long tankId = 0;
    RadioGroup rgFeedType;
    RadioButton rbStarter, rbGrower;
    String feedType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_feed, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etStartDate = (AppCompatEditText)mView.findViewById(R.id.etStartDate);
        etBrand = (AppCompatEditText)mView.findViewById(R.id.etBrand);
        etProtienLevel = (AppCompatEditText)mView.findViewById(R.id.etProtienLevel);
        etFishAge = (AppCompatEditText)mView.findViewById(R.id.etFishAge);

       // tvEntryDate = (TextView) mView.findViewById(R.id.tvEntryDate);


        rgFeedType = (RadioGroup) mView.findViewById(R.id.rgFeedType);
        rbStarter = (RadioButton) mView.findViewById(R.id.rbStarter);
        rbGrower = (RadioButton) mView.findViewById(R.id.rbGrower);



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentFeed.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String startDate = etStartDate.getText().toString().trim();
                if (!CommonConstants.isValidDate(startDate)){
                    ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please enter feeding start date for this tank.\nor check input formate e.g. \n23/08/2019\n13/03/2019");
                    return;
                }
                final String brand = etBrand.getText().toString().trim();
                if (brand==null || brand.length()==0){
                    ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please enter brand name of this feed.");
                    return;
                }

                if(rbGrower.isChecked()){
                    feedType = CommonConstants.FEED_STATE_GROWER;
                }else if (rbStarter.isChecked()){
                    feedType = CommonConstants.FEED_STATE_STARTER;
                }else{
                    ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please select feed Type of this feed.");
                    return;
                }


                final String protienLevel = etProtienLevel.getText().toString().trim();
                if (!CommonConstants.isFloat(protienLevel)){
                    ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please enter protein level of this feed.\nor check input formate e.g. \n45.5\n32");
                    return;
                }

                final String fishAge = etFishAge.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(fishAge)){
                    ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please enter fish age in days.\nor check input formate e.g. \n120\n32");
                    return;
                }


                builder = new LoadingDialog.Builder(FragmentFeed.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostFeeds.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            Feed result = new Feed();
                            result.setId((long)CommonConstants.getRandomInt(0,100000));
                            result.setTankId(tankId);
                            result.setFishAge(fishAge+"");
                            result.setType(feedType);
                            result.setProteinLevel(protienLevel);
                            result.setBrand(brand+"");
                            result.setStartDate(startDate);
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            FeedsDao dao = new FeedsDao(FragmentFeed.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentFeed.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentFeed.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentFeed.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Feed responce = (Feed) sender;
                        FeedsDao dao = new FeedsDao(FragmentFeed.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentFeed.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentFeed.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null, null, getString(R.string.base_url),
                        startDate +"",brand, feedType +"",protienLevel,fishAge);
            }
        });
        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
