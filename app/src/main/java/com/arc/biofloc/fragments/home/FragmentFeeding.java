/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.GoBackDialog;
import com.arc.biofloc.customdialog.GoBackListner;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.DailyChecklistDao;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.db.agrodao.FeedsDao;
import com.arc.biofloc.db.agrodao.SeedsDao;
import com.arc.biofloc.db.agrodao.SeedsReleaseDao;
import com.arc.biofloc.db.agrodao.WeeklyChecklistDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostFeeding;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feed;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.SeedsRelease;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentFeeding extends Fragment {

    public static FragmentFeeding newInstance() {
        return new FragmentFeeding();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etOverrideFeedPercentage, etCarbonPercentage;
    TextView tvEntryDate, tvBioMass, tvFeedPercentage, tvFeedAmount, tvProteinAmount, tvProducedNitrogen, tvWastedNitrogen, tvCatalystAmount;

    long tankId = 0;
    RadioGroup rgBiomassType;
    RadioButton tAb, tLb;

    RadioGroup rgCarbonSourceType;
    RadioButton rbCsSugar, rbCsMolasses;

    DailyChecklistDao dailyChecklistDao;
    WeeklyChecklistDao weeklyChecklistDao;
    FeedsDao feedsDao;
    SeedsDao seedsDao;
    SeedsReleaseDao seedsReleaseDao;
    FeedingDao feedingDao;
    DailyChecklist dailyChecklist;
    WeeklyCheckList weeklyCheckList;
    Feed feed;
    SeedsRelease seedsRelease;
    Seeds seeds;
    Feeding feeding;

    int carbonSource;
    int tankBiomass;
    GoBackDialog.Builder resolutionBuilder;
    float feedPercentage = 0.0f;
    float biomass = 0.0f;
    float medianBioMass = 0.0f;
    float legitimateBiomass = 0.0f;
    float feedAmount, proteinAmount, producedNitrogen,wastedNitrogen, catalystAmount, carbonParcentage;
    float actualBiomass = 0.0f;
    String carbonSourceType, entryTs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_feeding, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        entryTs = System.currentTimeMillis()+"";
        dailyChecklistDao = new DailyChecklistDao(this.getActivity());
        weeklyChecklistDao = new WeeklyChecklistDao(this.getActivity());
        feedsDao = new FeedsDao(this.getActivity());
        seedsDao = new SeedsDao(this.getActivity());
        seedsReleaseDao = new SeedsReleaseDao(this.getActivity());
        feedingDao = new FeedingDao(this.getActivity());
        dailyChecklist = dailyChecklistDao.getResult(tankId);
        if (dailyChecklist == null){
            goBackDialog("Please enter entry for daily check list first");
            return mView;
        }
       /* weeklyCheckList = weeklyChecklistDao.getResults(tankId);
        if(weeklyCheckList == null){
            goBackDialog("Please enter entry for weekly check list first");
            return mView;
        }*/

        feed = feedsDao.getResult(tankId);
        if (feed == null){
            goBackDialog("Please enter feed info first");
            return mView;
        }
        seeds = seedsDao.getResult(tankId);
        if(seeds == null){
            goBackDialog("Please enter seeds info first");
            return mView;
        }
        seedsRelease = seedsReleaseDao.getResult(tankId);
        if (seedsRelease == null){
            goBackDialog("Please enter seeds release info first");
            return mView;
        }

        feeding = feedingDao.getResult(tankId);

        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);
        etCarbonPercentage = (AppCompatEditText)mView.findViewById(R.id.etCarbonPercentage);
        etOverrideFeedPercentage = (AppCompatEditText)mView.findViewById(R.id.etOverrideFeedPercentage);
        tvBioMass = (TextView) mView.findViewById(R.id.tvBioMass);
        tvEntryDate = (TextView) mView.findViewById(R.id.tvEntryDate);
        tvFeedPercentage = (TextView) mView.findViewById(R.id.tvFeedPercentage);
        tvFeedAmount = (TextView) mView.findViewById(R.id.tvFeedAmount);
        tvProteinAmount = (TextView) mView.findViewById(R.id.tvProteinAmount);
        tvProducedNitrogen = (TextView) mView.findViewById(R.id.tvProducedNitrogen);
        tvWastedNitrogen = (TextView) mView.findViewById(R.id.tvWastedNitrogen);
        tvCatalystAmount = (TextView) mView.findViewById(R.id.tvCatalystAmount);
        rgBiomassType = (RadioGroup) mView.findViewById(R.id.rgBiomassType);
        tAb = (RadioButton) mView.findViewById(R.id.tAb);
        tLb = (RadioButton) mView.findViewById(R.id.tLb);
        rgCarbonSourceType = (RadioGroup) mView.findViewById(R.id.rgCarbonSourceType);
        rbCsSugar = (RadioButton) mView.findViewById(R.id.rbCsSugar);
        rbCsMolasses = (RadioButton) mView.findViewById(R.id.rbCsMolasses);


        tvEntryDate.setText("Entry date: "+CommonConstants.getDateAndTime(entryTs));

        int noOfSeedsWhenReleased = Integer.valueOf(seeds.getNumberOfSeeds()) - Integer.valueOf(seedsRelease.getMortality());
        int sumOfMortalityAfterReleased = dailyChecklistDao.getSumOfMortality(tankId); //seeds died after released
        float releasedBiomass = Float.valueOf(seedsRelease.getQuantity()); // weight of seeds the day we released
        float releasedMedianBiomass = releasedBiomass / noOfSeedsWhenReleased;
        final int liveSeeds = noOfSeedsWhenReleased - sumOfMortalityAfterReleased;

        if (weeklyCheckList != null){
            // after n'th week median biomass of seeds
          medianBioMass = Float.valueOf(weeklyCheckList.getMeadinBiomass()) /liveSeeds;

        }else{ // since there is no weekly checklist recorder, this is first week
            medianBioMass = (releasedBiomass - (releasedMedianBiomass*sumOfMortalityAfterReleased)) / liveSeeds;
        }
        actualBiomass = medianBioMass * liveSeeds;
        if (feeding == null){
            legitimateBiomass = (releasedBiomass - (releasedMedianBiomass*sumOfMortalityAfterReleased));

        }else{
            legitimateBiomass = Float.valueOf(feeding.getLegitimateBiomass());
        }
        tvBioMass.setText("Biomass: "+CommonConstants.getUpto2DecimalPoint(actualBiomass) +" grams");
        rgBiomassType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                feedPercentage = getPercentage(biomass/liveSeeds);
                calculateParams(biomass, feedPercentage);
                if (checkedId == R.id.tAb) {
                    tvBioMass.setText("Biomas: "+CommonConstants.getUpto2DecimalPoint(actualBiomass) +" grams");
                    biomass = actualBiomass;
                } else if (checkedId == R.id.tLb) {
                    tvBioMass.setText("Biomas: "+CommonConstants.getUpto2DecimalPoint(legitimateBiomass) +" grams");
                    biomass = legitimateBiomass;
                }

            }

        });

        tAb.setChecked(true);
        biomass = actualBiomass;
        feedPercentage = getPercentage(biomass/liveSeeds);
        calculateParams(biomass, feedPercentage);

       // tvEntryDate = (TextView) mView.findViewById(R.id.tvEntryDate);
        etCarbonPercentage.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String value = String.valueOf(s).trim();
                try{
                    carbonParcentage = Float.valueOf(value);
                    catalystAmount = wastedNitrogen * carbonParcentage/10.0f;
                    tvCatalystAmount.setText("Catalyst amount: "+CommonConstants.getUpto2DecimalPoint(catalystAmount)+" gram");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        etOverrideFeedPercentage.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {

                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String value = String.valueOf(s).trim();

                try{
                    if(value != null && value !="" && value.length() > 0){
                        feedPercentage =  Float.valueOf(value.trim()).floatValue();
                    }else{
                        float medBioMass = biomass/liveSeeds;
                        feedPercentage = getPercentage(medBioMass);
                    }
                    Print.e(this, "feed feedPercentage: "+ feedPercentage);
                    calculateParams(biomass, feedPercentage);

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });




        rgCarbonSourceType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbCsMolasses) {
                   carbonSourceType = CommonConstants.CARBON_SOURCE_MOLASSES;
                } else if (checkedId == R.id.rbCsSugar) {
                    carbonSourceType = CommonConstants.CARBON_SOURCE_SUGAR;
                }

            }

        });

        rbCsMolasses.setChecked(true);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentFeeding.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String cp = etCarbonPercentage.getText().toString().trim();
                if (!CommonConstants.isFloat(cp)){
                    ToastMsg.Toast(FragmentFeeding.this.getActivity(), "Please enter carbon feedPercentage.\nor check input formate e.g. \n12.5\n10");
                    return;
                }
                float usedProtein = proteinAmount - producedNitrogen;
                legitimateBiomass += usedProtein;

                builder = new LoadingDialog.Builder(FragmentFeeding.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostFeeding.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                          /*  Feed result = new Feed();
                            result.setId((long)CommonConstants.getRandomInt(0,100000));
                            result.setTankId(tankId);
                            result.setFishAge(fishAge+"");
                            result.setType(feedType);
                            result.setProteinLevel(protienLevel);
                            result.setBrand(brand+"");
                            result.setStartDate(startDate);
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            FeedsDao dao = new FeedsDao(FragmentFeeding.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentFeeding.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentFeeding.this.getActivity()).onBackPressed();
*/
                        }else{
                            ToastMsg.Toast(FragmentFeeding.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Feeding responce = (Feeding) sender;
                        FeedingDao dao = new FeedingDao(FragmentFeeding.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentFeeding.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentFeeding.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null,null, getString(R.string.base_url),
                        entryTs +"",legitimateBiomass+"", feedPercentage +"",feedAmount+"",
                        proteinAmount+"",producedNitrogen+"", wastedNitrogen+"", carbonSourceType,
                        carbonParcentage+"", catalystAmount+"");
            }
        });
        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }

    @Override
    public void onPause() {
        super.onPause();
        if (resolutionBuilder != null){
            resolutionBuilder.dismiss();
        }
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }



    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }

    private  void goBackDialog(String message){
        resolutionBuilder = new GoBackDialog.Builder(this.getActivity());
        resolutionBuilder.isCancellable(false)
                .setMessage(message)
                .build(new GoBackListner() {
                    @Override
                    public void onBack() {
                        resolutionBuilder.dismiss();
                        FragmentFeeding.this.getActivity().onBackPressed();
                    }
                });
    }

    private float getPercentage(float medianBiomass){

        float temparature = Float.valueOf(dailyChecklist.getTemperature());
            if(medianBiomass > 0.0 && medianBiomass <=1.0){
                if (temparature <=22.0){
                    return 6;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 7.5f;
                }
                 else if (temparature > 24.0 && temparature <=26.0){
                    return 9;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 10;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 9;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 8;
                }
                else{// if (temparature > 32.0){
                    return 8;
                }
            }else if(medianBiomass > 1.0 && medianBiomass <=3.0){
                if (temparature <=22.0){
                    return 5;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 6;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 7.5f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 8;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 7.5f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 6.5f;
                }
                else{// if (temparature > 32.0){
                    return 6.5f;
                }
            }
            else if(medianBiomass > 3.0 && medianBiomass <=6.0){
                if (temparature <=22.0){
                    return 4;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 5;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 6;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 6.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 6;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 5.5f;
                }
                else{// if (temparature > 32.0){
                    return 5.5f;
                }
            }
            else if(medianBiomass > 6.0 && medianBiomass <=10.0){
                if (temparature <=22.0){
                    return 3;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 4;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 5;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 5.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 5f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 4.5f;
                }
                else {//if (temparature > 32.0){
                    return 4.5f;
                }
            }
            else if(medianBiomass > 10.0 && medianBiomass <=30.0){
                if (temparature <=22.0){
                    return 3;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 4;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 5;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 5.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 5;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 4;
                }
                else{// if (temparature > 32.0){
                    return 4;
                }
            }
            else if(medianBiomass > 30.0 && medianBiomass <=70.0){
                if (temparature <=22.0){
                    return 2.5f;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 3.5f;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 4f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 4.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 4f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 3.5f;
                }
                else{// if (temparature > 32.0){
                    return 3.5f;
                }
            }
            else if(medianBiomass > 70.0 && medianBiomass <=100.0){
                if (temparature <=22.0){
                    return 2;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 2.5f;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 3;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 3.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 3;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 2.5f;
                }
                else {//if (temparature > 32.0){
                    return 0;
                }
            }
            else if(medianBiomass > 100.0 && medianBiomass <=200.0){
                if (temparature <=22.0){
                    return 1.5f;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 2;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 2.5f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 2.5f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 2.5f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 2;
                }
                else{// if (temparature > 32.0){
                    return 2;
                }
            }
            else if(medianBiomass > 200.0 && medianBiomass <=400.0){
                if (temparature <=22.0){
                    return 1;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 1.5f;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 1.8f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 2;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 1.8f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 1.5f;
                }
                else {//if (temparature > 32.0){
                    return 1.5f;
                }
            }
            else if(medianBiomass > 400.0 && medianBiomass <=800.0){
                if (temparature <=22.0){
                    return 1;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 1.5f;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 1.5f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 1.75f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 1.5f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 1.5f;
                }
                else{// if (temparature > 32.0){
                    return 1.5f;
                }
            }
            else if(medianBiomass > 800){
                if (temparature <=22.0){
                    return 0.75f;
                }else if (temparature > 22.0 && temparature <=24.0){
                    return 1.0f;
                }
                else if (temparature > 24.0 && temparature <=26.0){
                    return 1.25f;
                }
                else if (temparature > 26.0 && temparature <=28.0){
                    return 1.50f;
                }
                else if (temparature > 28.0 && temparature <=30.0){
                    return 1.25f;
                }
                else if (temparature > 30.0 && temparature <=32.0){
                    return 1.0f;
                }
                else {//if (temparature > 32.0){
                    return 1.0f;
                }
            }else{
                return  0;
            }
    }

    private void calculateParams(float bioMass, float feedPercentage){
        tvFeedPercentage.setText("Feed Percentage: "+feedPercentage+"%");
        feedAmount = bioMass * this.feedPercentage /100.0f;
        tvFeedAmount.setText("Feed amount: "+CommonConstants.getUpto2DecimalPoint(feedAmount)+" grams");
        float proteinLevel = Float.valueOf(feed.getProteinLevel());
        proteinAmount = feedAmount *proteinLevel/100.0f;
        tvProteinAmount.setText("Protein amount: "+CommonConstants.getUpto2DecimalPoint(proteinAmount)+" grams");
        producedNitrogen = proteinAmount * 16.0f/100.0f;
        tvProducedNitrogen.setText("Produced Nitrogen: "+CommonConstants.getUpto2DecimalPoint(producedNitrogen)+" grams");
        wastedNitrogen = producedNitrogen * 75.0f/100.0f;
        tvWastedNitrogen.setText("Wasted Nitrogent: "+CommonConstants.getUpto2DecimalPoint(wastedNitrogen)+" grams");
    }
}
