/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.SeedsReleaseDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostSeedsRelease;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.SeedsRelease;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentFishSeedsRelease extends Fragment {

    public static FragmentFishSeedsRelease newInstance() {
        return new FragmentFishSeedsRelease();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etQuantity, etTemperatureAdjustment, etPmWash, etHour, etMorality, etWashDuration;
    TextView tvReleaseTime, tvQuantity, tvTemperatureAdjustment, tvPmWash, tvHour, tvMorality, tvWashDuration;
    long releaseTimeStamp;
    long tankId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_seeds_release, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etQuantity = (AppCompatEditText)mView.findViewById(R.id.etQuantity);
        etTemperatureAdjustment = (AppCompatEditText)mView.findViewById(R.id.etTemperatureAdjustment);
        etPmWash = (AppCompatEditText)mView.findViewById(R.id.etPmWash);
        etHour = (AppCompatEditText)mView.findViewById(R.id.etHour);
        etMorality = (AppCompatEditText)mView.findViewById(R.id.etMorality);
        etWashDuration = (AppCompatEditText)mView.findViewById(R.id.etWashDuration);

        tvReleaseTime = (TextView) mView.findViewById(R.id.tvReleaseTime);
        tvQuantity = (TextView) mView.findViewById(R.id.tvQuantity);
        tvTemperatureAdjustment = (TextView) mView.findViewById(R.id.tvTemperatureAdjustment);
        tvPmWash = (TextView) mView.findViewById(R.id.tvPmWash);
        tvHour = (TextView) mView.findViewById(R.id.tvHour);
        tvMorality = (TextView) mView.findViewById(R.id.tvMorality);
        tvWashDuration = (TextView) mView.findViewById(R.id.tvWashDuration);

        SeedsReleaseDao seedsDao = new SeedsReleaseDao(this.getActivity());
        SeedsRelease seeds = seedsDao.getResult(tankId);
        if (seeds != null){
            alterVisibility(tvReleaseTime,   null,   CommonConstants.getDateAndTime(seeds.getReleaseTs()), "Seeds releasing time:", ".");
            alterVisibility(tvQuantity, etQuantity, seeds.getQuantity(), "Weight of seeds: ", "gram.");
            alterVisibility(tvTemperatureAdjustment, etTemperatureAdjustment, seeds.getTemperatureAdjustment(), "Temperature adjustment: ", "minute.");
            alterVisibility(tvPmWash, etPmWash, seeds.getPmWash(), "PM wash (Number of times):", ".");
            alterVisibility(tvWashDuration, etWashDuration, seeds.getWashDuration(), "PM wash duration: ", " minute.");
            alterVisibility(tvHour, etHour, seeds.getHour(), "How long waited: ", "hour.");
            alterVisibility(tvMorality, etMorality, seeds.getMortality(), "How many seeds died: ", ".");
            btnSave.setVisibility(View.GONE);
            btnCancel.setText("Back");
            btnCancel.setTextColor(getResources().getColor(R.color.white));
            btnCancel.setBackgroundResource(R.drawable.bt_shape3);

        }else{
            releaseTimeStamp = System.currentTimeMillis();
            tvReleaseTime.setText(CommonConstants.getDateAndTime(releaseTimeStamp+""));
            tvQuantity.setVisibility(View.GONE);
           // tvTarget.setVisibility(View.GONE);
            tvTemperatureAdjustment.setVisibility(View.GONE);
            tvPmWash.setVisibility(View.GONE);
            tvWashDuration.setVisibility(View.GONE);
            tvHour.setVisibility(View.GONE);
            tvMorality.setVisibility(View.GONE);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentFishSeedsRelease.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String quantity = etQuantity.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(quantity)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter Weight of seeds in gram.\nor check input formate e.g. \n1000\n3500");
                    return;
                }
                final String ta = etTemperatureAdjustment.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(ta)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter Temperature adjustment in minute.\nor check input formate e.g. \n25\n90");
                    return;
                }

                final String pmWash = etPmWash.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(pmWash)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter how many times seeds wash with Potassium permanganate.\nor check input formate e.g. \n2\n5");
                    return;
                }

                final String duration = etWashDuration.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(duration)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter how long soaking seeds in PM solution in minute\nor check input formate e.g. \n50\n10");
                    return;
                }
                final String hour = etHour.getText().toString().trim();
                if (!CommonConstants.isFloat(hour)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter how long waited before release in hour\nor check input formate e.g. \n2.5\n1");
                    return;
                }

                final String mortalityCheck = etMorality.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(mortalityCheck)){
                    ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please enter how many seeds died.\nor check input formate e.g. \n10\n0");
                    return;
                }

                builder = new LoadingDialog.Builder(FragmentFishSeedsRelease.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostSeedsRelease.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            SeedsRelease result = new SeedsRelease();
                            result.setId(tankId);
                            result.setTankId(tankId);
                            result.setWashDuration(duration);
                            result.setMortality(mortalityCheck);
                            result.setHour(hour);
                            result.setPmWash(pmWash);
                            result.setTemperatureAdjustment(ta);
                            result.setQuantity(quantity);
                            result.setReleaseTs(releaseTimeStamp+"");

                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            SeedsReleaseDao dao = new SeedsReleaseDao(FragmentFishSeedsRelease.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentFishSeedsRelease.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        SeedsRelease responce = (SeedsRelease) sender;
                        SeedsReleaseDao dao = new SeedsReleaseDao(FragmentFishSeedsRelease.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentFishSeedsRelease.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentFishSeedsRelease.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null,null, getString(R.string.base_url),
                        releaseTimeStamp+"",quantity,ta,pmWash,duration,hour,mortalityCheck);
            }
        });
        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }

    private void alterVisibility(TextView textView, View hideView, String value, String prefix, String suffix){
        if(textView!=null){
            textView.setVisibility(View.VISIBLE);
            setValue(textView, value, prefix, suffix);
        }

        if(hideView!=null){
            hideView.setVisibility(View.GONE);
        }

    }
    private void setValue(TextView tv, String value, String prefix, String suffix){
        if (prefix==null){
            prefix = "";
        }
        if (suffix == null){
            suffix = "";
        }
        tv.setText(prefix+" "+value+" "+suffix);
    }
}
