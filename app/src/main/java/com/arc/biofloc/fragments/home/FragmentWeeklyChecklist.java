/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.db.agrodao.WeeklyChecklistDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostWeeklyChecklist;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.WeeklyCheckList;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentWeeklyChecklist extends Fragment {

    public static FragmentWeeklyChecklist newInstance() {
        return new FragmentWeeklyChecklist();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etMeadinBiomass, etMeadinSize, etLegitimateSize, etAmmoniaLevel;
    TextView tvEntryDate;
    Long entryTime;
    long tankId = 0;
    //float legitimateBiomass;
    FeedingDao feedingDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_weekly_check_list, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        feedingDao = new FeedingDao(this.getActivity());
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etMeadinBiomass = (AppCompatEditText)mView.findViewById(R.id.etMeadinBiomass);
        etMeadinSize = (AppCompatEditText)mView.findViewById(R.id.etMeadinSize);
        etLegitimateSize = (AppCompatEditText)mView.findViewById(R.id.etLegitimateSize);
        etAmmoniaLevel = (AppCompatEditText)mView.findViewById(R.id.etAmmoniaLevel);

        tvEntryDate = (TextView) mView.findViewById(R.id.tvEntryDate);
       // tvLegitimateBiomass = (TextView) mView.findViewById(R.id.tvLegitimateBiomass);

        entryTime = System.currentTimeMillis();
        tvEntryDate.setText("Entry date: "+CommonConstants.getDateAndTime(entryTime +""));
        //legitimateBiomass = Float.valueOf(feedingDao.getResults(tankId).getLegitimateBiomass()); // calculate from other
       // tvLegitimateBiomass.setText("Legitimate Biomass"+legitimateBiomass +" gram");

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentWeeklyChecklist.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String biomass = etMeadinBiomass.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(biomass)){
                    ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Please enter meadin biomass (gm)(weight of 10 fish/10).\nor check input formate e.g. \n100\n250");
                    return;
                }
                final String size = etMeadinSize.getText().toString().trim();
                if (!CommonConstants.isFloat(size)){
                    ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Please enter meadin size (inch) (summation of 10 fish size/10).\nor check input formate e.g. \n3\n1.5");
                    return;
                }
                final String legS = etLegitimateSize.getText().toString().trim();
                if (!CommonConstants.isFloat(legS)){
                    ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Please enter Legitimate size should be.\nor check input formate e.g. \n3.5\n5");
                    return;
                }

                final String aLevel = etAmmoniaLevel.getText().toString().trim();
                if (!CommonConstants.isFloat(aLevel)){
                    ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Please enter Ammonia level.\nor check input formate e.g. \n1.5\n2");
                    return;
                }


                builder = new LoadingDialog.Builder(FragmentWeeklyChecklist.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostWeeklyChecklist.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            WeeklyCheckList result = new WeeklyCheckList();
                            result.setId((long)CommonConstants.getRandomInt(0,100000));
                            result.setTankId(tankId);
                            result.setEntryTs(entryTime+"");
                            result.setLegitimateSize(legS);
                            result.setMeadinSize(size);
                            result.setLegitimateBiomass(0+"");
                            result.setMeadinBiomass(biomass);
                            result.setAmmoniaLevel(aLevel);
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            WeeklyChecklistDao dao = new WeeklyChecklistDao(FragmentWeeklyChecklist.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentWeeklyChecklist.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        WeeklyCheckList responce = (WeeklyCheckList) sender;
                        WeeklyChecklistDao dao = new WeeklyChecklistDao(FragmentWeeklyChecklist.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentWeeklyChecklist.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentWeeklyChecklist.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null,null, getString(R.string.base_url),
                        entryTime +"",biomass, 0+"",size,legS,aLevel);
            }
        });

        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }




    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
