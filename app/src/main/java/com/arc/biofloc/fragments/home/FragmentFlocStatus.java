/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.FlocDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostFloc;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentFlocStatus extends Fragment {

    public static FragmentFlocStatus newInstance() {
        return new FragmentFlocStatus();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etTemperature, etVolume, etExamineDuration, etProbiotics, etMolasses, etFeed, etRemark;
    TextView tvExamineDate;
    Long examineTime;
    long tankId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_floc, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etTemperature = (AppCompatEditText)mView.findViewById(R.id.etTemperature);
        etVolume = (AppCompatEditText)mView.findViewById(R.id.etVolume);
        etExamineDuration = (AppCompatEditText)mView.findViewById(R.id.etExamineDuration);
        etProbiotics = (AppCompatEditText)mView.findViewById(R.id.etProbiotics);
        etMolasses = (AppCompatEditText)mView.findViewById(R.id.etMolasses);
        etFeed = (AppCompatEditText)mView.findViewById(R.id.etFeed);
        etRemark = (AppCompatEditText)mView.findViewById(R.id.etRemark);
      /*  etLenght = (AppCompatEditText)mView.findViewById(R.id.etLenght);
        etWidth = (AppCompatEditText)mView.findViewById(R.id.etWidth);*/

        tvExamineDate = (TextView) mView.findViewById(R.id.tvExamineDate);

        examineTime = System.currentTimeMillis();
        tvExamineDate.setText("Examine date: "+CommonConstants.getDateAndTime(examineTime+"."));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentFlocStatus.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String temp = etTemperature.getText().toString().trim();
                if (!CommonConstants.isFloat(temp)){
                    ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Please enter temperature(c).\nor check input formate e.g. \n22.5\n32");
                    return;
                }
                final String volume = etVolume.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(volume)){
                    ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Please enter volume(ml).\nor check input formate e.g. \n10\n35");
                    return;
                }
                final String duration = etExamineDuration.getText().toString().trim();
                if (!CommonConstants.isFloat(duration)){
                    ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Please enter Examine Duration(hr).\nor check input formate e.g. \n0.5\n1");
                    return;
                }
                final String probiotics = etProbiotics.getText().toString().trim();
                final String molasses = etMolasses.getText().toString().trim();
                final String feed = etFeed.getText().toString().trim();
                final String remark = etRemark.getText().toString().trim();

                builder = new LoadingDialog.Builder(FragmentFlocStatus.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostFloc.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            Floc result = new Floc();
                            result.setId((long)CommonConstants.getRandomInt(0,100000));
                            result.setTankId(tankId);
                            result.setRemark(remark);
                            result.setFeed(feed);
                            result.setMolasses(molasses);
                            result.setProbiotics(probiotics);
                            result.setTemperature(temp);
                            result.setVolume(volume);
                            result.setExamineDuration(duration);
                            result.setExamineTs(examineTime+"");
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            FlocDao dao = new FlocDao(FragmentFlocStatus.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentFlocStatus.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Floc responce = (Floc) sender;
                        FlocDao dao = new FlocDao(FragmentFlocStatus.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentFlocStatus.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentFlocStatus.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null,null, getString(R.string.base_url),
                        examineTime+"",duration, volume,temp,probiotics,molasses,feed,remark);
            }
        });

        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
