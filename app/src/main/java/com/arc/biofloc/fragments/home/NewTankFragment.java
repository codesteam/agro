/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.TankDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostTank;
import com.arc.biofloc.retrofit.responces.agro.tank.Tank;
import com.arc.biofloc.utility.Print;
import com.arc.biofloc.utility.ToastMsg;

import java.util.Calendar;


public class NewTankFragment extends Fragment {

    public static NewTankFragment newInstance() {
        return new NewTankFragment();
    }

    RelativeLayout rlNewProject;
    AppCompatButton btnCancel, btnCreate;
    boolean state = false;
    LoadingDialog.Builder builder;
    AppCompatEditText etTankName, etTankDetails, etType, etHeight, etWaterLevel, etRadius, etConicalHeight, etLenght, etWidth;
    RadioGroup rg1;
    RadioButton rect, cyl;
   // LinearLayout llCylinderTankParam, llRectTankParam;
    TextView tvMaxCpacity;
    TimePicker timePicker;
    DatePicker datePicker;
    float waterLevel = 0.0f, radius = 0.0f, conicalHeight = 0.0f;
    boolean tankShapeType = true;
    double tankMaxCapacity = 0.0f;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_new_tank, container, false);
        rlNewProject = (RelativeLayout)  mView.findViewById(R.id.rlNewProject);
        btnCreate = (AppCompatButton) mView.findViewById(R.id.btnCreate);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etTankName = (AppCompatEditText)mView.findViewById(R.id.etTankName);
        etTankDetails = (AppCompatEditText)mView.findViewById(R.id.etTankDetails);
        etType = (AppCompatEditText)mView.findViewById(R.id.etType);
        etHeight = (AppCompatEditText)mView.findViewById(R.id.etHeight);
        etWaterLevel = (AppCompatEditText)mView.findViewById(R.id.etWaterLevel);
        etRadius = (AppCompatEditText)mView.findViewById(R.id.etRadius);
        etConicalHeight = (AppCompatEditText)mView.findViewById(R.id.etConicalHeight);
      /*  etLenght = (AppCompatEditText)mView.findViewById(R.id.etLenght);
        etWidth = (AppCompatEditText)mView.findViewById(R.id.etWidth);*/
        rg1 = (RadioGroup) mView.findViewById(R.id.rg1);
        rect=(RadioButton) mView.findViewById(R.id.rect);
        cyl=(RadioButton) mView.findViewById(R.id.cy);
        tvMaxCpacity = (TextView) mView.findViewById(R.id.tvMaxCpacity);
        timePicker = (TimePicker) mView.findViewById(R.id.timePicker);
        datePicker = (DatePicker) mView.findViewById(R.id.datePicker);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)NewTankFragment.this.getActivity()).onBackPressed();
            }
        });


        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar calendar = Calendar.getInstance();
                calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(), timePicker.getCurrentMinute(), 0);
                final long startTime = calendar.getTimeInMillis();
                String name = etTankName.getText().toString().trim();
                if (name==null){
                    ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please write a tank name.");
                    return;
                }
                String details = etTankDetails.getText().toString().trim();
                String type = etType.getText().toString().trim();
                String height = etHeight.getText().toString().trim();
                String waterLevel = etWaterLevel.getText().toString().trim();
                Print.e(this, "water level:"+waterLevel);
                if (!CommonConstants.isFloat(waterLevel)){
                    ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please give a valid input for water level. \n example 33\n33.88");
                    return;
                }

                String tankShape = null;
                if(cyl.isChecked()){
                    tankShape = CommonConstants.TANK_TYPE_CYL;
                }else{
                    tankShape = CommonConstants.TANK_TYPE_RECT;
                }

                String radius = etRadius.getText().toString().trim();
                if (!CommonConstants.isFloat(radius)){
                    if(cyl.isChecked()){
                        ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please give a valid input for radius\n 33\n33.88");

                    }else{
                        ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please give a valid input for length\n 33\n33.88");
                    }
                    return;
                }
                String conicalHeight = etConicalHeight.getText().toString().trim();
                if (!CommonConstants.isFloat(conicalHeight)){
                    if(cyl.isChecked()){
                        ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please give a valid input for conical height\n example 33\n33.88");

                    }else{
                        ToastMsg.Toast(NewTankFragment.this.getActivity(), "Please give a valid input for width\n example 33\n33.88");
                    }
                    return;
                }

                final String maxCapacity = tankMaxCapacity+"";
              /*  @Field("name") String name,
                @Field("details") String details,
                @Field("feedType") String feedType,
                @Field("height") String height,
                @Field("water_level") String water_level,
                @Field("tank_shape") String tank_shape,
                @Field("radius") String radius,
                @Field("conical_height") String conical_height,
                @Field("max_capacity") String max_capacity,
                @Field("start_date") String start_date,
              */
              builder = new LoadingDialog.Builder(NewTankFragment.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostTank.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            Tank result = new Tank();
                            result.setId((long)CommonConstants.getRandomInt(0,1000000));
                            result.setProjectId(CommonSettings.getInstance(NewTankFragment.this.getActivity()).getProjectId());
                            result.setName(etTankName.getText().toString().trim());
                            result.setDetails(etTankDetails.getText().toString());
                            result.setType(etType.getText().toString().trim());
                            result.setHeight(etHeight.getText().toString().trim());
                            result.setWaterLevel(etWaterLevel.getText().toString().trim());
                            if (cyl.isChecked()){
                                result.setTankShape(CommonConstants.TANK_TYPE_CYL);
                            }else{
                                result.setTankShape(CommonConstants.TANK_TYPE_RECT);
                            }

                            result.setRadius(etRadius.getText().toString().trim());
                            result.setConicalHeight(etConicalHeight.getText().toString());
                            result.setMaxCapacity(maxCapacity);
                            result.setStartTs(startTime+"");
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            TankDao tankDao = new TankDao(NewTankFragment.this.getActivity());
                            tankDao.insert(result);

                            ToastMsg.Toast(NewTankFragment.this.getActivity(), "New fish tank created successfully");
                            ((MainActivity)NewTankFragment.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(NewTankFragment.this.getActivity(), "Tank creation failed, please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Tank responce = (Tank) sender;
                       /* Result result = new Result();
                        result.setId(responce.getId());
                        result.setProjectId(responce.getProjectId());
                        result.setName(responce.getName());
                        result.setDetails(responce.getDetails());
                        result.setType(responce.getType());
                        result.setHeight(responce.getHeight());
                        result.setWaterLevel(responce.getWaterLevel());
                        result.setTankShape(responce.getTankShape());
                        result.setRadius(responce.getRadius());
                        result.setConicalHeight(responce.getConicalHeight());
                        result.setMaxCapacity(responce.getMaxCapacity());
                        result.setStartTs(responce.getStartTs());
                        result.setCreatedTs(responce.getCreatedTs());
                        result.setModifiedTs(responce.getModifiedTs());*/
                        TankDao tankDao = new TankDao(NewTankFragment.this.getActivity());
                        tankDao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(NewTankFragment.this.getActivity(), "New fish tank created successfully");
                        ((MainActivity)NewTankFragment.this.getActivity()).onBackPressed();
                    }
                },null,CommonSettings.getInstance(NewTankFragment.this.getActivity()).getProjectId(), null,null, getString(R.string.base_url),
                        name,details,
                        type,height,waterLevel,tankShape,radius,conicalHeight,maxCapacity,startTime+"");
                Print.e(this, CommonConstants.getDateAndTime(startTime+""));

            }
        });

        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.cy) {
                    etRadius.setHint("Radius");
                    etConicalHeight.setHint("Conical height");
                    tankShapeType = true;
                    setTankMaxCpacity();
                } else  if (checkedId == R.id.rect) {
                    etRadius.setHint("Length");
                    etConicalHeight.setHint("Width");
                    tankShapeType = false;
                    setTankMaxCpacity();
                }


            }
        });
        cyl.setChecked(true);

        etWaterLevel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String value = String.valueOf(s);
                try{
                    waterLevel =  Float.valueOf(value.trim()).floatValue();
                    setTankMaxCpacity();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        etRadius.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String value = String.valueOf(s);
                try{
                    radius =  Float.valueOf(value.trim()).floatValue();
                    setTankMaxCpacity();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        etConicalHeight.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String value = String.valueOf(s);
                try{
                    conicalHeight =  Float.valueOf(value.trim()).floatValue();
                    setTankMaxCpacity();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        return mView;
    }


    private void setTankMaxCpacity(){
        if (waterLevel != 0.0 && conicalHeight != 0.0 && radius !=0.0){
            double volume=0.0f;
            if (tankShapeType){ // cylindircal tank
                volume = Math.PI * Math.pow(radius,2)*waterLevel+(Math.PI*Math.pow(radius,2)*conicalHeight)/3;
            }else{ //rectungular tank
                volume = waterLevel * conicalHeight * radius;
            }

            double ltrs = volume * 0.016387064f;
            tankMaxCapacity = ltrs;
            tvMaxCpacity.setText("Tank max capacity in litres: "+ CommonConstants.getUpto2DecimalPoint(tankMaxCapacity));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
