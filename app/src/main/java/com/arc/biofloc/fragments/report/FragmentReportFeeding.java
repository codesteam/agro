/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.report;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;

import tableview.TableViewAdapter;
import tableview.TableViewListener;
import tableview.TableViewModel;
import tableview.model.Cell;
import tableview.model.ColumnHeader;
import tableview.model.RowHeader;


public class FragmentReportFeeding extends Fragment {

    public static FragmentReportFeeding newInstance() {
        return new FragmentReportFeeding();
    }



   // LinearLayout llCylinderTankParam, llRectTankParam;
   private AbstractTableAdapter mTableViewAdapter;
    private TableView mTableView;
    private TableViewModel mTableViewModel;
    ArrayList<Feeding> results;
    List<List<Cell>> cells;
    List<RowHeader> rowHeaders;
    List<ColumnHeader> columnHeaders;
    Long tankId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_report_table, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }

        mTableView = mView.findViewById(R.id.tableview);


        FeedingDao  dao = new FeedingDao(this.getActivity());
        results = dao.getResults(tankId);
          if (results != null && results.size() > 0){
            Print.e(this, "results size: "+results.size());
              initializeTableView();
           mView.findViewById(R.id.noRecord).setVisibility(View.GONE);
        }else{
              mView.findViewById(R.id.noRecord).setVisibility(View.VISIBLE);
          }




        return mView;
    }

    private void initializeTableView() {
        // Create TableView View model class  to group view models of TableView
        mTableViewModel = new TableViewModel(getContext());

        // Create TableView Adapter
        mTableViewAdapter = new TableViewAdapter(getContext(), mTableViewModel);

        mTableView.setAdapter(mTableViewAdapter);
        mTableView.setTableViewListener(new TableViewListener(mTableView));
        rowHeaders = getRowHeaders(results);
        columnHeaders = getColHeaders();
        cells = getCells(results);
        mTableViewAdapter.setAllItems(columnHeaders,rowHeaders, cells);


    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle("Report");

    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }


    private List<ColumnHeader> getColHeaders(){
        List<ColumnHeader> colHeaders = new ArrayList<>();
        ColumnHeader colHeader1 = new ColumnHeader(0+"", "Entry date");
        colHeaders.add(colHeader1);
        ColumnHeader colHeader2 = new ColumnHeader(1+"","Legitimate Biomass");
        colHeaders.add(colHeader2);
        ColumnHeader colHeader3 = new ColumnHeader(2+"","Feed Percentage");
        colHeaders.add(colHeader3);
        ColumnHeader colHeader4 = new ColumnHeader(3+"","Feed Amount");
        colHeaders.add(colHeader4);
        ColumnHeader colHeader5 = new ColumnHeader(4+"","Protein Amount");
        colHeaders.add(colHeader5);
        ColumnHeader colHeader6 = new ColumnHeader(5+"","Produced Nitrogen");
        colHeaders.add(colHeader6);
        ColumnHeader colHeader7 = new ColumnHeader(6+"","Wasted Nitrogen");
        colHeaders.add(colHeader7);
        ColumnHeader colHeader8 = new ColumnHeader(7+"","Carbon Source Type");
        colHeaders.add(colHeader8);
        ColumnHeader colHeader9 = new ColumnHeader(8+"","Carbon Percentage");
        colHeaders.add(colHeader9);
        ColumnHeader colHeader10 = new ColumnHeader(9+"","Catalyst Amount");
        colHeaders.add(colHeader10);
        return  colHeaders;
    }

    private List<List<Cell>> getCells(ArrayList<Feeding> results){
        List<List<Cell>> cellsGrid = new ArrayList<List<Cell>>();
        for(int i=0; i< results.size();i++){
            List<Cell> cells = new ArrayList<>();

            String id = 0 + "-" + i;
            Cell cell1 = new Cell(id, CommonConstants.getDateAndTime(results.get(i).getFeedingTs())+"");
            cells.add(cell1);

            id = 1 + "-" + i;
            Cell cell2 = new Cell(id, results.get(i).getLegitimateBiomass()+"");
            cells.add(cell2);

            id = 2 + "-" + i;
            Cell cell3 = new Cell(id, results.get(i).getFeedPercentage()+"");
            cells.add(cell3);

            id = 3 + "-" + i;
            Cell cell4 = new Cell(id,results.get(i).getFeedAmount()+"");
            cells.add(cell4);

            id = 4 + "-" + i;
            Cell cell5 = new Cell(id,results.get(i).getProteinAmount()+"");
            cells.add(cell5);

            id = 5 + "-" + i;
            Cell cell6 = new Cell(id, results.get(i).getProducedNitrogen()+"");
            cells.add(cell6);

            id = 6 + "-" + i;
            Cell cell7 = new Cell(id,results.get(i).getWastedNitrogen()+"");
            cells.add(cell7);

            id = 7 + "-" + i;
            Cell cell8 = new Cell(id,results.get(i).getCarbonSourceType()+"");
            cells.add(cell8);

            id = 8 + "-" + i;
            Cell cell9 = new Cell(id,results.get(i).getCarbonPercentage()+"");
            cells.add(cell9);

            id = 9 + "-" + i;
            Cell cell10 = new Cell(id,results.get(i).getCatalystAmount()+"");
            cells.add(cell10);

            cellsGrid.add(cells);
        }
        return  cellsGrid;
    }
    private List<RowHeader> getRowHeaders(ArrayList<Feeding> results){
        List<RowHeader> rowHeaders = new ArrayList<>();
        for(int i=0; i< results.size();i++){
            RowHeader rowHeader = new RowHeader(i+"", (i+1)+"");
            rowHeaders.add(rowHeader);
        }
        return  rowHeaders;
    }




}
