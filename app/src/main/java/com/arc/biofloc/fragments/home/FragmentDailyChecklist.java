/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.DailyChecklistDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostDailyCheckList;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.DailyChecklist;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentDailyChecklist extends Fragment {

    public static FragmentDailyChecklist newInstance() {
        return new FragmentDailyChecklist();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etTemperature, etPh, etTDS, etDo, etMortality;
    TextView tvEntryDate;
    Long entryTime;
    long tankId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_daily_check_list, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etTemperature = (AppCompatEditText)mView.findViewById(R.id.etTemperature);
        etPh = (AppCompatEditText)mView.findViewById(R.id.etPh);
        etTDS = (AppCompatEditText)mView.findViewById(R.id.etTDS);
        etDo = (AppCompatEditText)mView.findViewById(R.id.etDo);
       // etAmmonia = (AppCompatEditText)mView.findViewById(R.id.etAmmonia);
        etMortality = (AppCompatEditText)mView.findViewById(R.id.etMortality);

        tvEntryDate = (TextView) mView.findViewById(R.id.tvEntryDate);

        entryTime = System.currentTimeMillis();
        tvEntryDate.setText("Entry date: "+CommonConstants.getDateAndTime(entryTime +""));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentDailyChecklist.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String temp = etTemperature.getText().toString().trim();

                if (!CommonConstants.isFloat(temp)){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter temperature(c).\nor check input formate e.g. \n22.45\n34");
                    return;
                }
                final String ph = etPh.getText().toString().trim();
                if (!CommonConstants.isFloat(ph)){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter PH level.\nor check input formate e.g. \n5.5\n7");
                    return;
                }
                final String tds = etTDS.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(tds)){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter TDS level.\nor check input formate e.g. \n150\n3300");
                    return;
                }

                final String doLevel = etDo.getText().toString().trim();
                if (!CommonConstants.isFloat(doLevel)){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter DO level.\nor check input formate e.g. \n5.5\n4");
                    return;
                }
                /*final String ammonia = etAmmonia.getText().toString().trim();
                if (ammonia==null || ammonia.length()==0){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter Ammonia level");
                    return;
                }*/
                final String mortality = etMortality.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(mortality)){
                    ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please enter Mortality level\nor check input formate eg. \n5\n40");
                    return;
                }


                builder = new LoadingDialog.Builder(FragmentDailyChecklist.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostDailyCheckList.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            DailyChecklist result = new DailyChecklist();
                            result.setId((long)CommonConstants.getRandomInt(0,100000));
                            result.setTankId(tankId);
                            result.setEntryTs(entryTime+"");
                            result.setMortality(mortality);
                            result.setPhLevel(ph);
                            result.setTdsLevel(tds);
                            result.setDoLevel(doLevel);
                            result.setAmmoniaLevel("0");
                            result.setTemperature(temp);
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            DailyChecklistDao dao = new DailyChecklistDao(FragmentDailyChecklist.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Saved successfully");
                            ((MainActivity)FragmentDailyChecklist.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        DailyChecklist responce = (DailyChecklist) sender;
                        DailyChecklistDao dao = new DailyChecklistDao(FragmentDailyChecklist.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentDailyChecklist.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentDailyChecklist.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null,null, getString(R.string.base_url),
                        entryTime +"",temp, ph,tds,doLevel,0+"",mortality);
            }
        });

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
