/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.report;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.db.agrodao.FlocDao;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Floc;
import com.arc.biofloc.utility.Print;

import java.util.ArrayList;
import java.util.List;

import tableview.TableViewAdapter;
import tableview.TableViewListener;
import tableview.TableViewModel;
import tableview.model.Cell;
import tableview.model.ColumnHeader;
import tableview.model.RowHeader;


public class FragmentReportFlocStatus extends Fragment {

    public static FragmentReportFlocStatus newInstance() {
        return new FragmentReportFlocStatus();
    }



   // LinearLayout llCylinderTankParam, llRectTankParam;
   private AbstractTableAdapter mTableViewAdapter;
    private TableView mTableView;
    private TableViewModel mTableViewModel;
    ArrayList<Floc> results;
    List<List<Cell>> cells;
    List<RowHeader> rowHeaders;
    List<ColumnHeader> columnHeaders;
    Long tankId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_report_table, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }

        mTableView = mView.findViewById(R.id.tableview);


        FlocDao dao = new FlocDao(this.getActivity());
        results = dao.getResults(tankId);
          if (results != null && results.size() > 0){
            Print.e(this, "results size: "+results.size());
              initializeTableView();
           mView.findViewById(R.id.noRecord).setVisibility(View.GONE);
        }else{
              mView.findViewById(R.id.noRecord).setVisibility(View.VISIBLE);
          }




        return mView;
    }

    private void initializeTableView() {
        // Create TableView View model class  to group view models of TableView
        mTableViewModel = new TableViewModel(getContext());

        // Create TableView Adapter
        mTableViewAdapter = new TableViewAdapter(getContext(), mTableViewModel);

        mTableView.setAdapter(mTableViewAdapter);
        mTableView.setTableViewListener(new TableViewListener(mTableView));
        rowHeaders = getRowHeaders(results);
        columnHeaders = getColHeaders();
        cells = getCells(results);
        mTableViewAdapter.setAllItems(columnHeaders,rowHeaders, cells);


    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle("Report");

    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }


    private List<ColumnHeader> getColHeaders(){
        List<ColumnHeader> colHeaders = new ArrayList<>();
        ColumnHeader colHeader1 = new ColumnHeader(0+"", "Examine date");
        colHeaders.add(colHeader1);
        ColumnHeader colHeader2 = new ColumnHeader(1+"","Temperature (c)");
        colHeaders.add(colHeader2);
        ColumnHeader colHeader3 = new ColumnHeader(2+"","Volume (ml)");
        colHeaders.add(colHeader3);
        ColumnHeader colHeader4 = new ColumnHeader(3+"","Examine Duration (hr)");
        colHeaders.add(colHeader4);
        ColumnHeader colHeader5 = new ColumnHeader(4+"","Probiotics (gm/ml)");
        colHeaders.add(colHeader5);
        ColumnHeader colHeader6 = new ColumnHeader(5+"","Molasses amount (gm)");
        colHeaders.add(colHeader6);
        ColumnHeader colHeader7 = new ColumnHeader(6+"","Feed (gm)");
        colHeaders.add(colHeader7);
        ColumnHeader colHeader8 = new ColumnHeader(7+"","Remarks");
        colHeaders.add(colHeader8);
        return  colHeaders;
    }

    private List<List<Cell>> getCells(ArrayList<Floc> results){
        List<List<Cell>> cellsGrid = new ArrayList<List<Cell>>();
        for(int i=0; i< results.size();i++){
            List<Cell> cells = new ArrayList<>();

            String id = 0 + "-" + i;
            Cell cell1 = new Cell(id, CommonConstants.getDateAndTime(results.get(i).getExamineTs())+"");
            cells.add(cell1);

            id = 1 + "-" + i;
            Cell cell2 = new Cell(id, results.get(i).getTemperature()+"");
            cells.add(cell2);

            id = 2 + "-" + i;
            Cell cell3 = new Cell(id, results.get(i).getVolume()+"");
            cells.add(cell3);

            id = 3 + "-" + i;
            Cell cell4 = new Cell(id,results.get(i).getExamineDuration()+"");
            cells.add(cell4);

            id = 4 + "-" + i;
            Cell cell5 = new Cell(id,results.get(i).getProbiotics()+"");
            cells.add(cell5);

            id = 5 + "-" + i;
            Cell cell6 = new Cell(id, results.get(i).getMolasses()+"");
            cells.add(cell6);

            id = 6 + "-" + i;
            Cell cell7 = new Cell(id,results.get(i).getFeed()+"");
            cells.add(cell7);

            id = 7 + "-" + i;
            Cell cell8 = new Cell(id,results.get(i).getRemark()+"");
            cells.add(cell8);
            cellsGrid.add(cells);
        }
        return  cellsGrid;
    }
    private List<RowHeader> getRowHeaders(ArrayList<Floc> results){
        List<RowHeader> rowHeaders = new ArrayList<>();
        for(int i=0; i< results.size();i++){
            RowHeader rowHeader = new RowHeader(i+"", (i+1)+"");
            rowHeaders.add(rowHeader);
        }
        return  rowHeaders;
    }




}
