/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.report;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;


public class FragmentPropertyReport extends Fragment {

    public static FragmentPropertyReport newInstance() {
        return new FragmentPropertyReport();
    }
    Long tankId;
    String tankName;

    AppCompatButton btnFlocStatus, btnFeeding,
            btnDaily, btnWeeklyChecklist, btnFeeds;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_report_property, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
            tankName = bundle.getString("name");
        }

        btnFlocStatus = (AppCompatButton) mView.findViewById(R.id.btnFlocStatus);

        btnFeeding = (AppCompatButton) mView.findViewById(R.id.btnFeeding);
        btnDaily = (AppCompatButton) mView.findViewById(R.id.btnDaily);
        btnFeeds = (AppCompatButton) mView.findViewById(R.id.btnFeeds);

        btnWeeklyChecklist = (AppCompatButton) mView.findViewById(R.id.btnWeeklyChecklist);

        btnFlocStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentReportFlocStatus fragment = new FragmentReportFlocStatus();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyReport.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnFeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentReportFeeding fragment = new FragmentReportFeeding();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyReport.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentReportDailyChecklist fragment = new FragmentReportDailyChecklist();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyReport.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        btnWeeklyChecklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               FragmentReportWeeklyChecklist fragment = new FragmentReportWeeklyChecklist();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyReport.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        btnFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentReportFeeds fragment = new FragmentReportFeeds();
                Bundle bundle = new Bundle();
                bundle.putLong("id", tankId);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = FragmentPropertyReport.this.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)this.getActivity()).setActionBarTitle(tankName+"");
        ((MainActivity)this.getActivity()).enableBack(true);
    }

}
