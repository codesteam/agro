/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.SeedsDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostFishSeeds;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Seeds;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentFishSeeds extends Fragment {

    public static FragmentFishSeeds newInstance() {
        return new FragmentFishSeeds();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etCapacity, etTarget, etArrivalDate, etPreparationDetails, etInitialAvarageSize,
            etMarketableSize, etNumberOfSeeds, etReleaseDate, etHarvestingDate, etSeedsDetails, etSupplierDetails;
    TextView tvCapacity, tvTarget, tvArrivalDate, tvPreparationDetails, tvInitialAvarageSize, tvMarketableSize,
            tvNumberOfSeeds, tvReleaseDate, tvHarvestingDate, tvSeedsDetails, tvSupplierDetails;
    Long examineTime;
    long tankId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_seeds, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etCapacity = (AppCompatEditText)mView.findViewById(R.id.etCapacity);
        etTarget = (AppCompatEditText)mView.findViewById(R.id.etTarget);
        etArrivalDate = (AppCompatEditText)mView.findViewById(R.id.etArrivalDate);
        etPreparationDetails = (AppCompatEditText)mView.findViewById(R.id.etPreparationDetails);
        etInitialAvarageSize = (AppCompatEditText)mView.findViewById(R.id.etInitialAvarageSize);
        etMarketableSize = (AppCompatEditText)mView.findViewById(R.id.etMarketableSize);
        etNumberOfSeeds = (AppCompatEditText)mView.findViewById(R.id.etNumberOfSeeds);
        etReleaseDate = (AppCompatEditText)mView.findViewById(R.id.etReleaseDate);
        etHarvestingDate = (AppCompatEditText)mView.findViewById(R.id.etHarvestingDate);
        etSeedsDetails = (AppCompatEditText)mView.findViewById(R.id.etSeedsDetails);
        etSupplierDetails = (AppCompatEditText)mView.findViewById(R.id.etSupplierDetails);


      /*  etLenght = (AppCompatEditText)mView.findViewById(R.id.etLenght);
        etWidth = (AppCompatEditText)mView.findViewById(R.id.etWidth);*/

        tvCapacity = (TextView) mView.findViewById(R.id.tvCapacity);
        tvTarget = (TextView) mView.findViewById(R.id.tvTarget);
        tvArrivalDate = (TextView) mView.findViewById(R.id.tvArrivalDate);
        tvPreparationDetails = (TextView) mView.findViewById(R.id.tvPreparationDetails);
        tvInitialAvarageSize = (TextView) mView.findViewById(R.id.tvInitialAvarageSize);
        tvMarketableSize = (TextView) mView.findViewById(R.id.tvMarketableSize);
        tvNumberOfSeeds = (TextView) mView.findViewById(R.id.tvNumberOfSeeds);
        tvReleaseDate = (TextView) mView.findViewById(R.id.tvReleaseDate);
        tvHarvestingDate = (TextView) mView.findViewById(R.id.tvHarvestingDate);
        tvSeedsDetails = (TextView) mView.findViewById(R.id.tvSeedsDetails);
        tvSupplierDetails = (TextView) mView.findViewById(R.id.tvSupplierDetails);
      //  tvInitialBiomass = (TextView) mView.findViewById(R.id.tvInitialBiomass);


        SeedsDao seedsDao = new SeedsDao(this.getActivity());
        Seeds seeds = seedsDao.getResult(tankId);
        if (seeds != null){
            alterVisibility(tvCapacity, etCapacity, seeds.getTankCapacity(), "Tank max capacity: ", "kg.");
            float targetPer = 0.0f;
            try
            {
                targetPer = Float.valueOf(seeds.getTankCapacity()).floatValue() * Float.valueOf(seeds.getTarget()).floatValue()/100;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            alterVisibility(tvTarget,   etTarget,   CommonConstants.getUpto2DecimalPoint(targetPer), "Target"+" "+seeds.getTarget()+"% of Max capacity: ", " kg.");
            alterVisibility(tvArrivalDate, etArrivalDate, seeds.getArrivalDate(), "Arraval Date of seeds: ", ".");
            alterVisibility(tvPreparationDetails, etPreparationDetails, seeds.getPreparationDetails(), "Preparation before release: ", ".");
            alterVisibility(tvInitialAvarageSize, etInitialAvarageSize, seeds.getInitialAvarageSize(), "Initial meadin size: ", " inch.");
            alterVisibility(tvMarketableSize, etMarketableSize, seeds.getTankCapacity(), "Marketable size: ", " kg.");
            alterVisibility(tvNumberOfSeeds, etNumberOfSeeds, seeds.getTankCapacity(), "Number Of Seeds: ", ".");
            alterVisibility(tvReleaseDate, etReleaseDate, seeds.getTankCapacity(), "Release Date: ", ".");
            alterVisibility(tvHarvestingDate, etHarvestingDate, seeds.getTankCapacity(), "Harvest Date: ", ".");
            alterVisibility(tvSeedsDetails, etSeedsDetails, seeds.getTankCapacity(), "Seeds Details: ", ".");
            alterVisibility(tvSupplierDetails, etSupplierDetails, seeds.getTankCapacity(), "SupplierDetails: ", ".");
           // alterVisibility(tvInitialBiomass, etInitialBiomass, seeds.getTankCapacity(), "Initial Biomass: ", " kg");
            btnSave.setVisibility(View.GONE);
            btnCancel.setText("Back");
            btnCancel.setTextColor(getResources().getColor(R.color.white));
            btnCancel.setBackgroundResource(R.drawable.bt_shape3);

        }else{
            tvCapacity.setVisibility(View.GONE);
           // tvTarget.setVisibility(View.GONE);
            tvArrivalDate.setVisibility(View.GONE);
            tvPreparationDetails.setVisibility(View.GONE);
            tvInitialAvarageSize.setVisibility(View.GONE);
            tvMarketableSize.setVisibility(View.GONE);
            tvNumberOfSeeds.setVisibility(View.GONE);
            tvReleaseDate.setVisibility(View.GONE);
            tvHarvestingDate.setVisibility(View.GONE);
            tvSeedsDetails.setVisibility(View.GONE);
            tvSupplierDetails.setVisibility(View.GONE);
           // tvInitialBiomass.setVisibility(View.GONE);
            etTarget.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    String capacity = etCapacity.getText().toString().trim();
                    String value = String.valueOf(s);
                    if (capacity != null && capacity.length() >0){
                        float targetPer = 0.0f;
                        try
                        {
                            targetPer = Float.valueOf(capacity).floatValue() * Float.valueOf(value).floatValue()/100;
                            tvTarget.setText("Target "+value+"% of max capacity: "+CommonConstants.getUpto2DecimalPoint(targetPer) +" kg.");
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }

                    }
                }
            });

            etCapacity.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    String target = etTarget.getText().toString().trim();
                    String value = String.valueOf(s);
                    if (target != null && target.length() >0){
                        float targetPer = 0.0f;
                        try
                        {
                            targetPer = Float.valueOf(target).floatValue() * Float.valueOf(value).floatValue()/100;
                            tvTarget.setText("Target "+target+"% of max capacity: "+CommonConstants.getUpto2DecimalPoint(targetPer) +"kg.");
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }

                    }
                }
            });
        }
        examineTime = System.currentTimeMillis();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentFishSeeds.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String capacity = etCapacity.getText().toString().trim();
                if (!CommonConstants.isFloat(capacity)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter max capacity of tank(kg).\nor check input formate e.g. \n5000\n3000.67");
                    return;
                }
                final String target = etTarget.getText().toString().trim();
                if (!CommonConstants.isFloat(target)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter target % of max capacity.\nor check input formate e.g. \n50.89\n90");
                    return;
                }

                final String arrivalDate = etArrivalDate.getText().toString().trim();
                if (!CommonConstants.isValidDate(arrivalDate)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter arrival date of seeds.\nor check input formate e.g. \n23/08/2019\n13/03/2019");
                    return;
                }

                final String prep = etPreparationDetails.getText().toString().trim();
                if (prep==null || prep.length()==0){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter preparation details before releasing seeds into tank");
                    return;
                }
                final String avarageSize = etInitialAvarageSize.getText().toString().trim();
                if (!CommonConstants.isFloat(avarageSize)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter meadin initial size (inch)");
                    return;
                }

                final String marketableSize = etMarketableSize.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(marketableSize)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter marketable size (gm).\nor check input formate e.g. \n450\n790");
                    return;
                }

                final String noOfSeeds = etNumberOfSeeds.getText().toString().trim();
                if (!CommonConstants.isPositiveInteger(noOfSeeds)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter number of seeds.\nor check input formate e.g. \n2450\n1790");
                    return;
                }
                final String releaseDate = etReleaseDate.getText().toString().trim();
                if (!CommonConstants.isValidDate(releaseDate)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter release date: \nor check input formate e.g.\n23/08/2019\n13/03/2019");
                    return;
                }

                final String harvestingDate = etHarvestingDate.getText().toString().trim();
                if (!CommonConstants.isValidDate(harvestingDate)){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter harvesting date (approx):\nor check input formate e.g. \n23/08/2019\n13/03/2019");
                    return;
                }

                final String seedDetails = etSeedsDetails.getText().toString().trim();
                if (seedDetails==null || seedDetails.length()==0){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter seeds details.");
                    return;
                }
                final String supplierDetails = etSupplierDetails.getText().toString().trim();
                if (supplierDetails==null || supplierDetails.length()==0){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter supplier details.");
                    return;
                }

              /*  final String biomass = etInitialBiomass.getText().toString().trim();
                if (biomass==null || biomass.length()==0){
                    ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please enter initial biomass.");
                    return;
                }*/


                builder = new LoadingDialog.Builder(FragmentFishSeeds.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostFishSeeds.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            Seeds result = new Seeds();
                            result.setId(tankId);
                            result.setTankId(tankId);
                            result.setInitialBiomass(0+"");
                            result.setSeedsDetails(seedDetails);
                            result.setSupplierDetails(supplierDetails);
                            result.setHarvestingDate(harvestingDate);
                            result.setReleaseDate(releaseDate);
                            result.setNumberOfSeeds(noOfSeeds);
                            result.setMarketableSize(marketableSize);
                            result.setInitialAvarageSize(avarageSize);

                            result.setPreparationDetails(prep);
                            result.setArrivalDate(arrivalDate);
                            result.setTarget(target);
                            result.setTankCapacity(capacity);

                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            SeedsDao dao = new SeedsDao(FragmentFishSeeds.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Saved successfully on fail");
                            ((MainActivity)FragmentFishSeeds.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Seeds responce = (Seeds) sender;
                        SeedsDao dao = new SeedsDao(FragmentFishSeeds.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentFishSeeds.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentFishSeeds.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null, null, getString(R.string.base_url),
                        capacity,target,arrivalDate,prep,avarageSize,marketableSize,noOfSeeds
                        ,releaseDate,harvestingDate,seedDetails,supplierDetails,0+"");
            }
        });

        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }




    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }

    private void alterVisibility(TextView textView, View hideView, String value, String prefix, String suffix){
        if(textView!=null){
            textView.setVisibility(View.VISIBLE);
            setValue(textView, value, prefix, suffix);
        }

        if(hideView!=null){
            hideView.setVisibility(View.GONE);
        }

    }
    private void setValue(TextView tv, String value, String prefix, String suffix){
        if (prefix==null){
            prefix = "";
        }
        if (suffix == null){
            suffix = "";
        }
        tv.setText(prefix+" "+value+" "+suffix);
    }
}
