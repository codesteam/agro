/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.arc.biofloc.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arc.biofloc.R;
import com.arc.biofloc.activity.MainActivity;
import com.arc.biofloc.common.CommonConstants;
import com.arc.biofloc.common.CommonSettings;
import com.arc.biofloc.customdialog.LoadingDialog;
import com.arc.biofloc.db.agrodao.FeedingDao;
import com.arc.biofloc.db.agrodao.HarvestDao;
import com.arc.biofloc.retrofit.api.APIClientResponse;
import com.arc.biofloc.retrofit.handler.agro.PostHarvesting;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Feeding;
import com.arc.biofloc.retrofit.responces.agro.tanks_components.Harvest;
import com.arc.biofloc.utility.ToastMsg;


public class FragmentHervesting extends Fragment {

    public static FragmentHervesting newInstance() {
        return new FragmentHervesting();
    }
    AppCompatButton btnSave, btnCancel;
    LoadingDialog.Builder builder;
    AppCompatEditText etTankBiomass, etLegitimateBiomass, etRemark;
    TextView tvHarvestingTime, tvTankBiomass, tvLegitimateBiomass, tvRemark;
    Long harvestingTime;
    long tankId = 0;
    FeedingDao feedingDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_tank_harvest, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tankId = bundle.getLong("id");
        }
        btnSave = (AppCompatButton) mView.findViewById(R.id.btnSave);
        btnCancel = (AppCompatButton) mView.findViewById(R.id.btnCancel);

        etTankBiomass = (AppCompatEditText)mView.findViewById(R.id.etTankBiomass);
        //etLegitimateBiomass = (AppCompatEditText)mView.findViewById(R.id.etTarget);
        etRemark = (AppCompatEditText)mView.findViewById(R.id.etRemark);

      /*  etLenght = (AppCompatEditText)mView.findViewById(R.id.etLenght);
        etWidth = (AppCompatEditText)mView.findViewById(R.id.etWidth);*/

        tvHarvestingTime = (TextView) mView.findViewById(R.id.tvHarvestingTime);
        tvTankBiomass = (TextView) mView.findViewById(R.id.tvTankBiomass);
        tvLegitimateBiomass = (TextView) mView.findViewById(R.id.tvLegitimateBiomass);
        tvRemark = (TextView) mView.findViewById(R.id.tvRemark);


        HarvestDao dao = new HarvestDao(this.getActivity());
        Harvest result = dao.getResult(tankId);
        if (result != null){
            tvHarvestingTime.setText("Harvested on date: "+CommonConstants.getDateAndTime(result.getHarvestingTs()+"."));
            alterVisibility(tvTankBiomass, etTankBiomass, result.getTankBiomass(), "Tank Biomass: ", "kg.");
            alterVisibility(tvLegitimateBiomass, etLegitimateBiomass, result.getLegitimateBiomass(), "Legitimate Biomass: ", " kg.");
            alterVisibility(tvRemark, etRemark, result.getRemark(), "Remark: ", ".");
            btnSave.setVisibility(View.GONE);
            btnCancel.setText("Back");
            btnCancel.setTextColor(getResources().getColor(R.color.white));
            btnCancel.setBackgroundResource(R.drawable.bt_shape3);

        }else{

            harvestingTime = System.currentTimeMillis();
            tvHarvestingTime.setText("Harvest on date:"+CommonConstants.getDateAndTime(harvestingTime+"")+".");
            tvTankBiomass.setVisibility(View.GONE);
           // tvTarget.setVisibility(View.GONE);
            feedingDao = new FeedingDao(this.getActivity());
            Feeding feeding = feedingDao.getResult(tankId);
            if (feeding != null){
                tvLegitimateBiomass.setText("Legitimate Biomass: "+feeding.getLegitimateBiomass()+".");
            }else{
                tvLegitimateBiomass.setText("Legitimate Biomass: please start feeding your fish first.");
            }

            tvRemark.setVisibility(View.GONE);
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)FragmentHervesting.this.getActivity()).onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String tankBiomass = etTankBiomass.getText().toString().trim();
                if (!CommonConstants.isFloat(tankBiomass)){
                    ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Please enter tank biomass in kg.\\nor check input formate e.g. \\n4500.5\\n3200\"");
                    return;
                }
                Feeding feeding = feedingDao.getResult(tankId);
                String biomass = null;
                if (feeding!=null){
                    biomass = feeding.getLegitimateBiomass();
                }
                final String legitimateBiomass = biomass;
               /* final String legitimateBiomass = etLegitimateBiomass.getText().toString().trim();
                if (legitimateBiomass==null || legitimateBiomass.length()==0){
                    ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Please enter Legitimate Biomass.");
                    return;
                }*/

                final String remark = etRemark.getText().toString().trim();
                if (remark==null || remark.length()==0){
                    ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Please enter remark.");
                    return;
                }

                builder = new LoadingDialog.Builder(FragmentHervesting.this.getActivity()).isCancellable(false);
                builder.setMessage(getString(R.string.please_wait)).build();
                PostHarvesting.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        if (CommonConstants.OFFLINE_MODE){
                            Harvest result = new Harvest();
                            result.setId(tankId);
                            result.setTankId(tankId);
                            result.setLegitimateBiomass(legitimateBiomass);
                            result.setTankBiomass(tankBiomass);
                            result.setRemark(remark);
                            result.setHarvestingTs(harvestingTime+"");
                            result.setCreatedTs(System.currentTimeMillis()+"");
                            result.setModifiedTs(System.currentTimeMillis()+"");
                            HarvestDao dao = new HarvestDao(FragmentHervesting.this.getActivity());
                            dao.insert(result);
                            ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Saved successfully on fail");
                            ((MainActivity)FragmentHervesting.this.getActivity()).onBackPressed();

                        }else{
                            ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Please try again!!");
                        }
                        builder.dismiss();
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Harvest responce = (Harvest) sender;
                        HarvestDao dao = new HarvestDao(FragmentHervesting.this.getActivity());
                        dao.insert(responce);
                        builder.dismiss();
                        ToastMsg.Toast(FragmentHervesting.this.getActivity(), "Saved successfully");
                        ((MainActivity)FragmentHervesting.this.getActivity()).onBackPressed();
                    }
                },null,tankId, null, null, getString(R.string.base_url),
                        harvestingTime+"",tankBiomass,legitimateBiomass,remark);
            }
        });

        return mView;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if(grid!= null){
            startGridAnimation();
        }*/
        ((MainActivity)this.getActivity()).setActionBarTitle(CommonSettings.getInstance(this.getActivity()).getProjectName());

    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }

    private void alterVisibility(TextView textView, View hideView, String value, String prefix, String suffix){
        if(textView!=null){
            textView.setVisibility(View.VISIBLE);
            setValue(textView, value, prefix, suffix);
        }

        if(hideView!=null){
            hideView.setVisibility(View.GONE);
        }

    }
    private void setValue(TextView tv, String value, String prefix, String suffix){
        if (prefix==null){
            prefix = "";
        }
        if (suffix == null){
            suffix = "";
        }
        tv.setText(prefix+" "+value+" "+suffix);
    }
}
