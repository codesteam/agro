package com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

/**
 * Created by chief on 3/5/18.
 */

public class NoNet implements ConnectionCallback {
    private static int count;
    private boolean isAlreadyShown = false;
    final String TAG = "NoNet";
    private DefaultNoNet mDefaultNoNet;
    private Context context;
    private NetworkMonitor mNetworkMonitor;
    private FragmentManager fm;
    private IntentFilter intentFilter;
    private ConnectionCallback connectionCallback;
    private Activity activity;
    private boolean isConnectionActive;
    private boolean isRegistered;

    public NoNet(Activity activity){
        this.activity = activity;
    }
    public void setConnectionCallback(ConnectionCallback connectionCallback) {
        this.connectionCallback = connectionCallback;
    }

    public boolean isConnectionActive() {
        return isConnectionActive;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    @Override
    public void Networkupdate(final boolean isConnectionActive) {
        this.isConnectionActive = isConnectionActive;
        connectionCallback.Networkupdate(isConnectionActive);
    }

    public void showDefaultDialog() {
        if (!isAlreadyShown) {
            mDefaultNoNet.show(fm, "nonetDefault");
            isAlreadyShown = true;
            Log.d(TAG, "Showing Dialog");
        }
    }

    public void hideDefaultDialog() {
        if (isAlreadyShown){
            mDefaultNoNet.dismiss();
            isAlreadyShown = false;
            Log.d(TAG, "Dismissing Dialog");
        }
    }

    public void RegisterNoNet() {
        if(!isRegistered){
            isRegistered = true;
            context.registerReceiver(mNetworkMonitor, intentFilter);
            mNetworkMonitor.register(this);
        }
    }

    public void initNoNet(int layoutId, View.OnClickListener backBtnListener,  Context context, FragmentManager fragmentManager, boolean cancelable) {
        if (mNetworkMonitor == null) {
            mNetworkMonitor = new NetworkMonitor();
            this.context = context;
            this.fm = fragmentManager;
            intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            mDefaultNoNet = DefaultNoNet.newInstance(layoutId, backBtnListener);
            mDefaultNoNet.setCancelable(cancelable);

        }
    }

    public void unRegisterNoNet() {
        if (isRegistered){
            isRegistered = false;
            context.unregisterReceiver(mNetworkMonitor);
            mNetworkMonitor.remove(this);
        }

    }
}