package com.arc.biofloc.fivemin.chief.nonetworklibrary.networkBroadcast;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.arc.biofloc.R;


/**
 * Created by chief on 2/5/18.
 */

public class DefaultNoNet extends DialogFragment {


    private Button notify, settings;
    private ImageView close;
    int layoutId;
    View.OnClickListener backBtnListner = null;
    public DefaultNoNet(){

    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public void setBackBtnListner(View.OnClickListener backBtnListner) {
        this.backBtnListner = backBtnListner;
    }

    public static DefaultNoNet newInstance(int layoutId,View.OnClickListener backBtnListner ) {
        DefaultNoNet frag = new DefaultNoNet();
        frag.setLayoutId(layoutId);
        frag.setBackBtnListner(backBtnListner);
        frag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(layoutId, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get Buttons from view
        notify = view.findViewById(R.id.RetryBtn);
        settings = view.findViewById(R.id.settingsBtn);
        if(backBtnListner != null){
            view.findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    backBtnListner.onClick(v);
                }
            });
        }
       /* close = view.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });*/

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWifiSettings();
            }
        });

        notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMobileData();
            }
        });

    }

    private void openWifiSettings() {
        startActivity(
                new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    private void openMobileData() {
        startActivity(
                new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));
    }



    @Override
    public void dismiss() {
        super.dismiss();
    }
}
