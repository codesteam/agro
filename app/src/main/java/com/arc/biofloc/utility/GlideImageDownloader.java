package com.arc.biofloc.utility;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.arc.biofloc.R;
import com.arc.biofloc.customdialog.DownloadCompleteListner;

public class GlideImageDownloader {
    String url;
    Activity context;
    ProgressBar pb;
    ImageView imageViewPb;
    boolean downloadComplete = false;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.warning_gray)
            .error(R.drawable.warning_gray)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH);
    DownloadCompleteListner downloadCompleteListner;
    public GlideImageDownloader(String url, Activity context){
        this.url = url;
        this.context = context;
    }

    public void setDownloadCompleteListner(DownloadCompleteListner downloadCompleteListner) {
        this.downloadCompleteListner = downloadCompleteListner;
    }

    public void setImageViewPb(ImageView imageViewPb) {
        this.imageViewPb = imageViewPb;
    }

    public void download(){
        Glide.with(context)
                .applyDefaultRequestOptions(options)
                .load(url)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        downloadComplete = true;
                        Print.e(this, "download complete url: "+url);
                        downloadCompleteListner.complete();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        downloadComplete = true;
                        Print.e(this, "download complete url: "+url);
                        downloadCompleteListner.complete();
                        return false;
                    }
                })
                .preload();

    }

    public boolean isDownloadComplete() {
        return downloadComplete;
    }
}
