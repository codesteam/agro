package com.android.rivchat.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.arc.biofloc.R;


public class Pushy {
    String notificationTitle = "Pushy";
    String notificationText = "Test notification";
    public void push(Context context, String name, String content, int id, Bitmap icon, boolean isGroup){

        // Prepare a notification with vibration, sound and lights
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name)
                .setContentText(content)
                .setLights(Color.RED, 1000, 1000)
                .setVibrate(new long[]{0, 400, 250, 400})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(getPendingIntent(context, context));
        // Automatically configure a Notification Channel for devices running Android O+
        //Pushy.setNotificationChannel(builder, context);

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        // Build the notification and display it
        notificationManager.notify(1, builder.build());
    }

    private PendingIntent getPendingIntent(Context result, Context context){
        Uri uri = Uri.parse("market://details?id=" + result.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, goToMarket, 0);
        return  contentIntent;
    }
    public void showNotification(Context context, String name, String content, int id, Bitmap icon, boolean isGroup) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = context.getPackageName();
        String channelName = context.getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(Html.fromHtml(content+"\n\ntap to view"));
        bigText.setBigContentTitle(name);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle()
                // Overrides ContentText in the big form of the template.
                .bigText(Html.fromHtml(content+"\n\ntap to view"))
                // Overrides ContentTitle in the big form of the template.
                .setBigContentTitle(name)
                // Summary line after the detail section in the big form of the template.
                // Note: To improve readability, don't overload the user with info. If Summary Text
                // doesn't add critical information, you should skip it.
                .setSummaryText(content);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(name+"") // text that show on notification bar
                .setAutoCancel(true)
                .setContentTitle(name+"")
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setGroup(context.getString(R.string.app_name))
                .setStyle(bigTextStyle);

        mBuilder.setContentIntent(getPendingIntent(context, context));

        notificationManager.notify(id, mBuilder.build());
    }
}
